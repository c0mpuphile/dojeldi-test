import { combineReducers } from 'redux';
import authReducer from './auth/reducer';
import categoryReducer from './category/reducer';
import slidersReducer from './sliders/reducer';
import bannersReducer from './banners/reducer';
import sectionsReducer from './sections/reducer';
import locationsReducer from './locations/reducer';
import cartReducer from './cart/reducer';
import bookReducer from './book/reducer';
import searchReducer from './search/reducer';
import filtersReducer from './filters/reducer';
import profileReducer from './profile/reducer';
import invoiceReducer from './invoice/reducer';
import transactionsReducer from './transactions/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  category: categoryReducer,
  sliders: slidersReducer,
  banners: bannersReducer,
  sections: sectionsReducer,
  locations: locationsReducer,
  cart: cartReducer,
  book: bookReducer,
  search: searchReducer,
  filters: filtersReducer,
  profile: profileReducer,
  invoice: invoiceReducer,
  transactions: transactionsReducer,
});

export default rootReducer;
