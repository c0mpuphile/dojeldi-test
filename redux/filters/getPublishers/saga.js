import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import FiltersAPI from '../../../services/filters';

function* getPublishers(action) {
  yield put(actions.getPublishersStart());
  try {
    const response = yield call([FiltersAPI, 'getPublishers'], action.payload);
    const data = response.data;
    yield put(actions.getPublishersSuccess(data));
  } catch (error) {
    yield put(actions.getPublishersFail(error));
  }
}

export default getPublishers;
