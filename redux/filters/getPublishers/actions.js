import * as types from './types';

export const getPublishers = payload => {
  return {
    type: types.FILTERS_GETPUBLISHERS,
    payload: payload,
  };
};

export const getPublishersStart = () => {
  return {
    type: types.FILTERS_GETPUBLISHERS_START,
  };
};

export const getPublishersSuccess = payload => {
  return {
    type: types.FILTERS_GETPUBLISHERS_SUCCESS,
    payload: payload,
  };
};

export const getPublishersFail = error => {
  return {
    type: types.FILTERS_GETPUBLISHERS_FAIL,
    error: error,
  };
};