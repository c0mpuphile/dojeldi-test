import * as types from './types';

export const getAuthors = payload => {
  return {
    type: types.FILTERS_GETAUTHORS,
    payload: payload,
  };
};

export const getAuthorsStart = () => {
  return {
    type: types.FILTERS_GETAUTHORS_START,
  };
};

export const getAuthorsSuccess = payload => {
  return {
    type: types.FILTERS_GETAUTHORS_SUCCESS,
    payload: payload,
  };
};

export const getAuthorsFail = error => {
  return {
    type: types.FILTERS_GETAUTHORS_FAIL,
    error: error,
  };
};