import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import FiltersAPI from '../../../services/filters';

function* getAuthors(action) {
  yield put(actions.getAuthorsStart());
  try {
    const response = yield call([FiltersAPI, 'getAuthors'], action.payload);
    const data = response.data;
    yield put(actions.getAuthorsSuccess(data));
  } catch (error) {
    yield put(actions.getAuthorsFail(error));
  }
}

export default getAuthors;
