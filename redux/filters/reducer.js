import { combineReducers } from 'redux';
import getPublishersReducer from './getPublishers/reducer';
import getAuthorsReducer from './getAuthors/reducer';
import getTagsReducer from './getTags/reducer';
import getBookTypesReducer from './getBookTypes/reducer';

const filtersReducer = combineReducers({
  getPublishers: getPublishersReducer,
  getAuthors: getAuthorsReducer,
  getTags: getTagsReducer,
  getBookTypes: getBookTypesReducer,
});

export default filtersReducer;
