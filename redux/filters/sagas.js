import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getPublishers from './getPublishers/saga';
import getAuthors from './getAuthors/saga';
import getTags from './getTags/saga';
import getBookTypes from './getBookTypes/saga';
// Types
import { FILTERS_GETPUBLISHERS } from './getPublishers/types';
import { FILTERS_GETAUTHORS } from './getAuthors/types';
import { FILTERS_GETTAGS } from './getTags/types';
import { FILTERS_GETBOOKTYPES } from './getBookTypes/types';

function* watchAll() {
  yield all([
    takeEvery(FILTERS_GETPUBLISHERS, getPublishers),
    takeEvery(FILTERS_GETAUTHORS, getAuthors),
    takeEvery(FILTERS_GETTAGS, getTags),
    takeEvery(FILTERS_GETBOOKTYPES, getBookTypes),
  ]);
}

export default watchAll;
