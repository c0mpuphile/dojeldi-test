import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import FiltersAPI from '../../../services/filters';

function* getBookTypes(action) {
  yield put(actions.getBookTypesStart());
  try {
    const response = yield call([FiltersAPI, 'getBookTypes'], action.payload);
    const data = response.data;
    yield put(actions.getBookTypesSuccess(data));
  } catch (error) {
    yield put(actions.getBookTypesFail(error));
  }
}

export default getBookTypes;
