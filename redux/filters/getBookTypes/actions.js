import * as types from './types';

export const getBookTypes = payload => {
  return {
    type: types.FILTERS_GETBOOKTYPES,
    payload: payload,
  };
};

export const getBookTypesStart = () => {
  return {
    type: types.FILTERS_GETBOOKTYPES_START,
  };
};

export const getBookTypesSuccess = payload => {
  return {
    type: types.FILTERS_GETBOOKTYPES_SUCCESS,
    payload: payload,
  };
};

export const getBookTypesFail = error => {
  return {
    type: types.FILTERS_GETBOOKTYPES_FAIL,
    error: error,
  };
};