import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import FiltersAPI from '../../../services/filters';

function* getTags(action) {
  yield put(actions.getTagsStart());
  try {
    const response = yield call([FiltersAPI, 'getTags'], action.payload);
    const data = response.data;
    yield put(actions.getTagsSuccess(data));
  } catch (error) {
    yield put(actions.getTagsFail(error));
  }
}

export default getTags;
