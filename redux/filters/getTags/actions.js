import * as types from './types';

export const getTags = payload => {
  return {
    type: types.FILTERS_GETTAGS,
    payload: payload,
  };
};

export const getTagsStart = () => {
  return {
    type: types.FILTERS_GETTAGS_START,
  };
};

export const getTagsSuccess = payload => {
  return {
    type: types.FILTERS_GETTAGS_SUCCESS,
    payload: payload,
  };
};

export const getTagsFail = error => {
  return {
    type: types.FILTERS_GETTAGS_FAIL,
    error: error,
  };
};