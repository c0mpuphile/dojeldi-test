import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getTree from './getTree/saga';
import getFull from './getFull/saga';
// Types
import { CATEGORY_GETTREE } from './getTree/types';
import { CATEGORY_GETFULL } from './getFull/types';

function* watchAll() {
  yield all([
    takeEvery(CATEGORY_GETTREE, getTree),
    takeEvery(CATEGORY_GETFULL, getFull),
  ]);
}

export default watchAll;
