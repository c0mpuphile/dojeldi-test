import * as types from './types';

export const getFull = payload => {
  return {
    type: types.CATEGORY_GETFULL,
    payload: payload,
  };
};

export const getFullStart = () => {
  return {
    type: types.CATEGORY_GETFULL_START,
  };
};

export const getFullSuccess = payload => {
  return {
    type: types.CATEGORY_GETFULL_SUCCESS,
    payload: payload,
  };
};

export const getFullFail = error => {
  return {
    type: types.CATEGORY_GETFULL_FAIL,
    error: error,
  };
};