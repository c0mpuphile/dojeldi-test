import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import CategoryAPI from '../../../services/category';

function* getFull(action) {
  yield put(actions.getFullStart());
  try {
    const response = yield call([CategoryAPI, 'getFull'], action.payload);
    const data = response.data;
    yield put(actions.getFullSuccess(data));
  } catch (error) {
    yield put(actions.getFullFail(error));
  }
}

export default getFull;
