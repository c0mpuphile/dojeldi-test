import { combineReducers } from 'redux';
import getTreeReducer from './getTree/reducer';
import getFullReducer from './getFull/reducer';

const categoryReducer = combineReducers({
  getTree: getTreeReducer,
  getFull: getFullReducer,
});

export default categoryReducer;
