import * as types from './types';

export const getTree = payload => {
  return {
    type: types.CATEGORY_GETTREE,
    payload: payload,
  };
};

export const getTreeStart = () => {
  return {
    type: types.CATEGORY_GETTREE_START,
  };
};

export const getTreeSuccess = payload => {
  return {
    type: types.CATEGORY_GETTREE_SUCCESS,
    payload: payload,
  };
};

export const getTreeFail = error => {
  return {
    type: types.CATEGORY_GETTREE_FAIL,
    error: error,
  };
};