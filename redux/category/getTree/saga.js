import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import CategoryAPI from '../../../services/category';

function* getTree(action) {
  yield put(actions.getTreeStart());
  try {
    const response = yield call([CategoryAPI, 'getTree'], action.payload);
    const data = response.data;
    yield put(actions.getTreeSuccess(data));
  } catch (error) {
    yield put(actions.getTreeFail(error));
  }
}

export default getTree;
