import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getSections from './getSections/saga';
// Types
import { SECTIONS_GETSECTIONS } from './getSections/types';

function* watchAll() {
  yield all([
    takeEvery(SECTIONS_GETSECTIONS, getSections),
  ]);
}

export default watchAll;
