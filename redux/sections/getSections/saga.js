import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import SectionsAPI from '../../../services/sections';

function* getSections(action) {
  yield put(actions.getSectionsStart());
  try {
    const response = yield call([SectionsAPI, 'getSections'], action.payload);
    const data = response.data;
    yield put(actions.getSectionsSuccess(data));
  } catch (error) {
    yield put(actions.getSectionsFail(error));
  }
}

export default getSections;
