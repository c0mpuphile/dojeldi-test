import * as types from './types';

export const getSections = payload => {
  return {
    type: types.SECTIONS_GETSECTIONS,
    payload: payload,
  };
};

export const getSectionsStart = () => {
  return {
    type: types.SECTIONS_GETSECTIONS_START,
  };
};

export const getSectionsSuccess = payload => {
  return {
    type: types.SECTIONS_GETSECTIONS_SUCCESS,
    payload: payload,
  };
};

export const getSectionsFail = error => {
  return {
    type: types.SECTIONS_GETSECTIONS_FAIL,
    error: error,
  };
};