import { combineReducers } from 'redux';
import getSectionsReducer from './getSections/reducer';

const sectionsReducer = combineReducers({
  getSections: getSectionsReducer,
});

export default sectionsReducer;
