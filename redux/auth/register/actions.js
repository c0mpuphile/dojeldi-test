import * as types from './types';

export const register = payload => {
  return {
    type: types.AUTH_REGISTER,
    payload: payload,
  };
};

export const registerStart = () => {
  return {
    type: types.AUTH_REGISTER_START,
  };
};

export const registerSuccess = payload => {
  return {
    type: types.AUTH_REGISTER_SUCCESS,
    payload: payload,
  };
};

export const registerFail = error => {
  return {
    type: types.AUTH_REGISTER_FAIL,
    error: error,
  };
};