import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import AuthAPI from "../../../services/auth";
import instance from "../../../services/axios";

function* register(action) {
  yield put(actions.registerStart());
  try {
    const response = yield call([AuthAPI, "register"], action.payload);
    const data = response.data;
    yield call([localStorage, "setItem"], "token", data.token);
    yield call([localStorage, "setItem"], "roles", data.roles);
    instance.defaults.headers.common["Authorization"] = "Bearer " + data.token;

    yield put(actions.registerSuccess(data));
  } catch (error) {
    yield put(actions.registerFail(error));
  }
}

export default register;
