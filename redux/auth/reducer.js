import { combineReducers } from 'redux';
import loginReducer from './login/reducer';
import registerReducer from './register/reducer';
import verifyReducer from './verify/reducer';

const authReducer = combineReducers({
  login: loginReducer,
  register: registerReducer,
  verify: verifyReducer,
});

export default authReducer;
