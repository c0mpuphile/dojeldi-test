import * as types from './types';

export const login = payload => {
  return {
    type: types.AUTH_LOGIN,
    payload: payload,
  };
};

export const loginStart = () => {
  return {
    type: types.AUTH_LOGIN_START,
  };
};

export const loginSuccess = payload => {
  return {
    type: types.AUTH_LOGIN_SUCCESS,
    payload: payload,
  };
};

export const loginFail = error => {
  return {
    type: types.AUTH_LOGIN_FAIL,
    error: error,
  };
};