import { put, call, select } from "redux-saga/effects";
import * as actions from "./actions";
import AuthAPI from "../../../services/auth";
import instance from "../../../services/axios";
import CartAPI from "../../../services/cart";
import { addToCart, setCart } from "../../cart/addToCart/actions";

function* login(action) {
  yield put(actions.loginStart());
  try {
    const { cart } = yield select((state) => state.cart.addToCart);
    const response = yield call([AuthAPI, "login"], action.payload);
    const data = response.data;
    let editedCart = [];
    yield call([localStorage, "setItem"], "token", data.token);
    yield call([localStorage, "setItem"], "roles", data.roles);
    instance.defaults.headers.common["Authorization"] = "Bearer " + data.token;
    yield CartAPI.addToCartOnline({ books: cart });
    const cartOnline = yield CartAPI.getCartOnline();
    cartOnline.data.data?.items?.map((item) => {
      for (let i = 0; i < item.quantity; i++) {
        editedCart.push({ id: item.id, type: item.type });
      }
    });
    yield put(setCart(editedCart));
    yield put(actions.loginSuccess(data));
  } catch (error) {
    console.log(error);
    yield put(actions.loginFail(error));
  }
}

export default login;
