import { all, takeEvery } from 'redux-saga/effects';
// Actions
import login from './login/saga';
import register from './register/saga';
import verify from './verify/saga';
// Types
import { AUTH_LOGIN } from './login/types';
import { AUTH_REGISTER } from './register/types';
import { AUTH_VERIFY } from './verify/types';

function* watchAll() {
  yield all([
    takeEvery(AUTH_LOGIN, login),
    takeEvery(AUTH_REGISTER, register),
    takeEvery(AUTH_VERIFY, verify),
  ]);
}

export default watchAll;
