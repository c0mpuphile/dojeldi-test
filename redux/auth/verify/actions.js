import * as types from './types';

export const verify = payload => {
  return {
    type: types.AUTH_VERIFY,
    payload: payload,
  };
};

export const verifyStart = () => {
  return {
    type: types.AUTH_VERIFY_START,
  };
};

export const verifySuccess = payload => {
  return {
    type: types.AUTH_VERIFY_SUCCESS,
    payload: payload,
  };
};

export const verifyFail = error => {
  return {
    type: types.AUTH_VERIFY_FAIL,
    error: error,
  };
};