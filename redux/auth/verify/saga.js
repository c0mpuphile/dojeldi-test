import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import AuthAPI from '../../../services/auth';

function* verify(action) {
  yield put(actions.verifyStart());
  try {
    const response = yield call([AuthAPI, 'verify'], action.payload);
    const data = response.data;
    yield put(actions.verifySuccess(data));
  } catch (error) {
    yield put(actions.verifyFail(error));
  }
}

export default verify;
