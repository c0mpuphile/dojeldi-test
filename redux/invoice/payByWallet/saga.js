import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* payByWallet(action) {
  yield put(actions.payByWalletStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([InvoiceAPI, 'payByWallet'], id, payload);
    const data = response.data;
    yield put(actions.payByWalletSuccess(data));
  } catch (error) {
    yield put(actions.payByWalletFail(error));
  }
}

export default payByWallet;
