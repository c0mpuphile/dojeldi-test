import * as types from './types';

export const payByWallet = payload => {
  return {
    type: types.INVOICE_PAYBYWALLET,
    payload: payload,
  };
};

export const payByWalletStart = () => {
  return {
    type: types.INVOICE_PAYBYWALLET_START,
  };
};

export const payByWalletSuccess = payload => {
  return {
    type: types.INVOICE_PAYBYWALLET_SUCCESS,
    payload: payload,
  };
};

export const payByWalletFail = error => {
  return {
    type: types.INVOICE_PAYBYWALLET_FAIL,
    error: error,
  };
};