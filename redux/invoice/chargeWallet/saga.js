import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* chargeWallet(action) {
  yield put(actions.chargeWalletStart());
  try {
    const response = yield call([InvoiceAPI, 'chargeWallet'], action.payload);
    const data = response.data;
    yield put(actions.chargeWalletSuccess(data));
  } catch (error) {
    yield put(actions.chargeWalletFail(error));
  }
}

export default chargeWallet;
