import * as types from './types';

export const chargeWallet = payload => {
  return {
    type: types.INVOICE_CHARGEWALLET,
    payload: payload,
  };
};

export const chargeWalletStart = () => {
  return {
    type: types.INVOICE_CHARGEWALLET_START,
  };
};

export const chargeWalletSuccess = payload => {
  return {
    type: types.INVOICE_CHARGEWALLET_SUCCESS,
    payload: payload,
  };
};

export const chargeWalletFail = error => {
  return {
    type: types.INVOICE_CHARGEWALLET_FAIL,
    error: error,
  };
};