import { all, takeEvery } from 'redux-saga/effects';
// Actions
import payInvoice from './payInvoice/saga';
import verifyPayment from './verifyPayment/saga';
import paymentMethods from './paymentMethods/saga';
import payByWallet from './payByWallet/saga';
import chargeWallet from './chargeWallet/saga';
import getBalance from './getBalance/saga';
import chargeWalletByCode from './chargeWalletByCode/saga';
import calculateCoupon from './calculateCoupon/saga';
// Types
import { INVOICE_PAYINVOICE } from './payInvoice/types';
import { INVOICE_VERIFYPAYMENT } from './verifyPayment/types';
import { INVOICE_PAYMENTMETHODS } from './paymentMethods/types';
import { INVOICE_PAYBYWALLET } from './payByWallet/types';
import { INVOICE_CHARGEWALLET } from './chargeWallet/types';
import { INVOICE_GETBALANCE } from './getBalance/types';
import { INVOICE_CHARGEWALLETBYCODE } from './chargeWalletByCode/types';
import { INVOICE_CALCULATECOUPON } from './calculateCoupon/types';

function* watchAll() {
  yield all([
    takeEvery(INVOICE_PAYINVOICE, payInvoice),
    takeEvery(INVOICE_VERIFYPAYMENT, verifyPayment),
    takeEvery(INVOICE_PAYMENTMETHODS, paymentMethods),
    takeEvery(INVOICE_PAYBYWALLET, payByWallet),
    takeEvery(INVOICE_CHARGEWALLET, chargeWallet),
    takeEvery(INVOICE_GETBALANCE, getBalance),
    takeEvery(INVOICE_CHARGEWALLETBYCODE, chargeWalletByCode),
    takeEvery(INVOICE_CALCULATECOUPON, calculateCoupon),
  ]);
}

export default watchAll;
