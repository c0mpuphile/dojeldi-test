import { combineReducers } from 'redux';
import payInvoiceReducer from './payInvoice/reducer';
import verifyPaymentReducer from './verifyPayment/reducer';
import paymentMethodsReducer from './paymentMethods/reducer';
import payByWalletReducer from './payByWallet/reducer';
import chargeWalletReducer from './chargeWallet/reducer';
import getBalanceReducer from './getBalance/reducer';
import chargeWalletByCodeReducer from './chargeWalletByCode/reducer';
import calculateCouponReducer from './calculateCoupon/reducer';

const invoiceReducer = combineReducers({
  payInvoice: payInvoiceReducer,
  verifyPayment: verifyPaymentReducer,
  paymentMethods: paymentMethodsReducer,
  payByWallet: payByWalletReducer,
  chargeWallet: chargeWalletReducer,
  getBalance: getBalanceReducer,
  chargeWalletByCode: chargeWalletByCodeReducer,
  calculateCoupon: calculateCouponReducer,
});

export default invoiceReducer;
