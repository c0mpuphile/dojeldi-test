import * as types from './types';

export const calculateCoupon = payload => {
  return {
    type: types.INVOICE_CALCULATECOUPON,
    payload: payload,
  };
};

export const calculateCouponStart = () => {
  return {
    type: types.INVOICE_CALCULATECOUPON_START,
  };
};

export const calculateCouponSuccess = payload => {
  return {
    type: types.INVOICE_CALCULATECOUPON_SUCCESS,
    payload: payload,
  };
};

export const calculateCouponFail = error => {
  return {
    type: types.INVOICE_CALCULATECOUPON_FAIL,
    error: error,
  };
};