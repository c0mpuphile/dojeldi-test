import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* calculateCoupon(action) {
  yield put(actions.calculateCouponStart());
  try {
    const { id, price, ...payload } = action.payload;
    const response = yield call([InvoiceAPI, 'calculateCoupon'], id, price, payload);
    const data = response.data;
    yield put(actions.calculateCouponSuccess(data));
  } catch (error) {
    yield put(actions.calculateCouponFail(error));
  }
}

export default calculateCoupon;
