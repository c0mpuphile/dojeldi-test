import * as types from './types';

const initialState = {
  isLoading: false,
  ok: false,
  data: null,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.INVOICE_CHARGEWALLETBYCODE_START:
      return {...state, isLoading: true, ok: false, data: null, error: null};
    case types.INVOICE_CHARGEWALLETBYCODE_SUCCESS:
      return {...state, ok: true, isLoading: false, data: action.payload};
    case types.INVOICE_CHARGEWALLETBYCODE_FAIL:
      return {...state, isLoading: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;
