import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import InvoiceAPI from "../../../services/invoice";

function* chargeWalletByCode(action) {
  yield put(actions.chargeWalletByCodeStart());
  try {
    const response = yield call(
      [InvoiceAPI, "chargeWalletByCode"],
      action.payload
    );
    const data = response.data;
    yield put(actions.chargeWalletByCodeSuccess(data));
  } catch (error) {
    yield put(actions.chargeWalletByCodeFail(error));
  }
}

export default chargeWalletByCode;
