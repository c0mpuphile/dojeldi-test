import * as types from './types';

export const chargeWalletByCode = payload => {
  return {
    type: types.INVOICE_CHARGEWALLETBYCODE,
    payload: payload,
  };
};

export const chargeWalletByCodeStart = () => {
  return {
    type: types.INVOICE_CHARGEWALLETBYCODE_START,
  };
};

export const chargeWalletByCodeSuccess = payload => {
  return {
    type: types.INVOICE_CHARGEWALLETBYCODE_SUCCESS,
    payload: payload,
  };
};

export const chargeWalletByCodeFail = error => {
  return {
    type: types.INVOICE_CHARGEWALLETBYCODE_FAIL,
    error: error,
  };
};