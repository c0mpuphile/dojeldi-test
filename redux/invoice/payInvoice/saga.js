import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import InvoiceAPI from "../../../services/invoice";

function* payInvoice(action) {
  yield put(actions.payInvoiceStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([InvoiceAPI, "payInvoice"], id, payload);
    const data = response.data;
    yield put(actions.payInvoiceSuccess(data));
  } catch (error) {
    yield put(actions.payInvoiceFail(error));
  }
}

export default payInvoice;
