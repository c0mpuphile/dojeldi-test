import * as types from './types';

export const payInvoice = payload => {
  return {
    type: types.INVOICE_PAYINVOICE,
    payload: payload,
  };
};

export const payInvoiceStart = () => {
  return {
    type: types.INVOICE_PAYINVOICE_START,
  };
};

export const payInvoiceSuccess = payload => {
  return {
    type: types.INVOICE_PAYINVOICE_SUCCESS,
    payload: payload,
  };
};

export const payInvoiceFail = error => {
  return {
    type: types.INVOICE_PAYINVOICE_FAIL,
    error: error,
  };
};