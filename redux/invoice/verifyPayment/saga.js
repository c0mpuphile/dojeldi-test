import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* verifyPayment(action) {
  yield put(actions.verifyPaymentStart());
  try {
    const response = yield call([InvoiceAPI, 'verifyPayment'], action.payload);
    const data = response.data;
    yield put(actions.verifyPaymentSuccess(data));
  } catch (error) {
    yield put(actions.verifyPaymentFail(error));
  }
}

export default verifyPayment;
