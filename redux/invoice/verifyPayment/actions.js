import * as types from './types';

export const verifyPayment = payload => {
  return {
    type: types.INVOICE_VERIFYPAYMENT,
    payload: payload,
  };
};

export const verifyPaymentStart = () => {
  return {
    type: types.INVOICE_VERIFYPAYMENT_START,
  };
};

export const verifyPaymentSuccess = payload => {
  return {
    type: types.INVOICE_VERIFYPAYMENT_SUCCESS,
    payload: payload,
  };
};

export const verifyPaymentFail = error => {
  return {
    type: types.INVOICE_VERIFYPAYMENT_FAIL,
    error: error,
  };
};