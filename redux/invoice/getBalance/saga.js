import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* getBalance(action) {
  yield put(actions.getBalanceStart());
  try {
    const response = yield call([InvoiceAPI, 'getBalance'], action.payload);
    const data = response.data;
    yield put(actions.getBalanceSuccess(data));
  } catch (error) {
    yield put(actions.getBalanceFail(error));
  }
}

export default getBalance;
