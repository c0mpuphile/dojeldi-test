import * as types from './types';

export const getBalance = payload => {
  return {
    type: types.INVOICE_GETBALANCE,
    payload: payload,
  };
};

export const getBalanceStart = () => {
  return {
    type: types.INVOICE_GETBALANCE_START,
  };
};

export const getBalanceSuccess = payload => {
  return {
    type: types.INVOICE_GETBALANCE_SUCCESS,
    payload: payload,
  };
};

export const getBalanceFail = error => {
  return {
    type: types.INVOICE_GETBALANCE_FAIL,
    error: error,
  };
};