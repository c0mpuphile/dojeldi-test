import * as types from './types';

export const paymentMethods = payload => {
  return {
    type: types.INVOICE_PAYMENTMETHODS,
    payload: payload,
  };
};

export const paymentMethodsStart = () => {
  return {
    type: types.INVOICE_PAYMENTMETHODS_START,
  };
};

export const paymentMethodsSuccess = payload => {
  return {
    type: types.INVOICE_PAYMENTMETHODS_SUCCESS,
    payload: payload,
  };
};

export const paymentMethodsFail = error => {
  return {
    type: types.INVOICE_PAYMENTMETHODS_FAIL,
    error: error,
  };
};