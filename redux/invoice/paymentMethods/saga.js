import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import InvoiceAPI from '../../../services/invoice';

function* paymentMethods(action) {
  yield put(actions.paymentMethodsStart());
  try {
    const response = yield call([InvoiceAPI, 'paymentMethods'], action.payload);
    const data = response.data;
    yield put(actions.paymentMethodsSuccess(data));
  } catch (error) {
    yield put(actions.paymentMethodsFail(error));
  }
}

export default paymentMethods;
