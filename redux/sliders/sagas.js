import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getSliders from './getSliders/saga';
// Types
import { SLIDERS_GETSLIDERS } from './getSliders/types';

function* watchAll() {
  yield all([
    takeEvery(SLIDERS_GETSLIDERS, getSliders),
  ]);
}

export default watchAll;
