import { combineReducers } from 'redux';
import getSlidersReducer from './getSliders/reducer';

const slidersReducer = combineReducers({
  getSliders: getSlidersReducer,
});

export default slidersReducer;
