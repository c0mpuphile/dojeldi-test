import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import SlidersAPI from '../../../services/sliders';

function* getSliders(action) {
  yield put(actions.getSlidersStart());
  try {
    const response = yield call([SlidersAPI, 'getSliders'], action.payload);
    const data = response.data;
    yield put(actions.getSlidersSuccess(data));
  } catch (error) {
    yield put(actions.getSlidersFail(error));
  }
}

export default getSliders;
