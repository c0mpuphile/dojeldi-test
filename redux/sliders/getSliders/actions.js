import * as types from './types';

export const getSliders = payload => {
  return {
    type: types.SLIDERS_GETSLIDERS,
    payload: payload,
  };
};

export const getSlidersStart = () => {
  return {
    type: types.SLIDERS_GETSLIDERS_START,
  };
};

export const getSlidersSuccess = payload => {
  return {
    type: types.SLIDERS_GETSLIDERS_SUCCESS,
    payload: payload,
  };
};

export const getSlidersFail = error => {
  return {
    type: types.SLIDERS_GETSLIDERS_FAIL,
    error: error,
  };
};