import * as types from './types';

export const rateBook = payload => {
  return {
    type: types.BOOK_RATEBOOK,
    payload: payload,
  };
};

export const rateBookStart = () => {
  return {
    type: types.BOOK_RATEBOOK_START,
  };
};

export const rateBookSuccess = payload => {
  return {
    type: types.BOOK_RATEBOOK_SUCCESS,
    payload: payload,
  };
};

export const rateBookFail = error => {
  return {
    type: types.BOOK_RATEBOOK_FAIL,
    error: error,
  };
};