import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BookAPI from '../../../services/book';

function* rateBook(action) {
  yield put(actions.rateBookStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([BookAPI, 'rateBook'], id, payload);
    const data = response.data;
    yield put(actions.rateBookSuccess(data));
  } catch (error) {
    yield put(actions.rateBookFail(error));
  }
}

export default rateBook;
