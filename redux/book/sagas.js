import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getBook from './getBook/saga';
import likeBook from './likeBook/saga';
import likeRate from './likeRate/saga';
import rateBook from './rateBook/saga';
import commentToRate from './commentToRate/saga';
// Types
import { BOOK_GETBOOK } from './getBook/types';
import { BOOK_LIKEBOOK } from './likeBook/types';
import { BOOK_LIKERATE } from './likeRate/types';
import { BOOK_RATEBOOK } from './rateBook/types';
import { BOOK_COMMENTTORATE } from './commentToRate/types';

function* watchAll() {
  yield all([
    takeEvery(BOOK_GETBOOK, getBook),
    takeEvery(BOOK_LIKEBOOK, likeBook),
    takeEvery(BOOK_LIKERATE, likeRate),
    takeEvery(BOOK_RATEBOOK, rateBook),
    takeEvery(BOOK_COMMENTTORATE, commentToRate),
  ]);
}

export default watchAll;
