import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BookAPI from '../../../services/book';

function* likeBook(action) {
  yield put(actions.likeBookStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([BookAPI, 'likeBook'], id, payload);
    const data = response.data;
    yield put(actions.likeBookSuccess(data));
  } catch (error) {
    yield put(actions.likeBookFail(error));
  }
}

export default likeBook;
