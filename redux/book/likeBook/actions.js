import * as types from './types';

export const likeBook = payload => {
  return {
    type: types.BOOK_LIKEBOOK,
    payload: payload,
  };
};

export const likeBookStart = () => {
  return {
    type: types.BOOK_LIKEBOOK_START,
  };
};

export const likeBookSuccess = payload => {
  return {
    type: types.BOOK_LIKEBOOK_SUCCESS,
    payload: payload,
  };
};

export const likeBookFail = error => {
  return {
    type: types.BOOK_LIKEBOOK_FAIL,
    error: error,
  };
};