import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BookAPI from '../../../services/book';

function* getBook(action) {
  yield put(actions.getBookStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([BookAPI, 'getBook'], id, payload);
    const data = response.data;
    yield put(actions.getBookSuccess(data));
  } catch (error) {
    yield put(actions.getBookFail(error));
  }
}

export default getBook;
