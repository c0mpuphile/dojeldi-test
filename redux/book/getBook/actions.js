import * as types from './types';

export const getBook = payload => {
  return {
    type: types.BOOK_GETBOOK,
    payload: payload,
  };
};

export const getBookStart = () => {
  return {
    type: types.BOOK_GETBOOK_START,
  };
};

export const getBookSuccess = payload => {
  return {
    type: types.BOOK_GETBOOK_SUCCESS,
    payload: payload,
  };
};

export const getBookFail = error => {
  return {
    type: types.BOOK_GETBOOK_FAIL,
    error: error,
  };
};