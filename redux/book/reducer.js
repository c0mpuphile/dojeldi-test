import { combineReducers } from 'redux';
import getBookReducer from './getBook/reducer';
import likeBookReducer from './likeBook/reducer';
import likeRateReducer from './likeRate/reducer';
import rateBookReducer from './rateBook/reducer';
import commentToRateReducer from './commentToRate/reducer';

const bookReducer = combineReducers({
  getBook: getBookReducer,
  likeBook: likeBookReducer,
  likeRate: likeRateReducer,
  rateBook: rateBookReducer,
  commentToRate: commentToRateReducer,
});

export default bookReducer;
