import * as types from './types';

export const commentToRate = payload => {
  return {
    type: types.BOOK_COMMENTTORATE,
    payload: payload,
  };
};

export const commentToRateStart = () => {
  return {
    type: types.BOOK_COMMENTTORATE_START,
  };
};

export const commentToRateSuccess = payload => {
  return {
    type: types.BOOK_COMMENTTORATE_SUCCESS,
    payload: payload,
  };
};

export const commentToRateFail = error => {
  return {
    type: types.BOOK_COMMENTTORATE_FAIL,
    error: error,
  };
};