import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BookAPI from '../../../services/book';

function* commentToRate(action) {
  yield put(actions.commentToRateStart());
  try {
    const { id, rateId, ...payload } = action.payload;
    const response = yield call([BookAPI, 'commentToRate'], id, rateId, payload);
    const data = response.data;
    yield put(actions.commentToRateSuccess(data));
  } catch (error) {
    yield put(actions.commentToRateFail(error));
  }
}

export default commentToRate;
