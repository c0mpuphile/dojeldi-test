import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BookAPI from '../../../services/book';

function* likeRate(action) {
  yield put(actions.likeRateStart());
  try {
    const { id, ...payload } = action.payload;
    const response = yield call([BookAPI, 'likeRate'], id, payload);
    const data = response.data;
    yield put(actions.likeRateSuccess(data));
  } catch (error) {
    yield put(actions.likeRateFail(error));
  }
}

export default likeRate;
