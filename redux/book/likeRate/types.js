export const BOOK_LIKERATE = 'BOOK_LIKERATE';
export const BOOK_LIKERATE_START = 'BOOK_LIKERATE_START';
export const BOOK_LIKERATE_SUCCESS = 'BOOK_LIKERATE_SUCCESS';
export const BOOK_LIKERATE_FAIL = 'BOOK_LIKERATE_FAIL';