import * as types from './types';

export const likeRate = payload => {
  return {
    type: types.BOOK_LIKERATE,
    payload: payload,
  };
};

export const likeRateStart = () => {
  return {
    type: types.BOOK_LIKERATE_START,
  };
};

export const likeRateSuccess = payload => {
  return {
    type: types.BOOK_LIKERATE_SUCCESS,
    payload: payload,
  };
};

export const likeRateFail = error => {
  return {
    type: types.BOOK_LIKERATE_FAIL,
    error: error,
  };
};