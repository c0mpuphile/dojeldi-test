import * as types from './types';

export const getLocations = payload => {
  return {
    type: types.LOCATIONS_GETLOCATIONS,
    payload: payload,
  };
};

export const getLocationsStart = () => {
  return {
    type: types.LOCATIONS_GETLOCATIONS_START,
  };
};

export const getLocationsSuccess = payload => {
  return {
    type: types.LOCATIONS_GETLOCATIONS_SUCCESS,
    payload: payload,
  };
};

export const getLocationsFail = error => {
  return {
    type: types.LOCATIONS_GETLOCATIONS_FAIL,
    error: error,
  };
};