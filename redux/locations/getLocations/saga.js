import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import LocationsAPI from '../../../services/locations';

function* getLocations(action) {
  yield put(actions.getLocationsStart());
  try {
    const response = yield call([LocationsAPI, 'getLocations'], action.payload);
    const data = response.data;
    yield put(actions.getLocationsSuccess(data));
  } catch (error) {
    yield put(actions.getLocationsFail(error));
  }
}

export default getLocations;
