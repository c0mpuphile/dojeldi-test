import { combineReducers } from 'redux';
import getLocationsReducer from './getLocations/reducer';

const locationsReducer = combineReducers({
  getLocations: getLocationsReducer,
});

export default locationsReducer;
