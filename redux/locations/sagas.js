import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getLocations from './getLocations/saga';
// Types
import { LOCATIONS_GETLOCATIONS } from './getLocations/types';

function* watchAll() {
  yield all([
    takeEvery(LOCATIONS_GETLOCATIONS, getLocations),
  ]);
}

export default watchAll;
