export const BANNERS_GETBANNERS = 'BANNERS_GETBANNERS';
export const BANNERS_GETBANNERS_START = 'BANNERS_GETBANNERS_START';
export const BANNERS_GETBANNERS_SUCCESS = 'BANNERS_GETBANNERS_SUCCESS';
export const BANNERS_GETBANNERS_FAIL = 'BANNERS_GETBANNERS_FAIL';