import * as types from './types';

export const getBanners = payload => {
  return {
    type: types.BANNERS_GETBANNERS,
    payload: payload,
  };
};

export const getBannersStart = () => {
  return {
    type: types.BANNERS_GETBANNERS_START,
  };
};

export const getBannersSuccess = payload => {
  return {
    type: types.BANNERS_GETBANNERS_SUCCESS,
    payload: payload,
  };
};

export const getBannersFail = error => {
  return {
    type: types.BANNERS_GETBANNERS_FAIL,
    error: error,
  };
};