import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import BannersAPI from '../../../services/banners';

function* getBanners(action) {
  yield put(actions.getBannersStart());
  try {
    const response = yield call([BannersAPI, 'getBanners'], action.payload);
    const data = response.data;
    yield put(actions.getBannersSuccess(data));
  } catch (error) {
    yield put(actions.getBannersFail(error));
  }
}

export default getBanners;
