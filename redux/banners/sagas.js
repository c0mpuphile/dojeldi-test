import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getBanners from './getBanners/saga';
// Types
import { BANNERS_GETBANNERS } from './getBanners/types';

function* watchAll() {
  yield all([
    takeEvery(BANNERS_GETBANNERS, getBanners),
  ]);
}

export default watchAll;
