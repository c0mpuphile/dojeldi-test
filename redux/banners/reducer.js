import { combineReducers } from 'redux';
import getBannersReducer from './getBanners/reducer';

const bannersReducer = combineReducers({
  getBanners: getBannersReducer,
});

export default bannersReducer;
