import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getUserTransactions from './getUserTransactions/saga';
import getPublisherTransactions from './getPublisherTransactions/saga';
// Types
import { TRANSACTIONS_GETUSERTRANSACTIONS } from './getUserTransactions/types';
import { TRANSACTIONS_GETPUBLISHERTRANSACTIONS } from './getPublisherTransactions/types';

function* watchAll() {
  yield all([
    takeEvery(TRANSACTIONS_GETUSERTRANSACTIONS, getUserTransactions),
    takeEvery(TRANSACTIONS_GETPUBLISHERTRANSACTIONS, getPublisherTransactions),
  ]);
}

export default watchAll;
