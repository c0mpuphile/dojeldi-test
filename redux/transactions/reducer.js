import { combineReducers } from 'redux';
import getUserTransactionsReducer from './getUserTransactions/reducer';
import getPublisherTransactionsReducer from './getPublisherTransactions/reducer';

const transactionsReducer = combineReducers({
  getUserTransactions: getUserTransactionsReducer,
  getPublisherTransactions: getPublisherTransactionsReducer,
});

export default transactionsReducer;
