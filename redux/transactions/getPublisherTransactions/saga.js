import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import TransactionsAPI from '../../../services/transactions';

function* getPublisherTransactions(action) {
  yield put(actions.getPublisherTransactionsStart());
  try {
    const response = yield call([TransactionsAPI, 'getPublisherTransactions'], action.payload);
    const data = response.data;
    yield put(actions.getPublisherTransactionsSuccess(data));
  } catch (error) {
    yield put(actions.getPublisherTransactionsFail(error));
  }
}

export default getPublisherTransactions;
