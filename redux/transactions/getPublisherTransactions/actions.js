import * as types from './types';

export const getPublisherTransactions = payload => {
  return {
    type: types.TRANSACTIONS_GETPUBLISHERTRANSACTIONS,
    payload: payload,
  };
};

export const getPublisherTransactionsStart = () => {
  return {
    type: types.TRANSACTIONS_GETPUBLISHERTRANSACTIONS_START,
  };
};

export const getPublisherTransactionsSuccess = payload => {
  return {
    type: types.TRANSACTIONS_GETPUBLISHERTRANSACTIONS_SUCCESS,
    payload: payload,
  };
};

export const getPublisherTransactionsFail = error => {
  return {
    type: types.TRANSACTIONS_GETPUBLISHERTRANSACTIONS_FAIL,
    error: error,
  };
};