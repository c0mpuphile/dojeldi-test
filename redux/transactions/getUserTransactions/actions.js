import * as types from './types';

export const getUserTransactions = payload => {
  return {
    type: types.TRANSACTIONS_GETUSERTRANSACTIONS,
    payload: payload,
  };
};

export const getUserTransactionsStart = () => {
  return {
    type: types.TRANSACTIONS_GETUSERTRANSACTIONS_START,
  };
};

export const getUserTransactionsSuccess = payload => {
  return {
    type: types.TRANSACTIONS_GETUSERTRANSACTIONS_SUCCESS,
    payload: payload,
  };
};

export const getUserTransactionsFail = error => {
  return {
    type: types.TRANSACTIONS_GETUSERTRANSACTIONS_FAIL,
    error: error,
  };
};