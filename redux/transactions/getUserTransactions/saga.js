import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import TransactionsAPI from '../../../services/transactions';

function* getUserTransactions(action) {
  yield put(actions.getUserTransactionsStart());
  try {
    const response = yield call([TransactionsAPI, 'getUserTransactions'], action.payload);
    const data = response.data;
    yield put(actions.getUserTransactionsSuccess(data));
  } catch (error) {
    yield put(actions.getUserTransactionsFail(error));
  }
}

export default getUserTransactions;
