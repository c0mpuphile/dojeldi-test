import * as types from './types';

export const getElectronicBooks = payload => {
  return {
    type: types.PROFILE_GETELECTRONICBOOKS,
    payload: payload,
  };
};

export const getElectronicBooksStart = () => {
  return {
    type: types.PROFILE_GETELECTRONICBOOKS_START,
  };
};

export const getElectronicBooksSuccess = payload => {
  return {
    type: types.PROFILE_GETELECTRONICBOOKS_SUCCESS,
    payload: payload,
  };
};

export const getElectronicBooksFail = error => {
  return {
    type: types.PROFILE_GETELECTRONICBOOKS_FAIL,
    error: error,
  };
};