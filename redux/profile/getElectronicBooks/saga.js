import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getElectronicBooks(action) {
  yield put(actions.getElectronicBooksStart());
  try {
    const response = yield call([ProfileAPI, 'getElectronicBooks'], action.payload);
    const data = response.data;
    yield put(actions.getElectronicBooksSuccess(data));
  } catch (error) {
    yield put(actions.getElectronicBooksFail(error));
  }
}

export default getElectronicBooks;
