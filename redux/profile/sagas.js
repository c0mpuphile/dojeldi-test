import { all, takeEvery } from 'redux-saga/effects';
// Actions
import getProfile from './getProfile/saga';
import getGenders from './getGenders/saga';
import editProfile from './editProfile/saga';
import uploadProfilePic from './uploadProfilePic/saga';
import addAddress from './addAddress/saga';
import getAddress from './getAddress/saga';
import editAddress from './editAddress/saga';
import getAudioBooks from './getAudioBooks/saga';
import getPrintBooks from './getPrintBooks/saga';
import getElectronicBooks from './getElectronicBooks/saga';
import getOrders from './getOrders/saga';
import getIntrests from './getIntrests/saga';
// Types
import { PROFILE_GETPROFILE } from './getProfile/types';
import { PROFILE_GETGENDERS } from './getGenders/types';
import { PROFILE_EDITPROFILE } from './editProfile/types';
import { PROFILE_UPLOADPROFILEPIC } from './uploadProfilePic/types';
import { PROFILE_ADDADDRESS } from './addAddress/types';
import { PROFILE_GETADDRESS } from './getAddress/types';
import { PROFILE_EDITADDRESS } from './editAddress/types';
import { PROFILE_GETAUDIOBOOKS } from './getAudioBooks/types';
import { PROFILE_GETPRINTBOOKS } from './getPrintBooks/types';
import { PROFILE_GETELECTRONICBOOKS } from './getElectronicBooks/types';
import { PROFILE_GETORDERS } from './getOrders/types';
import { PROFILE_GETINTRESTS } from './getIntrests/types';

function* watchAll() {
  yield all([
    takeEvery(PROFILE_GETPROFILE, getProfile),
    takeEvery(PROFILE_GETGENDERS, getGenders),
    takeEvery(PROFILE_EDITPROFILE, editProfile),
    takeEvery(PROFILE_UPLOADPROFILEPIC, uploadProfilePic),
    takeEvery(PROFILE_ADDADDRESS, addAddress),
    takeEvery(PROFILE_GETADDRESS, getAddress),
    takeEvery(PROFILE_EDITADDRESS, editAddress),
    takeEvery(PROFILE_GETAUDIOBOOKS, getAudioBooks),
    takeEvery(PROFILE_GETPRINTBOOKS, getPrintBooks),
    takeEvery(PROFILE_GETELECTRONICBOOKS, getElectronicBooks),
    takeEvery(PROFILE_GETORDERS, getOrders),
    takeEvery(PROFILE_GETINTRESTS, getIntrests),
  ]);
}

export default watchAll;
