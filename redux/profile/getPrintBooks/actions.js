import * as types from './types';

export const getPrintBooks = payload => {
  return {
    type: types.PROFILE_GETPRINTBOOKS,
    payload: payload,
  };
};

export const getPrintBooksStart = () => {
  return {
    type: types.PROFILE_GETPRINTBOOKS_START,
  };
};

export const getPrintBooksSuccess = payload => {
  return {
    type: types.PROFILE_GETPRINTBOOKS_SUCCESS,
    payload: payload,
  };
};

export const getPrintBooksFail = error => {
  return {
    type: types.PROFILE_GETPRINTBOOKS_FAIL,
    error: error,
  };
};