import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getPrintBooks(action) {
  yield put(actions.getPrintBooksStart());
  try {
    const response = yield call([ProfileAPI, 'getPrintBooks'], action.payload);
    const data = response.data;
    yield put(actions.getPrintBooksSuccess(data));
  } catch (error) {
    yield put(actions.getPrintBooksFail(error));
  }
}

export default getPrintBooks;
