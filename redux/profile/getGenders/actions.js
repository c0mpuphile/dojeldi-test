import * as types from './types';

export const getGenders = payload => {
  return {
    type: types.PROFILE_GETGENDERS,
    payload: payload,
  };
};

export const getGendersStart = () => {
  return {
    type: types.PROFILE_GETGENDERS_START,
  };
};

export const getGendersSuccess = payload => {
  return {
    type: types.PROFILE_GETGENDERS_SUCCESS,
    payload: payload,
  };
};

export const getGendersFail = error => {
  return {
    type: types.PROFILE_GETGENDERS_FAIL,
    error: error,
  };
};