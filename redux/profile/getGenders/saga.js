import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getGenders(action) {
  yield put(actions.getGendersStart());
  try {
    const response = yield call([ProfileAPI, 'getGenders'], action.payload);
    const data = response.data;
    yield put(actions.getGendersSuccess(data));
  } catch (error) {
    yield put(actions.getGendersFail(error));
  }
}

export default getGenders;
