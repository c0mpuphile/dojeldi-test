export const PROFILE_GETGENDERS = 'PROFILE_GETGENDERS';
export const PROFILE_GETGENDERS_START = 'PROFILE_GETGENDERS_START';
export const PROFILE_GETGENDERS_SUCCESS = 'PROFILE_GETGENDERS_SUCCESS';
export const PROFILE_GETGENDERS_FAIL = 'PROFILE_GETGENDERS_FAIL';