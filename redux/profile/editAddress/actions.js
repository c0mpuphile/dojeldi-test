import * as types from './types';

export const editAddress = payload => {
  return {
    type: types.PROFILE_EDITADDRESS,
    payload: payload,
  };
};

export const editAddressStart = () => {
  return {
    type: types.PROFILE_EDITADDRESS_START,
  };
};

export const editAddressSuccess = payload => {
  return {
    type: types.PROFILE_EDITADDRESS_SUCCESS,
    payload: payload,
  };
};

export const editAddressFail = error => {
  return {
    type: types.PROFILE_EDITADDRESS_FAIL,
    error: error,
  };
};