import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import ProfileAPI from "../../../services/profile";

function* editAddress(action) {
  yield put(actions.editAddressStart());
  try {
    console.log(action.payload);
    const { address_id, ...payload } = action.payload;
    const response = yield call(
      [ProfileAPI, "editAddress"],
      address_id,
      payload
    );
    const data = response.data;
    yield put(actions.editAddressSuccess(data));
  } catch (error) {
    yield put(actions.editAddressFail(error));
  }
}

export default editAddress;
