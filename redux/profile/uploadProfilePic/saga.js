import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* uploadProfilePic(action) {
  yield put(actions.uploadProfilePicStart());
  try {
    const response = yield call([ProfileAPI, 'uploadProfilePic'], action.payload);
    const data = response.data;
    yield put(actions.uploadProfilePicSuccess(data));
  } catch (error) {
    yield put(actions.uploadProfilePicFail(error));
  }
}

export default uploadProfilePic;
