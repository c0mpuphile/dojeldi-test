import * as types from './types';

export const uploadProfilePic = payload => {
  return {
    type: types.PROFILE_UPLOADPROFILEPIC,
    payload: payload,
  };
};

export const uploadProfilePicStart = () => {
  return {
    type: types.PROFILE_UPLOADPROFILEPIC_START,
  };
};

export const uploadProfilePicSuccess = payload => {
  return {
    type: types.PROFILE_UPLOADPROFILEPIC_SUCCESS,
    payload: payload,
  };
};

export const uploadProfilePicFail = error => {
  return {
    type: types.PROFILE_UPLOADPROFILEPIC_FAIL,
    error: error,
  };
};