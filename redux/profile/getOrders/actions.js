import * as types from './types';

export const getOrders = payload => {
  return {
    type: types.PROFILE_GETORDERS,
    payload: payload,
  };
};

export const getOrdersStart = () => {
  return {
    type: types.PROFILE_GETORDERS_START,
  };
};

export const getOrdersSuccess = payload => {
  return {
    type: types.PROFILE_GETORDERS_SUCCESS,
    payload: payload,
  };
};

export const getOrdersFail = error => {
  return {
    type: types.PROFILE_GETORDERS_FAIL,
    error: error,
  };
};