import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getOrders(action) {
  yield put(actions.getOrdersStart());
  try {
    const { status, ...payload } = action.payload;
    const response = yield call([ProfileAPI, 'getOrders'], status, payload);
    const data = response.data;
    yield put(actions.getOrdersSuccess(data));
  } catch (error) {
    yield put(actions.getOrdersFail(error));
  }
}

export default getOrders;
