import * as types from './types';

export const addAddress = payload => {
  return {
    type: types.PROFILE_ADDADDRESS,
    payload: payload,
  };
};

export const addAddressStart = () => {
  return {
    type: types.PROFILE_ADDADDRESS_START,
  };
};

export const addAddressSuccess = payload => {
  return {
    type: types.PROFILE_ADDADDRESS_SUCCESS,
    payload: payload,
  };
};

export const addAddressFail = error => {
  return {
    type: types.PROFILE_ADDADDRESS_FAIL,
    error: error,
  };
};