import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* addAddress(action) {
  yield put(actions.addAddressStart());
  try {
    const response = yield call([ProfileAPI, 'addAddress'], action.payload);
    const data = response.data;
    yield put(actions.addAddressSuccess(data));
  } catch (error) {
    yield put(actions.addAddressFail(error));
  }
}

export default addAddress;
