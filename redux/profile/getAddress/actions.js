import * as types from './types';

export const getAddress = payload => {
  return {
    type: types.PROFILE_GETADDRESS,
    payload: payload,
  };
};

export const getAddressStart = () => {
  return {
    type: types.PROFILE_GETADDRESS_START,
  };
};

export const getAddressSuccess = payload => {
  return {
    type: types.PROFILE_GETADDRESS_SUCCESS,
    payload: payload,
  };
};

export const getAddressFail = error => {
  return {
    type: types.PROFILE_GETADDRESS_FAIL,
    error: error,
  };
};