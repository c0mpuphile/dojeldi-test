import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getAudioBooks(action) {
  yield put(actions.getAudioBooksStart());
  try {
    const response = yield call([ProfileAPI, 'getAudioBooks'], action.payload);
    const data = response.data;
    yield put(actions.getAudioBooksSuccess(data));
  } catch (error) {
    yield put(actions.getAudioBooksFail(error));
  }
}

export default getAudioBooks;
