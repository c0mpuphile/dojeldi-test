import * as types from './types';

export const getAudioBooks = payload => {
  return {
    type: types.PROFILE_GETAUDIOBOOKS,
    payload: payload,
  };
};

export const getAudioBooksStart = () => {
  return {
    type: types.PROFILE_GETAUDIOBOOKS_START,
  };
};

export const getAudioBooksSuccess = payload => {
  return {
    type: types.PROFILE_GETAUDIOBOOKS_SUCCESS,
    payload: payload,
  };
};

export const getAudioBooksFail = error => {
  return {
    type: types.PROFILE_GETAUDIOBOOKS_FAIL,
    error: error,
  };
};