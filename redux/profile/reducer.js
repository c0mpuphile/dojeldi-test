import { combineReducers } from 'redux';
import getProfileReducer from './getProfile/reducer';
import getGendersReducer from './getGenders/reducer';
import editProfileReducer from './editProfile/reducer';
import uploadProfilePicReducer from './uploadProfilePic/reducer';
import addAddressReducer from './addAddress/reducer';
import getAddressReducer from './getAddress/reducer';
import editAddressReducer from './editAddress/reducer';
import getAudioBooksReducer from './getAudioBooks/reducer';
import getPrintBooksReducer from './getPrintBooks/reducer';
import getElectronicBooksReducer from './getElectronicBooks/reducer';
import getOrdersReducer from './getOrders/reducer';
import getIntrestsReducer from './getIntrests/reducer';

const profileReducer = combineReducers({
  getProfile: getProfileReducer,
  getGenders: getGendersReducer,
  editProfile: editProfileReducer,
  uploadProfilePic: uploadProfilePicReducer,
  addAddress: addAddressReducer,
  getAddress: getAddressReducer,
  editAddress: editAddressReducer,
  getAudioBooks: getAudioBooksReducer,
  getPrintBooks: getPrintBooksReducer,
  getElectronicBooks: getElectronicBooksReducer,
  getOrders: getOrdersReducer,
  getIntrests: getIntrestsReducer,
});

export default profileReducer;
