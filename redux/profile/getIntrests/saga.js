import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getIntrests(action) {
  yield put(actions.getIntrestsStart());
  try {
    const response = yield call([ProfileAPI, 'getIntrests'], action.payload);
    const data = response.data;
    yield put(actions.getIntrestsSuccess(data));
  } catch (error) {
    yield put(actions.getIntrestsFail(error));
  }
}

export default getIntrests;
