import * as types from './types';

export const getIntrests = payload => {
  return {
    type: types.PROFILE_GETINTRESTS,
    payload: payload,
  };
};

export const getIntrestsStart = () => {
  return {
    type: types.PROFILE_GETINTRESTS_START,
  };
};

export const getIntrestsSuccess = payload => {
  return {
    type: types.PROFILE_GETINTRESTS_SUCCESS,
    payload: payload,
  };
};

export const getIntrestsFail = error => {
  return {
    type: types.PROFILE_GETINTRESTS_FAIL,
    error: error,
  };
};