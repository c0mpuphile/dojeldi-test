import * as types from './types';

export const getProfile = payload => {
  return {
    type: types.PROFILE_GETPROFILE,
    payload: payload,
  };
};

export const getProfileStart = () => {
  return {
    type: types.PROFILE_GETPROFILE_START,
  };
};

export const getProfileSuccess = payload => {
  return {
    type: types.PROFILE_GETPROFILE_SUCCESS,
    payload: payload,
  };
};

export const getProfileFail = error => {
  return {
    type: types.PROFILE_GETPROFILE_FAIL,
    error: error,
  };
};