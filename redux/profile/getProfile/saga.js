import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* getProfile(action) {
  yield put(actions.getProfileStart());
  try {
    const response = yield call([ProfileAPI, 'getProfile'], action.payload);
    const data = response.data;
    yield put(actions.getProfileSuccess(data));
  } catch (error) {
    yield put(actions.getProfileFail(error));
  }
}

export default getProfile;
