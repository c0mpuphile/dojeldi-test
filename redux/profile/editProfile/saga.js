import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import ProfileAPI from '../../../services/profile';

function* editProfile(action) {
  yield put(actions.editProfileStart());
  try {
    const response = yield call([ProfileAPI, 'editProfile'], action.payload);
    const data = response.data;
    yield put(actions.editProfileSuccess(data));
  } catch (error) {
    yield put(actions.editProfileFail(error));
  }
}

export default editProfile;
