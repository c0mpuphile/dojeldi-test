import * as types from './types';

export const editProfile = payload => {
  return {
    type: types.PROFILE_EDITPROFILE,
    payload: payload,
  };
};

export const editProfileStart = () => {
  return {
    type: types.PROFILE_EDITPROFILE_START,
  };
};

export const editProfileSuccess = payload => {
  return {
    type: types.PROFILE_EDITPROFILE_SUCCESS,
    payload: payload,
  };
};

export const editProfileFail = error => {
  return {
    type: types.PROFILE_EDITPROFILE_FAIL,
    error: error,
  };
};