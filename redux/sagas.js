import { all } from 'redux-saga/effects';
import watchAuth from './auth/sagas';
import watchCategory from './category/sagas';
import watchSliders from './sliders/sagas';
import watchBanners from './banners/sagas';
import watchSections from './sections/sagas';
import watchLocations from './locations/sagas';
import watchCart from './cart/sagas';
import watchBook from './book/sagas';
import watchSearch from './search/sagas';
import watchFilters from './filters/sagas';
import watchProfile from './profile/sagas';
import watchInvoice from './invoice/sagas';
import watchTransactions from './transactions/sagas';

function* watchAll() {
  yield all([
    watchAuth(),
    watchCategory(),
    watchSliders(),
    watchBanners(),
    watchSections(),
    watchLocations(),
    watchCart(),
    watchBook(),
    watchSearch(),
    watchFilters(),
    watchProfile(),
    watchInvoice(),
    watchTransactions(),
  ]);
}

export default watchAll;
