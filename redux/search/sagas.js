import { all, takeEvery } from 'redux-saga/effects';
// Actions
import searchFull from './searchFull/saga';
// Types
import { SEARCH_SEARCHFULL } from './searchFull/types';

function* watchAll() {
  yield all([
    takeEvery(SEARCH_SEARCHFULL, searchFull),
  ]);
}

export default watchAll;
