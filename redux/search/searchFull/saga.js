import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import SearchAPI from "../../../services/search";

function* searchFull(action) {
  yield put(actions.searchFullStart());
  try {
    const response = yield call([SearchAPI, "searchFull"], action.payload);

    const data = response.data;
    yield put(actions.searchFullSuccess(data));
  } catch (error) {
    yield put(actions.searchFullFail(error));
  }
}

export default searchFull;
