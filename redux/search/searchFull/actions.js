import * as types from './types';

export const searchFull = payload => {
  return {
    type: types.SEARCH_SEARCHFULL,
    payload: payload,
  };
};

export const searchFullStart = () => {
  return {
    type: types.SEARCH_SEARCHFULL_START,
  };
};

export const searchFullSuccess = payload => {
  return {
    type: types.SEARCH_SEARCHFULL_SUCCESS,
    payload: payload,
  };
};

export const searchFullFail = error => {
  return {
    type: types.SEARCH_SEARCHFULL_FAIL,
    error: error,
  };
};