import { combineReducers } from 'redux';
import searchFullReducer from './searchFull/reducer';

const searchReducer = combineReducers({
  searchFull: searchFullReducer,
});

export default searchReducer;
