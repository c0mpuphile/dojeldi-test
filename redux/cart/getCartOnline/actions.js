import * as types from './types';

export const getCartOnline = payload => {
  return {
    type: types.CART_GETCARTONLINE,
    payload: payload,
  };
};

export const getCartOnlineStart = () => {
  return {
    type: types.CART_GETCARTONLINE_START,
  };
};

export const getCartOnlineSuccess = payload => {
  return {
    type: types.CART_GETCARTONLINE_SUCCESS,
    payload: payload,
  };
};

export const getCartOnlineFail = error => {
  return {
    type: types.CART_GETCARTONLINE_FAIL,
    error: error,
  };
};