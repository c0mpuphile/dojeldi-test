import * as types from './types';

const initialState = {
  isLoading: false,
  ok: false,
  data: null,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CART_GETCARTONLINE_START:
      return {...state, isLoading: true, ok: false, data: null, error: null};
    case types.CART_GETCARTONLINE_SUCCESS:
      return {...state, ok: true, isLoading: false, data: action.payload};
    case types.CART_GETCARTONLINE_FAIL:
      return {...state, isLoading: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;
