import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import CartAPI from "../../../services/cart";
import { setCart } from "../addToCart/actions";

function* getCartOnline(action) {
  yield put(actions.getCartOnlineStart());
  try {
    const response = yield call([CartAPI, "getCartOnline"], action.payload);
    const data = response.data;
    let editedCart = [];
    data.data?.items?.map((item) => {
      for (let i = 0; i < item.quantity; i++) {
        editedCart.push({ id: item.id, type: item.type });
      }
    });
    yield put(setCart(editedCart));
    yield put(actions.getCartOnlineSuccess(data));
  } catch (error) {
    yield put(actions.getCartOnlineFail(error));
  }
}

export default getCartOnline;
