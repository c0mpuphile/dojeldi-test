import { put, call } from "redux-saga/effects";
import * as actions from "./actions";
import CartAPI from "../../../services/cart";

export function* addToCart(action) {
  const token = localStorage.getItem("token");
  if (token) {
    try {
      yield call([CartAPI, "addToCartOnline"], { books: [action.payload] });
    } catch (error) {}
  }
}

export function* removeFromCart(action) {
  const token = localStorage.getItem("token");
  if (token) {
    try {
      yield call([CartAPI, "removeFromCartOnline"], {
        books: [action.payload],
      });
    } catch (error) {}
  }
}
