import * as types from "./types";

const initialState = {
  isLoading: false,
  ok: false,
  data: null,
  error: null,
  cart: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CART:
      return { ...state, cart: action.payload };
    case types.CART_ADDTOCART:
      return { ...state, cart: [...state.cart, action.payload] };
    case types.CART_REMOVEFROMCART:
      return {
        ...state,
        cart: state.cart.splice(
          state.cart.indexOf(
            (item) =>
              item.id != action.payload.id && item.type != action.payload.type
          ),
          1
        ),
      };
    default:
      return state;
  }
};

export default reducer;
