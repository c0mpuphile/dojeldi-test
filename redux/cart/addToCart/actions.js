import * as types from "./types";

export const addToCart = (payload) => {
  return {
    type: types.CART_ADDTOCART,
    payload: payload,
  };
};

export const removeFromCart = (payload) => {
  return {
    type: types.CART_REMOVEFROMCART,
    payload: payload,
  };
};

export const setCart = (payload) => {
  return {
    type: types.SET_CART,
    payload: payload,
  };
};
