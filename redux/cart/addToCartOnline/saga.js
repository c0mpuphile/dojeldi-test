import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import CartAPI from '../../../services/cart';

function* addToCartOnline(action) {
  yield put(actions.addToCartOnlineStart());
  try {
    const response = yield call([CartAPI, 'addToCartOnline'], action.payload);
    const data = response.data;
    yield put(actions.addToCartOnlineSuccess(data));
  } catch (error) {
    yield put(actions.addToCartOnlineFail(error));
  }
}

export default addToCartOnline;
