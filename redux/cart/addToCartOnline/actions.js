import * as types from './types';

export const addToCartOnline = payload => {
  return {
    type: types.CART_ADDTOCARTONLINE,
    payload: payload,
  };
};

export const addToCartOnlineStart = () => {
  return {
    type: types.CART_ADDTOCARTONLINE_START,
  };
};

export const addToCartOnlineSuccess = payload => {
  return {
    type: types.CART_ADDTOCARTONLINE_SUCCESS,
    payload: payload,
  };
};

export const addToCartOnlineFail = error => {
  return {
    type: types.CART_ADDTOCARTONLINE_FAIL,
    error: error,
  };
};