import { all, takeEvery } from "redux-saga/effects";
// Actions
import { addToCart, removeFromCart } from "./addToCart/saga";
import addToCartOnline from "./addToCartOnline/saga";
import getCartOnline from "./getCartOnline/saga";
import removeFromCartOnline from "./removeFromCartOnline/saga";
// Types
import { CART_ADDTOCART } from "./addToCart/types";
import { CART_REMOVEFROMCART } from "./addToCart/types";
import { CART_ADDTOCARTONLINE } from "./addToCartOnline/types";
import { CART_GETCARTONLINE } from "./getCartOnline/types";
import { CART_REMOVEFROMCARTONLINE } from "./removeFromCartOnline/types";

function* watchAll() {
  yield all([
    takeEvery(CART_ADDTOCART, addToCart),
    takeEvery(CART_REMOVEFROMCART, removeFromCart),
    takeEvery(CART_ADDTOCARTONLINE, addToCartOnline),
    takeEvery(CART_GETCARTONLINE, getCartOnline),
    takeEvery(CART_REMOVEFROMCARTONLINE, removeFromCartOnline),
  ]);
}

export default watchAll;
