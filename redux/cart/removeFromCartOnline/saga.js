import { put, call } from 'redux-saga/effects';
import * as actions from './actions';
import CartAPI from '../../../services/cart';

function* removeFromCartOnline(action) {
  yield put(actions.removeFromCartOnlineStart());
  try {
    const response = yield call([CartAPI, 'removeFromCartOnline'], action.payload);
    const data = response.data;
    yield put(actions.removeFromCartOnlineSuccess(data));
  } catch (error) {
    yield put(actions.removeFromCartOnlineFail(error));
  }
}

export default removeFromCartOnline;
