import * as types from './types';

export const removeFromCartOnline = payload => {
  return {
    type: types.CART_REMOVEFROMCARTONLINE,
    payload: payload,
  };
};

export const removeFromCartOnlineStart = () => {
  return {
    type: types.CART_REMOVEFROMCARTONLINE_START,
  };
};

export const removeFromCartOnlineSuccess = payload => {
  return {
    type: types.CART_REMOVEFROMCARTONLINE_SUCCESS,
    payload: payload,
  };
};

export const removeFromCartOnlineFail = error => {
  return {
    type: types.CART_REMOVEFROMCARTONLINE_FAIL,
    error: error,
  };
};