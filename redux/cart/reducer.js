import { combineReducers } from "redux";
import addToCartReducer from "./addToCart/reducer";
import addToCartOnlineReducer from "./addToCartOnline/reducer";
import getCartOnlineReducer from "./getCartOnline/reducer";
import removeFromCartOnlineReducer from "./removeFromCartOnline/reducer";

const cartReducer = combineReducers({
  addToCart: addToCartReducer,
  addToCartOnline: addToCartOnlineReducer,
  getCartOnline: getCartOnlineReducer,
  removeFromCartOnline: removeFromCartOnlineReducer,
});

export default cartReducer;
