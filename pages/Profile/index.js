import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Hidden,
  Icon,
  IconButton,
  Link,
  makeStyles,
  MenuItem,
  Paper,
  Select,
  Step,
  StepButton,
  StepConnector,
  StepLabel,
  Stepper,
  SvgIcon,
  Tab,
  Tabs,
  TextareaAutosize,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import MomentUtils from "@date-io/moment";
import {
  AddCircleOutline,
  CheckCircleOutline,
  Close,
  Delete,
  FileCopy,
  Headset,
  InfoOutlined,
  MoreVert,
  PlayArrow,
  Settings,
} from "@material-ui/icons";
import { useEffect, useState } from "react";
import MyAddresses from "../components/ProfileComponents/MyAddresses";
import MyBookRequests from "../components/ProfileComponents/MyBookRequests";
import MyBooks from "../components/ProfileComponents/MyBooks";
import MyBooksListItem from "../components/ProfileComponents/MyBooksListItem";
import MyFavourits from "../components/ProfileComponents/MyFavourits";
import MyOrders from "../components/ProfileComponents/MyOrders";
import MyTickets from "../components/ProfileComponents/MyTickets";
import MyTransactions from "../components/ProfileComponents/MyTransactions";
import ProfileDetails from "../components/ProfileComponents/ProfileDetailsComponenet";
import ResumeBook from "../components/ProfileComponents/ResumeBookComponent";
import MyMessages from "../components/ProfileComponents/MyMessages";

import Header from "../components/PublicComponents/Header";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import Redirect from "../components/PublicComponents/Redirect";
import Router from "next/router";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

export default function Profile(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [tab, setTab] = useState(false);
  const [changeAddress, setChangeAddress] = useState(false);

  useEffect(() => {
    if (
      localStorage.getItem("token") === null ||
      !localStorage.getItem("roles").includes("is-user")
    ) {
      Router.push("/Login");
    }
  }, []);
  useEffect(() => {
    if (!tab && matches) {
      setTab(1);
    } else if (!tab && !matches) {
      setTab(0);
    }
  }, [matches]);
  const handleTabChange = (event, value) => {
    setTab(value);
  };
  const useStyle = makeStyles({
    indicator: {
      backgroundColor: "#202439",
    },
    scroll: {
      maxWidth: "100vw",
    },
  });
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  const classes = useStyle();
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Grid
        container
        style={{ marginTop: matches ? 40 : 16, justifyContent: "center" }}
      >
        <Hidden mdDown>
          <Grid item lg={3} style={{ paddingRight: 32, paddingLeft: 16 }}>
            <ProfileDetails />
          </Grid>
        </Hidden>
        <Grid
          item
          lg={7}
          style={{
            paddingLeft: matches ? 32 : 8,
            paddingRight: matches ? 16 : 8,
            maxWidth: !matches ? "100vw" : null,
          }}
        >
          {/* <Hidden mdDown>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <ResumeBook />
              <ResumeBook />
            </Box>
          </Hidden> */}

          <Box>
            <Tabs
              variant={"scrollable"}
              scrollButtons="off"
              value={tab}
              classes={{
                indicator: classes.indicator,
                scroller: classes.scroll,
              }}
              onChange={handleTabChange}
              style={{ borderBottom: "1px #D7E1EA solid" }}
            >
              <Tab
                style={{
                  display: matches ? "none" : "block",
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="پروفایل"
                {...a11yProps(0)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="کـتابـخانـه"
                {...a11yProps(1)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="سـفارش‌هـا"
                {...a11yProps(2)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="عـلاقمندی‌هـا"
                {...a11yProps(3)}
              />
              {/* <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="تـیکت‌هـا"
                {...a11yProps(4)}
              /> */}
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="آدرس‌هـا"
                {...a11yProps(4)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="کـیف پـول"
                {...a11yProps(5)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="درخواست کتاب"
                {...a11yProps(6)}
              />
              {/* <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="پیام‌ها"
                {...a11yProps(8)}
              /> */}
            </Tabs>
            <Hidden lgUp>
              <TabPanel style={{ marginTop: 24 }} value={tab} index={0}>
                <Box style={{ width: "100%" }}>
                  <ProfileDetails />
                  {/* 
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Box style={{ width: "100%", marginTop: 16 }}>
                      <ResumeBook />
                    </Box>
                    <Box style={{ width: "100%", marginTop: 16 }}>
                      <ResumeBook />
                    </Box>
                  </Box> */}
                </Box>
              </TabPanel>
            </Hidden>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={1}>
              <MyBooks />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={2}>
              <MyOrders />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={3}>
              <MyFavourits />
            </TabPanel>
            {/* <TabPanel style={{ marginTop: 24 }} value={tab} index={4}>
              <MyTickets />
            </TabPanel> */}
            <TabPanel style={{ marginTop: 24 }} value={tab} index={4}>
              <MyAddresses changeAddress={setChangeAddress} />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={5}>
              <MyTransactions />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={6}>
              <MyBookRequests />
            </TabPanel>
            {/* <TabPanel style={{ marginTop: 24 }} value={tab} index={7}>
              <MyMessages />
            </TabPanel> */}
          </Box>
        </Grid>
      </Grid>
      <Dialog open={false} fullWidth>
        <DialogTitle>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Box style={{ visibility: "hidden" }}>asd</Box>
            <Box>
              <Typography
                style={{
                  fontFamily: "ravi black",
                  fontSize: 24,
                  color: "#202439",
                  textAlign: "center",
                }}
              >
                ارسال تیکت جدید
              </Typography>
            </Box>
            <Box>
              <IconButton>
                <Close style={{ color: "#878A9B" }} />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        ></DialogContent>
      </Dialog>
      <Dialog open={false} fullWidth>
        <DialogContent
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
              width: "100%",
              justifyContent: "center",
            }}
          >
            <CheckCircleOutline style={{ fontSize: 96, color: "#2DB67D" }} />
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              margin: "16px 8px",
              justifyContent: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#202439",
              }}
            >
              مبلغ ۱۴۵,۰۰۰ تومان به کیف پول شما اضافه شد
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              margin: "0px 8px",
              justifyContent: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              شارژ کیف پول شما با موفقیت انجام شد. هم اکنون میتوانید از اعتبار
              خود در خرید استفاده کنید
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              margin: "0px 8px",
              justifyContent: "space-between",
              backgroundColor: "#F3F3F5",
              borderRadius: 10,
              padding: 16,
              marginTop: 16,
            }}
          >
            <Box style={{ visibility: "hidden" }}>asdas</Box>
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              کد پیگیری سفارش ۵۹۸۸۴۷۷۲۳۵
            </Typography>
            <IconButton>
              <FileCopy style={{ color: "#202439" }} />
            </IconButton>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              alignItems: "center",
              marginTop: 32,
            }}
          >
            <Button
              style={{
                width: "100%",
                backgroundColor: "#FF5252",
                padding: 16,
                borderRadius: 16,
                margin: "0px 8px",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  color: "white",
                }}
              >
                بازگشت به صفحه اصلی
              </Typography>
            </Button>
            <Button
              style={{
                width: "100%",
                padding: 16,
                borderRadius: 16,
                margin: "0px 8px",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  color: "#878A9B",
                }}
              >
                بازگشت به صفحه پروفایل
              </Typography>
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </Container>
  );
}
