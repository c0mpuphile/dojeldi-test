import { useSnackbar } from "notistack";
import instance from "../services/axios";
import Router from "next/router";

export default function Interceptor(props) {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  instance.interceptors.response.use(
    (response) => {
      if (response?.data?.message) {
        enqueueSnackbar(response?.data?.message, { variant: "success" });
      }
      return response;
    },
    (error) => {
      if (error?.response?.data?.location) {
        window.location = error.response.data.location;
      } else if (error?.response?.status === 401) {
        localStorage.removeItem("token");
      } else if (error?.response?.data?.message) {
        enqueueSnackbar(error.response.data.message, { variant: "error" });
      }
      return Promise.reject(error);
    }
  );
  return <>{props.children}</>;
}
