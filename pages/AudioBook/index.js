import {
  Box,
  Container,
  Divider,
  Grid,
  IconButton,
  Link,
  Slider,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  ArrowBack,
  Forward10Rounded,
  Instagram,
  PlayCircleFilledRounded,
  PlayCircleFilledWhiteOutlined,
  Replay10Rounded,
  SkipNextRounded,
  SkipPreviousRounded,
  Telegram,
  Twitter,
  VolumeOffRounded,
  VolumeUpRounded,
  WhatsApp,
} from "@material-ui/icons";
import Head from "next/head";
import BookSeasonItemList from "../components/AudioBookComponents/BookSeasonItemList";
import MediaPlayer from "../components/AudioBookComponents/MediaPlayer";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainContentHeader from "../components/ProductPageComponents/MainContentHeader";
import MainImage from "../components/ProductPageComponents/MainImage";
import Header from "../components/PublicComponents/Header";

export default function AudioBook(props) {
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa", alignItems: "center" }}
    >
      <Head>محصول</Head>

      <Box>
        <Grid
          container
          spacing={3}
          style={{ justifyContent: "center", width: "100%", margin: 0 }}
        >
          <Grid item lg={3}>
            <MediaPlayer />
          </Grid>
          <Grid item lg={5}>
            <MainContentHeader />
            <Box
              style={{
                overflowY: "auto",
                height: 560,
                marginTop: 32,
                boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
                padding: 24,
                backgroundColor: "white",
                borderRadius: 20,
              }}
            >
              <BookSeasonItemList />
              <BookSeasonItemList />
              <BookSeasonItemList />
              <BookSeasonItemList />
              <BookSeasonItemList />
              <BookSeasonItemList />
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 8,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#878A9B" }}
                >
                  با دوستان خود به اشتراک بگذارید
                </Typography>
                <Box style={{ marginRight: 8 }}>
                  <IconButton>
                    <Twitter style={{ color: "#878A9B", fontSize: 18 }} />
                  </IconButton>
                  <IconButton>
                    <Instagram style={{ color: "#878A9B", fontSize: 18 }} />
                  </IconButton>
                  <IconButton>
                    <WhatsApp style={{ color: "#878A9B", fontSize: 18 }} />
                  </IconButton>
                  <IconButton>
                    <Telegram style={{ color: "#878A9B", fontSize: 18 }} />
                  </IconButton>
                </Box>
              </Box>
              <Box style={{ display: "flex", alignItems: "center" }}>
                <Link
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 16,
                    color: "#1273EB",
                  }}
                >
                  بازگشت به کتابخانه
                </Link>
                <ArrowBack style={{ color: "#1273EB" }} />
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}
