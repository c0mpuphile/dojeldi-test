import {
  Box,
  CircularProgress,
  IconButton,
  makeStyles,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTree } from "../../../redux/category/getTree/actions";
import { BASE_URL } from "../../../services/axios";
import styles from "../../../styles/Home.module.css";
import queryString from "query-string";

import Child from "../Icons/Child";
import HelpBook from "../Icons/HelpBook";
import Language from "../Icons/Language";
import Search from "../Icons/Search";
import Universal from "../Icons/Universal";
import University from "../Icons/University";
import Router from "next/router";

export default function SearchHeader(props) {
  const { error, ok, data, isLoading } = useSelector(
    (state) => state.category.getTree
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getTree());
  }, []);
  const [categories, setCategories] = useState([""]);
  const [q, setQ] = useState(null);
  const useStyle = makeStyles({
    menuSelect: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      padding: 8,
    },
  });
  const classes = useStyle();
  return (
    <Box className={styles.search_container}>
      <Select
        IconComponent={ExpandMore}
        displayEmpty
        className={styles.search_select}
        classes={{ selectMenu: classes.menuSelect }}
        disableUnderline
        onChange={(event) => setCategories([event.target.value])}
        value={categories[0]}
      >
        {data && data?.data && ok ? (
          [{ id: "", name: "همه دسته‌بندی ها", icon: null }, ...data.data].map(
            (item) => (
              <MenuItem value={item.id}>
                {item?.icon ? (
                  <Box>
                    <img src={BASE_URL + "/image/" + item?.icon} />
                  </Box>
                ) : null}
                <Typography
                  variant="h6"
                  style={{ fontFamily: "ravi", marginRight: 8 }}
                >
                  {item?.name}
                </Typography>
              </MenuItem>
            )
          )
        ) : (
          <CircularProgress color="primary" />
        )}
      </Select>
      <input
        onChange={(event) => setQ(event.target.value)}
        value={q}
        className={styles.search_input}
        placeholder={"جستجو بین بیش از 10,000 عنوان کتاب"}
      />
      <IconButton
        onClick={() => {
          Router.push({
            pathname: "/Category",
            query: Object.assign(
              { q: q || "" },
              categories[0] !== "" ? { categories: [...categories, ""] } : {}
            ),
          });
        }}
        style={{ marginLeft: 8 }}
      >
        <Search />
      </IconButton>
    </Box>
  );
}
