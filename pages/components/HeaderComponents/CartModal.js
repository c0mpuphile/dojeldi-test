import {
  Badge,
  Box,
  Button,
  ClickAwayListener,
  Dialog,
  DialogContent,
  Divider,
  Fade,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import instance from "../../../services/axios";
import AnonCartComponentItem from "../ConfirmCartComponent/AnonCartComponentItem";
import Bascket from "../Icons/Bascket";

export default function CartModal(props) {
  const { cart } = useSelector((state) => state.cart.addToCart);
  const [dataCart, setDataCart] = useState([]);
  const [openCart, setOpenCart] = useState(false);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [time, setTime] = useState();
  const router = useRouter();

  const StyledBadge = withStyles((theme) => ({
    badge: {
      border: "2px solid white",
    },
    root: {},
  }))(Badge);
  useEffect(() => {
    clearTimeout(time);
    setTime(
      setTimeout(() => {
        if (openCart && cart) {
          let cartB = cart;
          let body = [];
          while (cartB.length !== 0) {
            const item = cartB[0];
            body.push({
              id: item.id,
              type: item.type,
              quantity: cartB.filter((x) => _.isEqual(x, item)).length,
            });
            cartB = cartB.filter((x) => !_.isEqual(x, item));
          }
          instance
            .post("/cart/calculate", { books: body })
            .then((response) => setDataCart(response.data.data));
        }
      })
    );
  }, [openCart, cart]);
  if (matches) {
    return (
      <ClickAwayListener
        onClickAway={() => {
          openCart ? setOpenCart(false) : null;
        }}
      >
        <Box
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "flex-start",
          }}
        >
          <Box
            style={{
              borderBottom: openCart ? "4px solid #FF5252" : "none",
              marginLeft: matches ? 8 : null,
            }}
          >
            <IconButton
              onClick={() => {
                setOpenCart(true);
                // router.push("/CheckOutProgress");
              }}
            >
              <StyledBadge
                badgeContent={cart.length}
                color="primary"
                overlap="circle"
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
              >
                <Bascket />
              </StyledBadge>
            </IconButton>
          </Box>

          <Fade in={openCart}>
            <Box
              style={{
                boxShadow: "0px 10px 25px rgba(0,0,0,0.15)",
                backgroundColor: "white",
                border: "1px solid #ECEDEF",
                borderRadius: 20,
                position: "absolute",
                maxWidth: 436,
                marginTop: 64,
                overflowY: "auto",
                maxHeight: 380,
              }}
            >
              {dataCart?.items?.books?.map((item) => (
                <>
                  <AnonCartComponentItem data={item} />
                  <Divider style={{ width: "100%" }} />
                </>
              ))}
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "space-between",
                  padding: 16,
                  alignItems: "center",
                }}
              >
                <Box style={{ display: "flex" }}>
                  <Typography
                    variant="h3"
                    style={{ fontFamily: "ravi black", color: "#202439" }}
                  >
                    {dataCart?.payable_price}
                  </Typography>
                  <Typography
                    variant="h6"
                    style={{
                      fontFamily: "ravi medium",
                      color: "#878A9B",
                      marginRight: 4,
                    }}
                  >
                    تومان
                  </Typography>
                </Box>
                <Box>
                  <Button
                    onClick={() => {
                      router.push("/CheckOutProgress");
                    }}
                    style={{ backgroundColor: "#FF5252" }}
                  >
                    <Typography
                      variant="h5"
                      style={{ fontFamily: "ravi medium", color: "white" }}
                    >
                      مشاهده سبد و تکمیل خرید
                    </Typography>
                  </Button>
                </Box>
              </Box>
            </Box>
          </Fade>
        </Box>
      </ClickAwayListener>
    );
  } else {
    return (
      <Box
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "flex-start",
        }}
      >
        <Box
          style={{
            borderBottom: openCart ? "4px solid #FF5252" : "none",
            marginLeft: matches ? 8 : null,
          }}
        >
          <IconButton
            onClick={() => {
              setOpenCart(true);
              // router.push("/CheckOutProgress");
            }}
          >
            <StyledBadge
              badgeContent={cart.length}
              color="primary"
              overlap="circle"
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
            >
              <Bascket />
            </StyledBadge>
          </IconButton>
        </Box>
        <Dialog open={openCart} onBackdropClick={() => setOpenCart(false)}>
          <DialogContent>
            {dataCart?.items?.books?.map((item) => (
              <>
                <AnonCartComponentItem data={item} />
                <Divider style={{ width: "100%" }} />
              </>
            ))}
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                justifyContent: "space-between",
                padding: 16,
                alignItems: "center",
              }}
            >
              <Box style={{ display: "flex" }}>
                <Typography
                  variant="h3"
                  style={{ fontFamily: "ravi black", color: "#202439" }}
                >
                  {dataCart?.payable_price}
                </Typography>
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi medium",
                    color: "#878A9B",
                    marginRight: 4,
                  }}
                >
                  تومان
                </Typography>
              </Box>
              <Box>
                <Button
                  onClick={() => {
                    router.push("/CheckOutProgress");
                  }}
                  style={{ backgroundColor: "#FF5252" }}
                >
                  <Typography
                    variant="h5"
                    style={{ fontFamily: "ravi medium", color: "white" }}
                  >
                    تکمیل خرید
                  </Typography>
                </Button>
              </Box>
            </Box>
          </DialogContent>
        </Dialog>
      </Box>
    );
  }
}
