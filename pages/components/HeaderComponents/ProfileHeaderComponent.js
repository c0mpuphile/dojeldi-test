import {
  Avatar,
  Badge,
  Box,
  Button,
  CircularProgress,
  ClickAwayListener,
  Divider,
  Fade,
  Hidden,
  IconButton,
  Link,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { ArrowDropDown, VpnKey } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styles from "../../../styles/Home.module.css";
import { getProfile } from "../../../redux/profile/getProfile/actions";
import Bascket from "../Icons/Bascket";
import { getCartOnline } from "../../../redux/cart/getCartOnline/actions";
import { useRouter } from "next/router";
import persianJs from "persianjs";
import instance, { BASE_URL } from "../../../services/axios";
import ConfirmCartComponentItem from "../ConfirmCartComponent/ConfirmCartCompoentItem";
import AnonCartComponentItem from "../ConfirmCartComponent/AnonCartComponentItem";
import CartModal from "./CartModal";

export default function ProfileHeader(props) {
  const dispatch = useDispatch();
  const theme = useTheme();
  const { error, ok, data, isLoading } = useSelector(
    (state) => state.profile.getProfile
  );
  const [clickProfile, setClickProfile] = useState(false);
  const router = useRouter();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [login, setLogin] = useState(undefined);

  useEffect(() => {
    setLogin(
      localStorage.getItem("token") && localStorage.getItem("token") !== null
    );
  }, []);
  useEffect(() => {
    if (login) {
      dispatch(getProfile());
      dispatch(getCartOnline());
    }
  }, [login]);

  return (
    <Box className={styles.account_container}>
      <CartModal />
      {login ? (
        <Hidden mdDown>
          {!clickProfile ? <Divider orientation="vertical" flexItem /> : null}
          <ClickAwayListener
            onClickAway={() => {
              clickProfile ? setClickProfile(false) : null;
            }}
          >
            <Box>
              <Box
                onClick={() => {
                  setClickProfile(true);
                }}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  visibility: clickProfile ? "hidden" : null,
                }}
              >
                <Box
                  style={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Box
                    style={{
                      padding: 16,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Avatar
                      src={BASE_URL + "/image/" + props.data?.image}
                      style={{ marginLeft: 8 }}
                    ></Avatar>
                    <Typography
                      style={{
                        fontFamily: "ravi",
                        fontSize: 16,
                        color: "black",
                      }}
                    >
                      {data?.data?.name + " " + data?.data?.surname}
                    </Typography>
                  </Box>
                  <ArrowDropDown style={{ color: "black", marginRight: 24 }} />
                </Box>
              </Box>
              {clickProfile ? (
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    boxShadow: clickProfile
                      ? "0px 10px 25px rgba(0,0,0,0.15)"
                      : null,
                    borderRadius: clickProfile ? 20 : null,
                    position: "absolute",
                    backgroundColor: "white",
                    top: 16,
                  }}
                >
                  <Box
                    style={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Box
                      style={{
                        padding: 16,
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Avatar
                        src={BASE_URL + "/image/" + props.data?.image}
                        style={{ marginLeft: 8 }}
                      ></Avatar>
                      <Typography
                        style={{
                          fontFamily: "ravi",
                          fontSize: 16,
                          color: "black",
                        }}
                      >
                        {data?.data?.name + " " + data?.data?.surname}
                      </Typography>
                    </Box>
                    <ArrowDropDown
                      style={{ color: "black", marginRight: 24 }}
                    />
                  </Box>
                  {clickProfile ? (
                    <Box>
                      <Divider style={{ width: "100%" }} />
                      <Box style={{ padding: 16 }}>
                        <Link href="/Profile">
                          <a
                            style={{
                              color: "#878A9B",
                              fontFamily: "ravi",
                              fontSize: 14,
                            }}
                          >
                            مشاهده پروفایل
                          </a>
                        </Link>
                      </Box>
                      <Box
                        style={{
                          padding: 16,
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Typography
                          style={{
                            color: "#878A9B",
                            fontFamily: "ravi",
                            fontSize: 14,
                          }}
                        >
                          موجودی کیف پول
                        </Typography>
                        <Typography
                          style={{
                            color: "#202439",
                            fontFamily: "ravi medium",
                            fontSize: 14,
                            marginRight: 8,
                          }}
                        >
                          {data?.data?.balance}
                        </Typography>
                      </Box>
                      <Divider style={{ width: "100%" }} />
                      <Box style={{ padding: 16 }}>
                        <a
                          onClick={() => {
                            localStorage.removeItem("token");
                            localStorage.removeItem("address");

                            router.push("/Login");
                          }}
                          style={{
                            color: "#FF5252",
                            fontFamily: "ravi",
                            fontSize: 14,
                          }}
                        >
                          خروج از حساب کاربری
                        </a>
                      </Box>
                    </Box>
                  ) : null}
                </Box>
              ) : null}
            </Box>
          </ClickAwayListener>
        </Hidden>
      ) : (
        <>
          <Divider orientation="vertical" flexItem />
          <IconButton
            style={{ marginRight: matches ? 8 : null }}
            onClick={() => {
              router.push("/Login");
            }}
          >
            <VpnKey />
          </IconButton>
        </>
      )}
    </Box>
  );
}
