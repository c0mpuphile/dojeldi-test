import {
  Box,
  Breadcrumbs,
  Divider,
  Link,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import { useState } from "react";
import MainContentBody from "./MainContentBody";
import MainContentFooter from "./MainContentFooter";
import MainContentHeader from "./MainContentHeader";
import MainContentMobileFooter from "./MainContentMobileFooter";

export default function MainContent(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [showCriticism, setShowCriticism] = useState(false);
  return (
    <Box>
      {matches ? <MainContentHeader data={props.data} /> : null}
      {matches ? <Divider style={{ width: "100%", marginTop: 32 }} /> : null}

      <MainContentBody
        setC={(value) => setShowCriticism(value)}
        data={props.data}
      />
      {matches ? <Divider style={{ width: "100%", marginTop: 32 }} /> : null}

      {matches ? (
        <MainContentFooter data={props.data} />
      ) : (
        <MainContentMobileFooter data={props.data} />
      )}
      {matches ? (
        <Box>
          <img src="/Featurespro.png" width="100%" />
        </Box>
      ) : null}
      {/* hidden content */}
      {showCriticism && props.data?.criticism ? (
        <Box style={{ marginTop: 16 }}>
          <Typography variant="h6" style={{ fontFamily: "ravi medium" }}>
            {props.props.data?.criticism}
          </Typography>
        </Box>
      ) : null}
      {/* hidden content end */}
    </Box>
  );
}
