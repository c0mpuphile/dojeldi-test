import {
  Box,
  Breadcrumbs,
  Divider,
  Typography,
  withStyles,
} from "@material-ui/core";
import { StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import Link from "next/link";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function MainContentHeader(props) {
  return (
    <Box>
      <Breadcrumbs>
        <Link
          href="/"
          variant="body2"
          style={{ fontFamily: "ravi", color: "#878A9B" }}
        >
          دوجلدی
        </Link>
        <Link
          href="/Category"
          variant="body2"
          style={{ fontFamily: "ravi", color: "#878A9B" }}
        >
          دسته‌بندی
        </Link>
        {props.data?.categories?.map((item) => (
          <Link
            href={`/Category?categories=${item?.id}&categories=`}
            variant="body2"
            style={{ fontFamily: "ravi", color: "#878A9B" }}
          >
            {item.title}
          </Link>
        ))}
      </Breadcrumbs>
      <Typography
        variant="h1"
        style={{
          fontFamily: "ravi black",
          color: "#202439",
          marginTop: 16,
        }}
      >
        {props.data?.persian_title}
      </Typography>
      <Box
        style={{
          flexDirection: "row",
          display: "flex",
          marginTop: 4,
        }}
      >
        <Box style={{ display: "flex", flexDirection: "row" }}>
          <Typography
            variant="h6"
            style={{
              fontFamily: "ravi",
              color: "#878A9B",
            }}
          >
            نویسنده:
          </Typography>
          {props.data?.authors?.map((item) => (
            <Link href={`/Category?authors=&authors=${item?.id}`}>
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi",
                  color: "#1273EB",
                }}
              >
                {item.title}
              </Typography>
            </Link>
          ))}
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            margin: "0px 32px",
          }}
        >
          <Typography
            variant="h6"
            style={{
              fontFamily: "ravi",
              color: "#878A9B",
            }}
          >
            مترجم:
          </Typography>
          {props.data?.translators?.map((item) => (
            <Link href={`/Category?translators=&translators=${item?.id}`}>
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi",
                  color: "#1273EB",
                }}
              >
                {item.title}
              </Typography>
            </Link>
          ))}
        </Box>
        <Box style={{ display: "flex", flexDirection: "row" }}>
          <Typography
            variant="h6"
            style={{
              fontFamily: "ravi",
              color: "#878A9B",
            }}
          >
            انتشارات:
          </Typography>
          <Link
            href={`/Category?publishers=&publishers=${props.data?.publisher?.id}`}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi",
                color: "#1273EB",
              }}
            >
              {props.data?.publisher?.name}
            </Typography>
          </Link>
        </Box>
      </Box>
      <StyledRating
        icon={<StarRounded fontSize="inherit" />}
        name="read-only"
        value={props.data?.stars}
        readOnly
        style={{ marginTop: 16 }}
      />
    </Box>
  );
}
