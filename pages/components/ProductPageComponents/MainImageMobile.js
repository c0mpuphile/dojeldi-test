import {
  Box,
  Divider,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import Link from "next/link";
import { BASE_URL } from "../../../services/axios";
import MainImageMobileHeader from "./MainImageMobileHeader";

export default function MainImageMobile(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexWrap: "nowrap",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <MainImageMobileHeader data={props.data} />
      <Paper
        style={{
          minHeight: 100,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          boxShadow: "0px 30px 50px rgba(0,0,0,0.05)",
          flexDirection: "column",
          marginTop: 88,
          borderRadius: 16,
        }}
      >
        <img
          src={BASE_URL + "/image/" + props.data?.image}
          onError={(event) => {
            event.target.src = "/missingbook.png";
          }}
          style={{
            maxHeight: 210,
            boxShadow: "20px 20px 40px rgba(0,0,0,0.15)",
            borderRadius: 20,
            marginTop: -64,
          }}
        />
        <Box style={{ display: "flex", flexDirection: "row", marginTop: 16 }}>
          <Box>
            <img src="/Thumb1.png" style={{ width: "100%" }} />
          </Box>
          <Box>
            <img src="/Thumb2.png" style={{ width: "100%" }} />
          </Box>
          <Box>
            <img src="/Thumb3.png" style={{ width: "100%" }} />
          </Box>
          <Box>
            <img src="/Thumb4.png" style={{ width: "100%" }} />
          </Box>
        </Box>
        <Divider style={{ width: "100%" }} />
        <Box
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            padding: "8px 24px",
          }}
        >
          <Box
            style={{
              flex: "100%",
              display: "flex",
              justifyContent: "flex-start",
              margin: "8px 0px",
            }}
          >
            <Typography
              variant="h5"
              style={{ fontFamily: "ravi", color: "#878A9B" }}
            >
              نـویسنده:{" "}
            </Typography>
            {props.data?.authors?.map((item) => (
              <Link href={`/Category?authors=&authors=${item?.id}`}>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#1273EB",
                  }}
                >
                  {item.title}
                </Typography>
              </Link>
            ))}
          </Box>
          <Box
            style={{
              flex: "100%",
              display: "flex",
              justifyContent: "flex-start",
              margin: "8px 0px",
            }}
          >
            <Typography
              variant="h5"
              style={{ fontFamily: "ravi", color: "#878A9B" }}
            >
              مـترجم:{" "}
            </Typography>
            {props.data?.translators?.map((item) => (
              <Link href={`/Category?translators=&translators=${item?.id}`}>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#1273EB",
                  }}
                >
                  {item.title}
                </Typography>
              </Link>
            ))}
          </Box>
          <Box
            style={{
              flex: "100%",
              display: "flex",
              justifyContent: "flex-start",
              margin: "8px 0px",
            }}
          >
            <Typography
              variant="h5"
              style={{ fontFamily: "ravi", color: "#878A9B" }}
            >
              انتشارات:{" "}
            </Typography>
            <Link
              href={`/Category?publishers=&publishers=${props.data?.publisher?.id}`}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi",
                  color: "#1273EB",
                }}
              >
                {props.data?.publisher?.name}
              </Typography>
            </Link>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
}
