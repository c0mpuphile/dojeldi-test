import { Box, Divider, Link, Typography } from "@material-ui/core";

export default function MainContentBody(props) {
  return (
    <Box>
      <Typography
        variant="h6"
        style={{
          fontFamily: "ravi",
          color: "#878A9B",
          lineHeight: 2,
          marginTop: 16,
        }}
      >
        {props.data?.description}
        {props.data?.criticism ? (
          <Link
            onClick={() => {
              props.setC(true);
            }}
            variant="h6"
            style={{ fontFamily: "ravi", color: "#FF8523" }}
          >
            مشاهده بیشتر
          </Link>
        ) : null}
      </Typography>
    </Box>
  );
}
