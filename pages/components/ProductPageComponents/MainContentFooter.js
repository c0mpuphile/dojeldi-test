import { Box, Divider, Typography } from "@material-ui/core";

export default function MainContentFooter(props) {
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box
          style={{
            flex: "0 0 50%",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 32,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              نام فارسی
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                textAlign: "right",
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              نام انگلیسی
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.english_title}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              دسته‌بندی
            </Typography>

            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.categories.map((item) => item.title).join("/")}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              نویسنده
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.authors.map((item) => item.title).join(" و ")}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              مترجم
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.translators.map((item) => item.title).join(" و ")}
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            flex: " 0 0 45%",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 32,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              فرمت
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                textAlign: "right",
              }}
            >
              {props.data?.format}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              سال انتشار
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.publication_year}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              ISBN
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.isbn}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              نسخه
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.edition}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              انتشارات
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
              }}
            >
              {props.data?.publisher.name}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Divider style={{ width: "100%", marginTop: 32 }} />
    </Box>
  );
}
