import {
  Avatar,
  Box,
  Button,
  Card,
  CardMedia,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  IconButton,
  Link,
  List,
  ListItem,
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AddCommentOutlined,
  AddShoppingCart,
  Close,
  FavoriteBorderOutlined,
  Headset,
  StarRounded,
  ThumbDownOutlined,
  ThumbUpOutlined,
} from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import ProductListItem from "../PublicComponents/ProductListItem";
import styles from "../ProductList.module.css";
import CommentItem from "./CommentItem";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { rateBook } from "../../../redux/book/rateBook/actions";
import { useRouter } from "next/router";
import { commentToRate } from "../../../redux/book/commentToRate/actions";
import { getBook } from "../../../redux/book/getBook/actions";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function Comment(props) {
  const theme = useTheme();
  const [rate, setRate] = useState({
    title: "",
    description: "",
    rate: 0,
  });
  const [comment, setComment] = useState();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const dispatch = useDispatch();
  const router = useRouter();
  const { ok: rateOk } = useSelector((state) => state.book.rateBook);
  const { ok: rateCommentOk } = useSelector(
    (state) => state.book.commentToRate
  );

  useEffect(() => {
    if (rateOk) dispatch(getBook({ id: router.query.id }));
  }, [rateOk]);
  useEffect(() => {
    if (rateCommentOk) dispatch(getBook({ id: router.query.id }));
  }, [rateCommentOk]);
  return (
    <Box style={{ width: "100%" }}>
      <Box style={{ marginTop: 32 }}>
        <Card
          style={{
            display: "flex",
            flexDirection: "row",
            boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
          }}
        >
          <Grid
            container
            style={{
              display: "flex",
              flexDirection: matches ? "row" : "column",
              justifyContent: "center",
              padding: "4vw",
              flexWrap: "nowrap",
            }}
          >
            <Grid
              item
              lg={4}
              sm={"auto"}
              style={{ marginLeft: matches ? "2vw" : "0px" }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Typography
                  variant="h3"
                  style={{
                    fontFamily: "ravi black",
                    color: "#202439",
                    textAlign: "center",
                  }}
                >
                  دیدگاه خود را ثبت کنید
                </Typography>
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    marginTop: 8,
                    color: "#878A9B",
                    textAlign: "center",
                  }}
                >
                  با ثبت دیدگاه خود درباره این اثر، ما را در مسیر ارائه بهتر
                  خدمات همراهمی کنید. دیدگاه شما پس از تایید توسط مدیریت، در این
                  صفحه نمایش داده خواهد شد
                </Typography>
                <Link
                  variant="h6"
                  style={{
                    color: "#1273EB",
                    fontFamily: "ravi",
                    textAlign: "center",
                    marginTop: 8,
                    marginBottom: 40,
                  }}
                >
                  مشاهده قوانین ثبت دیدگاه
                </Link>
                {comment ? (
                  <Box
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      marginBottom: 16,
                    }}
                  >
                    <Typography
                      variant="h4"
                      style={{ fontFamily: "ravi medium" }}
                    >
                      {"برای " + comment.title + " : "}
                    </Typography>
                    <IconButton
                      onClick={() => {
                        setComment(null);
                      }}
                    >
                      <Close />
                    </IconButton>
                  </Box>
                ) : null}
                <Box
                  style={{
                    display: "flex",
                    border: "1px #E0E1E5 solid",
                    borderRadius: 10,
                    padding: 18,
                    width: "100%",
                  }}
                >
                  <input
                    value={rate.title}
                    onChange={(event) => {
                      setRate({ ...rate, title: event.target.value });
                    }}
                    placeholder="عنوان دیدگاه"
                    style={{
                      border: "none",
                      fontFamily: "ravi",
                      fontSize: 16,
                      color: "#878A9B",
                      width: "100%",
                    }}
                  />
                </Box>
                <Box
                  style={{
                    display: "flex",
                    marginTop: 20,
                    border: "1px #E0E1E5 solid",
                    borderRadius: 10,
                    padding: 18,
                  }}
                >
                  <textarea
                    value={rate.description}
                    onChange={(event) => {
                      setRate({
                        ...rate,
                        description: event.target.value,
                      });
                    }}
                    rows={10}
                    placeholder="متن دیدگاه"
                    style={{
                      border: "none",
                      flex: "100%",
                      fontFamily: "ravi",
                      fontSize: 16,
                      color: "#878A9B",
                    }}
                  />
                </Box>
                {!comment ? (
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      marginTop: 24,
                    }}
                  >
                    <Typography
                      variant="h3"
                      style={{
                        fontFamily: "ravi",
                        color: "#878A9B",
                      }}
                    >
                      به ایـن کتاب امـتیاز دهـید
                    </Typography>
                    <StyledRating
                      name="pristine"
                      value={rate.rate}
                      onChange={(event) => {
                        setRate({
                          ...rate,
                          rate: event.target.value,
                        });
                      }}
                    />
                  </Box>
                ) : null}
                <Box style={{ marginTop: 40 }}>
                  <Button
                    onClick={() => {
                      if (comment) {
                        dispatch(
                          commentToRate({
                            id: router.query.id,
                            rateId: comment.rateId,
                            title: rate.title,
                            description: rate.description,
                          })
                        );
                        setComment(null);
                        setRate({ title: "", description: "", rate: 0 });
                      } else {
                        dispatch(rateBook({ ...rate, id: router.query.id }));
                        setRate({ title: "", description: "", rate: 0 });
                      }
                    }}
                    style={{
                      border: "none",
                      backgroundColor: "#FF9126",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      width: "100%",
                      padding: 16,
                      borderRadius: 10,
                    }}
                  >
                    <Typography
                      variant="h3"
                      style={{
                        fontFamily: "ravi medium",
                        color: "white",
                      }}
                    >
                      ثبت دیدگاه
                    </Typography>
                  </Button>
                </Box>
              </Box>
            </Grid>
            <Grid item lg={8} sm={"auto"}>
              <GridList cols={1} style={{ padding: "0px 16px 32px 16px" }}>
                {props.data?.map((item) => (
                  <GridListTile style={{ marginTop: 40, height: "100%" }}>
                    <CommentItem
                      setComment={(value) => setComment(value)}
                      data={item}
                    />
                  </GridListTile>
                ))}
              </GridList>
            </Grid>
          </Grid>
        </Card>
      </Box>
    </Box>
  );
}
