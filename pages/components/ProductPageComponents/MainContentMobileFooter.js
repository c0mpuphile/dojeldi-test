import {
  Box,
  Card,
  Collapse,
  Divider,
  Link,
  Typography,
} from "@material-ui/core";
import { useState } from "react";

export default function MainContentMobileFooter(props) {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Card
        style={{
          borderRadius: 16,
          boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
          marginTop: 32,
        }}
      >
        <Box style={{ padding: 16 }}>
          <Typography
            variant="h4"
            style={{
              fontFamily: "ravi black",
              color: "#202439",
              textAlign: "center",
            }}
          >
            مشخصات کتاب
          </Typography>
        </Box>
        <Divider style={{ width: "100%" }} />
        <Box
          style={{
            flex: "1",
            display: "flex",
            flexDirection: "column",
            padding: 20,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              flex: 1,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
                flex: "0 0 30%",
              }}
            >
              نام فارسی
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                textAlign: "right",
                flex: "0 0 70%",
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
                flex: "0 0 30%",
              }}
            >
              نام انگلیسی
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                flex: "0 0 70%",
              }}
            >
              {props.data?.english_title}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
                flex: "0 0 30%",
              }}
            >
              دسته‌بندی
            </Typography>

            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                flex: "0 0 70%",
              }}
            >
              {props.data?.categories.map((item) => item.title).join("/")}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
                flex: "0 0 30%",
              }}
            >
              نویسنده
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                flex: "0 0 70%",
              }}
            >
              {props.data?.authors.map((item) => item.title).join(" و ")}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: 16,
            }}
          >
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
                flex: "0 0 30%",
              }}
            >
              مترجم
            </Typography>
            <Typography
              variant="h6"
              style={{
                color: "#878A9B",
                fontFamily: "ravi",
                flex: "0 0 70%",
              }}
            >
              {props.data?.translators.map((item) => item.title).join(" و ")}
            </Typography>
          </Box>
          <Collapse in={open}>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 16,
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  flex: "0 0 30%",
                }}
              >
                فرمت
              </Typography>
              <Typography
                variant="h6"
                style={{
                  color: "#878A9B",
                  fontFamily: "ravi",
                  flex: "0 0 70%",
                }}
              >
                {props.data?.format}
              </Typography>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 16,
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  flex: "0 0 30%",
                }}
              >
                سال انتشار
              </Typography>
              <Typography
                variant="h6"
                style={{
                  color: "#878A9B",
                  fontFamily: "ravi",
                  flex: "0 0 70%",
                }}
              >
                {props.data?.publication_year}
              </Typography>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 16,
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  flex: "0 0 30%",
                }}
              >
                ISBN
              </Typography>
              <Typography
                variant="h6"
                style={{
                  color: "#878A9B",
                  fontFamily: "ravi",
                  flex: "0 0 70%",
                }}
              >
                {props.data?.isbn}
              </Typography>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 16,
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  flex: "0 0 30%",
                }}
              >
                نسخه
              </Typography>
              <Typography
                variant="h6"
                style={{
                  color: "#878A9B",
                  fontFamily: "ravi",
                  flex: "0 0 70%",
                }}
              >
                {props.data?.edition}
              </Typography>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: 16,
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  flex: "0 0 30%",
                }}
              >
                انتشارات
              </Typography>
              <Typography
                variant="h6"
                style={{
                  color: "#878A9B",
                  fontFamily: "ravi",
                  flex: "0 0 70%",
                }}
              >
                {props.data?.publisher.name}
              </Typography>
            </Box>
          </Collapse>
        </Box>
        <Divider style={{ width: "100%" }} />
        <Box
          style={{
            display: "flex",
            padding: 16,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Link
            onClick={() => setOpen((prev) => !prev)}
            variant="h6"
            style={{
              fontFamily: "ravi",
              color: "#FF8523",
              textAlign: "center",
            }}
          >
            مشاهده بیشتر
          </Link>
        </Box>
      </Card>
    </Box>
  );
}
