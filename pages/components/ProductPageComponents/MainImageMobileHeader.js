import {
  Box,
  Breadcrumbs,
  Divider,
  Link,
  Typography,
  withStyles,
} from "@material-ui/core";
import { StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function MainImageMobileHeader(props) {
  return (
    <Box
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Breadcrumbs>
        <Link
          href="/"
          variant="h6"
          style={{ fontFamily: "ravi", color: "#878A9B" }}
        >
          دوجلدی
        </Link>
        <Link
          href="/Category"
          variant="h6"
          style={{ fontFamily: "ravi", color: "#878A9B" }}
        >
          دسته‌بندی
        </Link>
        {props.data?.categories?.map((item) => (
          <Link
            href={`/Category?categories=${item?.id}&categories=`}
            variant="h6"
            style={{ fontFamily: "ravi", color: "#878A9B" }}
          >
            {item.title}
          </Link>
        ))}
      </Breadcrumbs>
      <Typography
        variant="h1"
        style={{
          fontFamily: "ravi black",
          color: "#202439",
          marginTop: 8,
          textAlign: "center",
        }}
      >
        {props.data?.persian_title}
      </Typography>

      <StyledRating
        icon={<StarRounded fontSize="inherit" />}
        name="read-only"
        value={props.data?.stars}
        readOnly
        style={{ marginTop: 4 }}
      />
    </Box>
  );
}
