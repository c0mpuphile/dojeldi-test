import {
  Avatar,
  Box,
  Grid,
  GridList,
  GridListTile,
  IconButton,
  Typography,
  withStyles,
} from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import { useDispatch } from "react-redux";
import { likeRate } from "../../../redux/book/likeRate/actions";
import Comment from "../Icons/Comment";
import Disslike from "../Icons/Disslike";
import Like from "../Icons/Like";
import moment from "moment-jalaali";

const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);

export default function CommentItem(props) {
  const dispatch = useDispatch();
  return (
    <>
      <Box style={{ display: "flex", flexDirection: "row" }}>
        <Box>
          <Avatar src="profile.png" />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            marginRight: 16,
            flex: "1 0 0%",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              marginLeft: 16,
            }}
          >
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi bold",
                color: "#202439",
              }}
            >
              {props.data?.user?.full_name}
            </Typography>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi medium",
                color: "#878A9B",
              }}
            >
              {moment(props.data?.created_at).format("jYYYY/jMM/jDD")}
            </Typography>
          </Box>
          <Box style={{ marginTop: 24 }}>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi",
                color: "#202439",
              }}
            >
              {props.data?.description}
            </Typography>
          </Box>
          {!props.second ? (
            <Box
              style={{
                display: "flex",
                marginTop: 20,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <StyledRating name="pristine" value={props.data?.rate} />

              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <IconButton
                    onClick={() => {
                      props.setComment({
                        rateId: props.data?.id,
                        title: props.data?.title,
                      });
                    }}
                  >
                    <Comment />
                  </IconButton>
                </Box>

                <Box
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Typography
                    variant="h5"
                    style={{
                      color: "#B7B9C3",
                      fontFamily: "ravi medium",
                    }}
                  >
                    {props.data?.likes.dislikes}
                  </Typography>
                  <IconButton
                    onClick={() => {
                      dispatch(
                        likeRate({ id: props.data?.id, type: "DISLIKE" })
                      );
                    }}
                  >
                    <Disslike />
                  </IconButton>
                </Box>
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Typography
                    variant="h5"
                    style={{
                      color: "#B7B9C3",
                      fontFamily: "ravi medium",
                    }}
                  >
                    {props.data?.likes.likes}
                  </Typography>
                  <IconButton
                    onClick={() => {
                      dispatch(likeRate({ id: props.data?.id, type: "LIKE" }));
                    }}
                  >
                    <Like />
                  </IconButton>
                </Box>
              </Box>
            </Box>
          ) : null}
        </Box>
      </Box>
      {!props.second && props.data?.comment ? (
        <Grid container>
          <Grid item lg={2} sm={"auto"}></Grid>
          <Grid item lg={10} sm={"auto"}>
            <GridList cols={1} style={{ padding: "0px 16px 32px 16px" }}>
              <GridListTile style={{ marginTop: 40, height: "100%" }}>
                <CommentItem second={true} data={props.data?.comment} />
              </GridListTile>
            </GridList>
          </Grid>
        </Grid>
      ) : null}
    </>
  );
}
