import {
  Box,
  Card,
  Divider,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import {
  Favorite,
  FavoriteBorder,
  GetApp,
  Instagram,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { likeBook } from "../../../redux/book/likeBook/actions";
import Book from "../Icons/Book";
import BookAudio from "../Icons/BookAudio";
import BookElec from "../Icons/BookElec";
import Download from "../Icons/Download";
import ProductCartType from "../PublicComponents/ProductCartType";

export default function MainPrices(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const dispatch = useDispatch();
  const { ok: okFav } = useSelector((state) => state.book.likeBook);
  const router = useRouter();

  return (
    <Box>
      <Box
        elevation={0}
        style={{
          borderRadius: 20,
          backgroundColor: "#EFF0F5",
          borderColor: "#E0E1E5",
          borderWidth: 1,
        }}
      >
        <Card
          style={{
            borderRadius: 20,
            boxShadow: "0px 0px 30px rgba(0,0,0,0.1)",
          }}
        >
          <Box style={{}}>
            <ProductCartType
              icon={
                <Box style={{ width: 48 }}>
                  <Book
                    width="100%"
                    height="100%"
                    color={
                      props.data?.has_print_version ? "#1273EB" : "#B7B9C3"
                    }
                  />
                </Box>
              }
              disable={!props.data?.has_print_version}
              typeFa="نسخه کاغذی"
              type="PRINT"
              price={props.data?.print_version_price}
              salePrice={props.data?.print_version_sale_price}
              discount={props.data?.print_version_discount}
            />
          </Box>
          <Divider style={{ width: "100%" }} />
          <Box style={{}}>
            <ProductCartType
              icon={
                <Box style={{ width: 48 }}>
                  <BookElec
                    width="100%"
                    height="100%"
                    color={
                      props.data?.has_electronic_version ? "#FF5252" : "#B7B9C3"
                    }
                  />
                </Box>
              }
              disable={!props.data?.has_electronic_version}
              typeFa="نسخه الکترونیک"
              type="ELECTRONIC"
              price={props.data?.electronic_version_price}
              salePrice={props.data?.electronic_version_sale_price}
              discount={props.data?.electronic_version_discount}
            />
          </Box>
          <Divider style={{ width: "100%" }} />
          <Box style={{}}>
            <ProductCartType
              icon={
                <Box style={{ width: 48 }}>
                  <BookAudio
                    width="100%"
                    height="100%"
                    color={
                      props.data?.has_audio_version ? "#FF9A27" : "#B7B9C3"
                    }
                  />
                </Box>
              }
              disable={!props.data?.has_audio_version}
              typeFa="نسخه صوتی"
              type="AUDIO"
              price={props.data?.audio_version_price}
              salePrice={props.data?.audio_version_sale_price}
              discount={props.data?.audio_version_discount}
            />
          </Box>
          <Divider style={{ width: "100%" }} />
          <Box
            onClick={() => dispatch(likeBook({ id: router.query.id }))}
            style={{
              margin: 24,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {!okFav ? (
              <FavoriteBorder style={{ marginLeft: 4 }} />
            ) : (
              <Favorite style={{ marginLeft: 4 }} />
            )}
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi medium",
                color: "#878A9B",
                marginRight: 4,
              }}
            >
              افـزودن به لـیست عـلاقـمندی‌هـا
            </Typography>
          </Box>
          <Divider style={{ width: "100%" }} />
          <Box
            style={{
              margin: 16,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi",
                color: "#878A9B",
              }}
            >
              با دوستان خود به اشتراک بگذارید
            </Typography>
            <Box>
              <Twitter
                style={{ color: "#878A9B", fontSize: 18, margin: "0px 8px" }}
              />
              <Instagram
                style={{ color: "#878A9B", fontSize: 18, margin: "0px 8px" }}
              />
              <WhatsApp
                style={{ color: "#878A9B", fontSize: 18, margin: "0px 8px" }}
              />
              <Telegram
                style={{ color: "#878A9B", fontSize: 18, margin: "0px 8px" }}
              />
            </Box>
          </Box>
        </Card>
        <Box
          style={{
            padding: "16px 32px",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Typography variant="h5" style={{ fontFamily: "ravi" }}>
            به مشاور نیاز دارید؟
          </Typography>
          <Typography
            variant="h4"
            style={{
              fontFamily: "ravi black",

              direction: "ltr",
            }}
          >
            ۰۲۱ - ۵۴۰۶۳
          </Typography>
        </Box>
      </Box>
      {/*
      <Card
        style={{
          marginTop: 16,
          borderRadius: 20,
          boxShadow: "0px 20px 20px rgba(0,0,0,0.05)",
        }}
      >
        <Box
          style={{
            margin: 24,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h5"
            style={{
              fontFamily: "ravi medium",
              color: "#878A9B",
            }}
          >
            دانـلود پـیش نـمایش کـتاب الکترونیک
          </Typography>
          <Download />
        </Box>
        <Divider style={{ width: "100%" }} />
        <Box
          style={{
            margin: 24,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Typography
            variant="h5"
            style={{
              fontFamily: "ravi medium",
              color: "#878A9B",
            }}
          >
            دانـلود پـیش نـمایش کـتاب الکترونیک
          </Typography>
          <Download />
        </Box>
      </Card>
      */}
    </Box>
  );
}
