import { Box, Paper, useMediaQuery, useTheme } from "@material-ui/core";
import { BASE_URL } from "../../../services/axios";

export default function MainImage(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexWrap: "nowrap",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Paper
        style={{
          minHeight: 100,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: "50%",
          boxShadow: "0px 30px 50px rgba(0,0,0,0.05)",
        }}
      >
        <img
          src={BASE_URL + "/image/" + props.data?.image}
          onError={(event) => {
            event.target.src = "/missingbook.png";
          }}
          style={{
            width: "360px",
            boxShadow: "20px 20px 40px rgba(0,0,0,0.15)",
            borderRadius: 20,
          }}
        />
      </Paper>
      {/* <Box style={{ display: "flex", flexDirection: "row", marginTop: 16 }}>
        <Box>
          <img src="/Thumb1.png" style={{ width: "100%" }} />
        </Box>
        <Box>
          <img src="/Thumb2.png" style={{ width: "100%" }} />
        </Box>
        <Box>
          <img src="/Thumb3.png" style={{ width: "100%" }} />
        </Box>
        <Box>
          <img src="/Thumb4.png" style={{ width: "100%" }} />
        </Box>
      </Box> */}
    </Box>
  );
}
