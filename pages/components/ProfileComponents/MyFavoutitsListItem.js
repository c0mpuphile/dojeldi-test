import {
  Box,
  Button,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Delete, Headset, MoreVert, ShoppingCart } from "@material-ui/icons";

export default function MyFavouritsListItem(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        border: matches ? "2px rgba(157,160,177,0.3) solid" : "none",
        borderRadius: 5,
        padding: 0,
        height: "100%",
        boxShadow: !matches ? "0px 0px 20px rgba(0,0,0,0.05)" : null,
        backgroundColor: !matches ? "white" : "transparent",
        alignItems: "center",
      }}
    >
      <img
        src="/book1.jpg"
        style={{
          height: matches ? 200 : 110,
          borderRadius: matches ? "0px 5px 5px 0px" : 5,
          margin: !matches ? 8 : 0,
        }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          padding: 20,
          flex: "auto",
        }}
      >
        <Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Headset style={{ fontSize: 16, color: "#FF9A27" }} />
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi",
                color: "#878A9B",
                marginRight: 8,
              }}
            >
              نسخه صوتی
            </Typography>
          </Box>
          <Box style={{ marginTop: 8 }}>
            <Typography
              variant="h4"
              style={{
                fontFamily: "ravi black",
                color: "#202439",
              }}
            >
              کتاب جنایت و مکافات از فئودور داستایوفسکی
            </Typography>
          </Box>
          <Box style={{ marginTop: 4 }}>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi medium",
                color: "#1273EB",
              }}
            >
              فئودور داستایوفسکی
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Button style={{ backgroundColor: "#2DB67D", height: 32 }}>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Headset style={{ color: "white" }} />
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "white",
                    marginRight: 8,
                  }}
                >
                  گـوش کـردن بـه کـتاب
                </Typography>
              </Box>
            </Button>
            <Button
              style={{
                backgroundColor: "rgba(255,82,82,0.15)",
                marginRight: 4,
                height: 32,
              }}
            >
              <Delete style={{ color: "#FF5252" }} />
            </Button>
          </Box>
          <IconButton>
            <MoreVert style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
