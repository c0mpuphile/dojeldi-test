import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  Divider,
  GridList,
  GridListTile,
  Hidden,
  IconButton,
  makeStyles,
  Paper,
  Step,
  StepConnector,
  StepLabel,
  Stepper,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  ArrowBack,
  Check,
  Close,
  FileCopy,
  InfoOutlined,
  Loop,
} from "@material-ui/icons";
import { useEffect, useState } from "react";
import Filter from "../PublicComponents/FilterGenerator";
import MyOrdersListItem from "./MyOrdersListItem";
import clsx from "clsx";
import { useDispatch, useSelector } from "react-redux";
import { getOrders } from "../../../redux/profile/getOrders/actions";

export default function MyOrders(params) {
  const dispatch = useDispatch();
  const {
    ok: ordersOk,
    isLoading: ordersLoading,
    data: ordersData,
  } = useSelector((state) => state.profile.getOrders);
  const [status, setStatus] = useState("ALL");
  useEffect(() => {
    dispatch(getOrders({ status }));
  }, [status]);
  const statuses = [
    { status: "ALL", title: "همه" },
    {
      status: "PENDING",
      title: "در انتظار پرداخت",
      color: "#8E4DFF",
      icon: <Loop style={{ color: "#8E4DFF" }} />,
    },
    {
      status: "FINISHED",
      title: "تحویل داده شده",
      color: "#2DB67D",
      icon: <Check style={{ color: "#2DB67D" }} />,
    },
    {
      status: "CANCELLED",
      title: "لغو شده",
      color: "#FF5252",
      icon: <Close style={{ color: "#FF5252" }} />,
    },
    { status: "RETURNED", title: "مرجوعی" },
  ];
  const QontoConnector = withStyles({
    alternativeLabel: {
      top: 10,
      left: "calc(-50% + 16px)",
      right: "calc(50% + 16px)",
    },
    active: {
      "& $line": {
        borderColor: "#FF5252",
      },
    },
    completed: {
      "& $line": {
        borderColor: "#FF5252",
      },
    },
    line: {
      borderColor: "#eaeaf0",
      borderTopWidth: 3,
      borderRadius: 1,
    },
  })(StepConnector);

  const useQontoStepIconStyles = makeStyles({
    root: {
      color: "#eaeaf0",
      display: "flex",
      height: 22,
      alignItems: "center",
    },
    active: {
      color: "#FF5252",
    },
    circle: {
      width: 8,
      height: 8,
      borderRadius: "50%",
      backgroundColor: "currentColor",
    },
    completed: {
      width: 8,
      height: 8,
      borderRadius: "50%",
      backgroundColor: "#FF5252",
    },
  });

  function QontoStepIcon(props) {
    const classes = useQontoStepIconStyles();
    const { active, completed } = props;

    return (
      <div
        className={clsx(classes.root, {
          [classes.active]: active,
        })}
      >
        {completed ? (
          <div className={classes.completed} />
        ) : (
          <div className={classes.circle} />
        )}
      </div>
    );
  }

  const [activeStep, setActiveStep] = useState(1);
  const steps = [
    {
      id: 0,
      image: "/step1.png",
      label: "پرداخت و تکمیل خرید",
    },
    {
      id: 1,
      image: "/step2.png",
      label: "تایید و آماده سازی سفارش",
    },
    {
      id: 2,
      image: "/step3.png",
      label: "تحویل مرسوله به شرکت پست",
    },
    {
      id: 3,
      image: "/step4.png",
      label: "تحویل مرسوله به مشتری",
    },
  ];
  const [details, setDetails] = useState(false);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Filter
        typeValue="status"
        data={statuses}
        value={status}
        setValue={(value) => setStatus(value)}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {ordersLoading ? (
          <CircularProgress color="primary" />
        ) : (
          <GridList
            cellHeight={matches ? 311 : 371}
            cols={1}
            style={{ marginTop: 24 }}
            spacing={8}
          >
            {ordersData?.data?.map((order) => (
              <GridListTile>
                <MyOrdersListItem
                  statuses={statuses}
                  data={order}
                  details={setDetails}
                />
              </GridListTile>
            ))}
          </GridList>
        )}
      </Box>
      <Dialog
        onBackdropClick={() => {
          setDetails(false);
        }}
        open={details}
        fullWidth
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            padding: 32,
          }}
        >
          <Box style={{ display: "flex", flexDirection: "column" }}>
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              ۲۵۶۳۱۴۷۹۵۶۳۲۵۴۱۸۵۲۳۶۵۴۸۹
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
            >
              کد پیگیری مرسوله پستی
            </Typography>
          </Box>
          <IconButton>
            <FileCopy />
          </IconButton>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            padding: "0px 32px",
            alignItems: "center",
          }}
        >
          <InfoOutlined style={{ color: "#1273EB" }} />
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "#1273EB",
              marginRight: 4,
            }}
          >
            سامانه پیگیری مرلوله پستی شرکت پست ایران
          </Typography>
        </Box>
        <Divider style={{ width: "100%", marginTop: 16 }} />
        <Box>
          <Stepper
            style={{
              backgroundColor: "transparent",
              padding: 16,
            }}
            activeStep={1}
            alternativeLabel
            connector={<QontoConnector />}
          >
            {steps.map((item) => (
              <Step key={item.id}>
                <StepLabel step StepIconComponent={QontoStepIcon}>
                  <img src={item.image} />
                  <Typography
                    variant="h3"
                    style={{
                      fontFamily: "ravi black",
                      color: "#202439",
                      marginTop: 4,
                    }}
                  >
                    {item.label}
                  </Typography>
                </StepLabel>
              </Step>
            ))}
          </Stepper>
        </Box>
        <Box
          style={{
            backgroundColor: matches ? "rgba(32,36,57,0.03)" : "white",
            flexDirection: matches ? "row" : "column",
            display: "flex",
            padding: "16px 32px",
            alignItems: "center",
            justifyContent: "space-around",
            width: !matches ? "100%" : null,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: matches ? "column" : "row-reverse",
              alignItems: "center",
              width: !matches ? "100%" : null,
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 16 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              DJB-۴۸۸۳۲۶
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
            >
              شماره پیگیری سفارش
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: matches ? "column" : "row-reverse",
              alignItems: "center",
              width: !matches ? "100%" : null,
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 16 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              ۵۲,۰۰۰ تومان
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
            >
              مبلغ کل پرداخت شده
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: matches ? "column" : "row-reverse",
              alignItems: "center",
              width: !matches ? "100%" : null,
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 16 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              ۲۴ مرداد ۹۹
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
            >
              تـاریخ ثـبت سـفـارش
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: matches ? "column" : "row-reverse",
              alignItems: "center",
              width: !matches ? "100%" : null,
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 16 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              رایـگـان
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
            >
              هزینه ارسال محموله
            </Typography>
          </Box>
        </Box>
        <Box>
          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Box
              style={{
                maxWidth: 64,
                margin: matches ? 32 : 16,
              }}
            >
              <img
                src="/book1.jpg"
                style={{ borderRadius: 10, width: "100%" }}
              />
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                margin: matches ? 32 : 16,
                marginRight: 0,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                }}
              >
                <img
                  src="/ebook.svg"
                  style={{ width: 16, margin: "0px 4px", color: "#FF5252" }}
                />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه الکترونیک
                </Typography>
              </Box>
              <Box style={{ marginTop: 8 }}>
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 18 : 10,
                    color: "#202439",
                  }}
                >
                  کتاب پاکسازی ذهن برای موفق شدن در زندگی
                </Typography>
              </Box>
              <Hidden lgUp>
                <Typography
                  style={{
                    fontSize: 10,
                    fontFamily: "ravi medium",
                    color: "#1273EB",
                    marginTop: 8,
                  }}
                >
                  فلورانس اسکاول شین
                </Typography>
              </Hidden>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
                flex: "1 0 0%",
                margin: 32,
              }}
            >
              <Box style={{ display: "flex", flexDirection: "row" }}>
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi light",
                    textDecorationLine: "line-through",
                    color: "#878A9B",
                    margin: "0px 2px",
                  }}
                >
                  ۳۲,۰۰۰
                </Typography>
                <Typography
                  variant="h4"
                  style={{
                    backgroundColor: "#FF5252",
                    borderRadius: 5,
                    color: "white",
                    fontFamily: "ravi",
                    padding: "0px 4px",
                    margin: "0px 2px",
                  }}
                >
                  ۲۵٪
                </Typography>
              </Box>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 21 : 16,

                    margin: "0px 2px",
                  }}
                >
                  ۲۶,۰۰۰
                </Typography>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi medium",
                    margin: "0px 2px",
                    color: "#878A9B",
                  }}
                >
                  تومان
                </Typography>
              </Box>
            </Box>
          </Box>

          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Box
              style={{
                maxWidth: 64,
                margin: matches ? 32 : 16,
              }}
            >
              <img
                src="/book1.jpg"
                style={{ borderRadius: 10, width: "100%" }}
              />
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                margin: matches ? 32 : 16,
                marginRight: 0,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                }}
              >
                <img
                  src="/ebook.svg"
                  style={{ width: 16, margin: "0px 4px", color: "#FF5252" }}
                />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه الکترونیک
                </Typography>
              </Box>
              <Box style={{ marginTop: 8 }}>
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 18 : 10,
                    color: "#202439",
                  }}
                >
                  کتاب پاکسازی ذهن برای موفق شدن در زندگی
                </Typography>
              </Box>
              <Hidden lgUp>
                <Typography
                  style={{
                    fontSize: 10,
                    fontFamily: "ravi medium",
                    color: "#1273EB",
                    marginTop: 8,
                  }}
                >
                  فلورانس اسکاول شین
                </Typography>
              </Hidden>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
                flex: "1 0 0%",
                margin: 32,
              }}
            >
              <Box style={{ display: "flex", flexDirection: "row" }}>
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi light",
                    textDecorationLine: "line-through",
                    color: "#878A9B",
                    margin: "0px 2px",
                  }}
                >
                  ۳۲,۰۰۰
                </Typography>
                <Typography
                  variant="h4"
                  style={{
                    backgroundColor: "#FF5252",
                    borderRadius: 5,
                    color: "white",
                    fontFamily: "ravi",
                    padding: "0px 4px",
                    margin: "0px 2px",
                  }}
                >
                  ۲۵٪
                </Typography>
              </Box>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 21 : 16,

                    margin: "0px 2px",
                  }}
                >
                  ۲۶,۰۰۰
                </Typography>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi medium",
                    margin: "0px 2px",
                    color: "#878A9B",
                  }}
                >
                  تومان
                </Typography>
              </Box>
            </Box>
          </Box>

          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Box
              style={{
                maxWidth: 64,
                margin: matches ? 32 : 16,
              }}
            >
              <img
                src="/book1.jpg"
                style={{ borderRadius: 10, width: "100%" }}
              />
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                margin: matches ? 32 : 16,
                marginRight: 0,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignItems: "center",
                }}
              >
                <img
                  src="/ebook.svg"
                  style={{ width: 16, margin: "0px 4px", color: "#FF5252" }}
                />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه الکترونیک
                </Typography>
              </Box>
              <Box style={{ marginTop: 8 }}>
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 18 : 10,
                    color: "#202439",
                  }}
                >
                  کتاب پاکسازی ذهن برای موفق شدن در زندگی
                </Typography>
              </Box>
              <Hidden lgUp>
                <Typography
                  style={{
                    fontSize: 10,
                    fontFamily: "ravi medium",
                    color: "#1273EB",
                    marginTop: 8,
                  }}
                >
                  فلورانس اسکاول شین
                </Typography>
              </Hidden>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
                flex: "1 0 0%",
                margin: 32,
              }}
            >
              <Box style={{ display: "flex", flexDirection: "row" }}>
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi light",
                    textDecorationLine: "line-through",
                    color: "#878A9B",
                    margin: "0px 2px",
                  }}
                >
                  ۳۲,۰۰۰
                </Typography>
                <Typography
                  variant="h4"
                  style={{
                    backgroundColor: "#FF5252",
                    borderRadius: 5,
                    color: "white",
                    fontFamily: "ravi",
                    padding: "0px 4px",
                    margin: "0px 2px",
                  }}
                >
                  ۲۵٪
                </Typography>
              </Box>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: matches ? 21 : 16,

                    margin: "0px 2px",
                  }}
                >
                  ۲۶,۰۰۰
                </Typography>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi medium",
                    margin: "0px 2px",
                    color: "#878A9B",
                  }}
                >
                  تومان
                </Typography>
              </Box>
            </Box>
          </Box>
        </Box>
      </Dialog>
    </Box>
  );
}
