import MomentUtils from "@date-io/moment";
import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  makeStyles,
  MenuItem,
  Paper,
  Select,
  SvgIcon,
  Tab,
  Tabs,
  TextField,
  Typography,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutline,
  Close,
  Headset,
  InfoOutlined,
  PlayArrow,
  Settings,
} from "@material-ui/icons";
import { useRouter } from "next/router";

import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProfile } from "../../../redux/profile/getProfile/actions";
import { BASE_URL } from "../../../services/axios";
import ChargeProfile from "./ChargeProfile";
import EditProfile from "./EditProfile";
export default function ProfileDetails(params) {
  const [modal, setModal] = useState(false);
  const [profileEdit, setProfileEdit] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  useEffect(() => {
    dispatch(getProfile());
  }, []);
  const { data: profileData } = useSelector(
    (state) => state.profile.getProfile
  );

  return (
    <Paper
      style={{
        display: "flex",
        flexDirection: "column",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        borderRadius: 20,
        padding: "32px 0px",
        margin: "0px 20px",
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          padding: "0px 32px",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Avatar
            src={BASE_URL + "/image/" + profileData?.data?.profile_picture}
            style={{ width: 64, height: 64 }}
          >
            {profileData?.data?.profile_picture ? (
              <img
                src={BASE_URL + "/image/" + profileData?.data?.profile_picture}
              />
            ) : (
              "M"
            )}
          </Avatar>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              marginRight: 20,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              {profileData?.data?.name} {profileData?.data?.surname}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              {profileData?.data?.mobile}
            </Typography>
          </Box>
        </Box>
        <IconButton
          onClick={() => {
            setProfileEdit(true);
          }}
        >
          <Settings />
        </IconButton>
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: "0px 32px",
          marginTop: 32,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            {profileData?.data?.audio_books_count}
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            کـتاب صوتی
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            {profileData?.data?.electronic_books_count}
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            کـتاب الـکترونـیک
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            {profileData?.data?.interests}
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            عــلاقـمـنـدی
          </Typography>
        </Box>
      </Box>
      <Divider
        orientation="horizontal"
        style={{ width: "100%", marginTop: 20 }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          padding: "0px 32px",
          marginTop: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#FF5252",
              }}
            >
              {profileData?.data?.balance}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 18,
                color: "#FF5252",
                marginRight: 4,
              }}
            >
              تومان
            </Typography>
          </Box>
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            مـوجودی کـیف پـول شـما
          </Typography>
        </Box>
        <Button
          fullWidth
          onClick={() => {
            setModal(true);
          }}
          style={{
            backgroundColor: "#FF5252",
            borderRadius: 10,
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "white",
            marginTop: 16,
            alignItems: "center",
          }}
        >
          افزایش موجودی کیف پول
        </Button>
        <Button
          fullWidth
          style={{
            backgroundColor: "rgba(255,82,82,0.15)",
            borderRadius: 10,
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "#FF5252",
            marginTop: 16,
            alignItems: "center",
          }}
        >
          مـشاهده تـراکنش‌هـا
        </Button>
      </Box>
      <Divider
        orientation="horizontal"
        style={{ width: "100%", marginTop: 32 }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          marginTop: 20,
        }}
      >
        <Link
          onClick={() => {
            localStorage.removeItem("token");
            localStorage.removeItem("address");

            router.push("/Login");
          }}
          style={{
            fontFamily: "ravi medium",
            color: "#FF5252",
            fontSize: 16,
          }}
        >
          خروج از حساب کاربری
        </Link>
      </Box>
      <ChargeProfile setModal={setModal} modal={modal} />
      <EditProfile
        data={profileData?.data}
        setProfileEdit={setProfileEdit}
        profileEdit={profileEdit}
      />
    </Paper>
  );
}
