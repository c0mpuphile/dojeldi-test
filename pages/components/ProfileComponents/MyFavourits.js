import {
  Box,
  CircularProgress,
  GridList,
  GridListTile,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getIntrests } from "../../../redux/profile/getIntrests/actions";
import BascketWhite from "../Icons/BascketWhite";
import MyBooksListItem from "./MyBooksListItem";
import MyFavouritsListItem from "./MyFavoutitsListItem";

export default function MyFavourits(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const dispatch = useDispatch();
  const {
    ok: intrestOk,
    isLoading: intrestLoading,
    data: intrestData,
  } = useSelector((state) => state.profile.getIntrests);
  useEffect(() => {
    dispatch(getIntrests());
  }, []);
  return (
    <Box>
      {intrestLoading ? (
        <CircularProgress color="primary" />
      ) : (
        <GridList
          cellHeight={matches ? 200 : 140}
          cols={matches ? 2 : 1}
          spacing={32}
        >
          {intrestData?.map((item) => (
            <GridListTile>
              <MyBooksListItem
                data={item}
                title={"خرید این کتاب"}
                icon={<BascketWhite />}
              />
            </GridListTile>
          ))}
        </GridList>
      )}
    </Box>
  );
}
