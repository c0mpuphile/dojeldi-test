import {
  Box,
  Button,
  Hidden,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Delete, Headset, MoreVert } from "@material-ui/icons";
import { useRouter } from "next/router";
import { BASE_URL } from "../../../services/axios";
import Headphone from "../Icons/Headphone";
import Trash from "../Icons/Trash";

export default function MyBooksListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const router = useRouter();
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        border: matches ? "2px rgba(157,160,177,0.3) solid" : "none",
        borderRadius: 5,
        padding: 0,
        height: "100%",
        boxShadow: !matches ? "0px 0px 20px rgba(0,0,0,0.05)" : null,
        backgroundColor: !matches ? "white" : "transparent",
        alignItems: "center",
      }}
    >
      <img
        src={BASE_URL + "/image/" + props.data?.image}
        onError={(event) => {
          event.target.src = "/missingbook.png";
        }}
        style={{
          height: matches ? 200 : 110,
          borderRadius: matches ? "0px 5px 5px 0px" : 5,
          margin: !matches ? 8 : 0,
        }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          padding: 20,
          height: "100%",
        }}
      >
        <Box>
          <Box style={{ marginTop: 8 }}>
            <Typography
              variant="h4"
              style={{
                fontFamily: "ravi black",
                color: "#202439",
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Box style={{ marginTop: 4 }}>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi medium",
                color: "#1273EB",
              }}
            >
              فئودور داستایوفسکی
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Button
              onClick={() =>
                router.push({
                  pathname: "/Peoduct",
                  query: { id: props.data?.id },
                })
              }
              style={{
                backgroundColor: "#2DB67D",
                height: 32,
                borderRadius: 8,
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                {!props.noIcon ? props.icon || <Headphone /> : null}
                <Hidden mdDown>
                  <Typography
                    variant="h6"
                    style={{
                      fontFamily: "ravi",
                      color: "white",
                      marginRight: 8,
                    }}
                  >
                    {props.title ? props.title : "گـوش کـردن بـه کـتاب"}
                  </Typography>
                </Hidden>
              </Box>
            </Button>
            <Button
              style={{
                backgroundColor: "rgba(255,82,82,0.15)",
                marginRight: 4,
                height: 32,
                minWidth: 0,
                padding: 8,
                borderRadius: 8,
              }}
            >
              <Trash style={{ color: "#FF5252" }} />
            </Button>
          </Box>
          <IconButton>
            <MoreVert style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
