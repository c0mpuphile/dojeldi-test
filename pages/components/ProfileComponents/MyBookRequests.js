import {
  Box,
  Button,
  Divider,
  GridList,
  GridListTile,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";
import { useEffect, useState } from "react";
import instance from "../../../services/axios";
import BookRequest from "../PublicComponents/BookRequest";
import MyBookRequestsListItem from "./MyBookRequestsListItem";
import MyOrdersListItem from "./MyOrdersListItem";
import MyTicketsListItem from "./MyTicketsListItem";

export default function MyTickets(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [bookRequest,setBookRequest] = useState(false)
  const [requests , setRequests] = useState([])
  useEffect(() => {
    instance.get("/user/book-requests").then(response => setRequests(response.data?.data))
  },[])
  useEffect(() => {
    if(!bookRequest)
    instance.get("/user/book-requests").then(response => setRequests(response.data?.data))
  },[bookRequest])
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          justifyContent: "space-between",
        }}
      >
        <Box
          style={{ display: "flex", flexDirection: "row", overflow: "auto" }}
        >
          <button
            style={{
              backgroundColor: true ? "white" : "transparent",
              borderColor: true ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: true ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              همه درخواست‌ها
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              موجود در فروشگاه
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              در انـتظار بررسی
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              رد شـده
            </Typography>
          </button>
        </Box>
        <Button
          style={{
            backgroundColor: "#1273EB",
            marginTop: !matches ? 8 : 0,
            padding: 8,
            borderRadius: 10,
          }}
          onClick={() => setBookRequest(true)}
        >
          <Typography
            style={{ fontFamily: "ravi medium", fontSize: 14, color: "white" }}
          >
            ثبت درخواست کتاب
          </Typography>
        </Button>
      </Box>
      <GridList cellHeight={"auto"} cols={1}>
        {requests.map(request =>   <GridListTile
          style={{
            padding: "16px 0px",
          }}
        >
          <MyBookRequestsListItem data={request}/>
        </GridListTile>)}
      </GridList>
      <BookRequest bookRequest={bookRequest} setBookRequest={(value) => setBookRequest(value)}/>
    </Box>
  );
}
