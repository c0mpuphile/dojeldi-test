import {
  Box,
  Button,
  Divider,
  GridList,
  GridListTile,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";
import MyBookRequestsListItem from "./MyBookRequestsListItem";
import MyMessagesListItem from "./MyMessagesListItem";
import MyOrdersListItem from "./MyOrdersListItem";
import MyTicketsListItem from "./MyTicketsListItem";

export default function MyMessages(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          justifyContent: "space-between",
        }}
      >
        <Box
          style={{ display: "flex", flexDirection: "row", overflow: "auto" }}
        >
          <button
            style={{
              backgroundColor: true ? "white" : "transparent",
              borderColor: true ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: true ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              هـمه پـیام‌هـا
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              مـربوط به آگهی‌های مـن
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              مربوط به آگهی دیگران
            </Typography>
          </button>
        </Box>
      </Box>
      <GridList cols={1} style={{ marginTop: 24 }}>
        <GridListTile>
          <MyMessagesListItem />
        </GridListTile>
        <GridListTile>
          <MyMessagesListItem />
        </GridListTile>
      </GridList>
    </Box>
  );
}
