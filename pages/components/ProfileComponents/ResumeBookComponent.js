import { Box, IconButton, Typography } from "@material-ui/core";
import { Headset, PlayArrow } from "@material-ui/icons";

export default function ResumeBook(props) {
  return (
    <Box
      style={{
        margin: "0px 20px",
        padding: 20,
        display: "flex",
        flexDirection: "row",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        borderRadius: 20,
        background: `linear-gradient(180deg, rgba(84,85,126,1) 0%, rgba(143,156,187,1) 100%)`,
      }}
    >
      <Box>
        <img
          src="/book1.jpg"
          height={160}
          style={{
            borderRadius: 16,
            boxShadow: "10px 10px 20px rgba(0,0,0,0.5)",
          }}
        />
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          marginRight: 20,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Headset style={{ fontSize: 16, color: "white" }} />
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "white",
              marginRight: 8,
            }}
          >
            نسخه صوتی
          </Typography>
        </Box>
        <Box style={{ marginTop: 8 }}>
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 16,
              color: "white",
            }}
          >
            کتاب پاکسازی ذهن برای موفق شدن در زندگی
          </Typography>
        </Box>
        <Box style={{ marginTop: 4 }}>
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "white",
            }}
          >
            فئودور داستایوفسکی
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 16,
          }}
        >
          <Box
            style={{
              backgroundColor: "#475070",
              padding: "8px 16px",
              borderRadius: 10,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 14,
                color: "white",
              }}
            >
              ۰۲:۲۸ / ۱۲:۴۵
            </Typography>
          </Box>
          <IconButton style={{ backgroundColor: "white", marginRight: 8 }}>
            <PlayArrow style={{ fontSize: 16, color: "#2DB67D" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
