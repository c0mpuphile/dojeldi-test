import {
  Box,
  Button,
  GridList,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { AddCircleOutline, AddCircleOutlineOutlined } from "@material-ui/icons";
import { useEffect, useState } from "react";
import MyTransactionsListItem from "./MyTransactionsListItem";
import ChargeProfile from "./ChargeProfile";
import { useDispatch, useSelector } from "react-redux";
import { getUserTransactions } from "../../../redux/transactions/getUserTransactions/actions";

export default function MyTransactions() {
  const [modal, setModal] = useState(false);
  const theme = useTheme();
  const { data: profileData } = useSelector(
    (state) => state.profile.getProfile
  );
  const { data: transactions } = useSelector(
    (state) => state.transactions.getUserTransactions
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUserTransactions());
  }, []);
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Paper
        style={{
          borderRadius: 20,
          marginTop: 20,
          height: "auto",
          width: "100%",
          boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
          padding: 0,
          backgroundColor: matches ? "white" : "transparent",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "column",
            alignItems: "center",
            justifyContent: "space-between",
            padding: 24,
            backgroundColor: matches ? "#F8F8F9" : "transparent",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: matches ? "flex-start" : "center",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi black",
                  fontSize: 18,
                  color: "#202439",
                }}
              >
                {profileData?.data?.balance}
              </Typography>
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 14,
                  color: "#202439",
                  marginRight: 4,
                }}
              >
                تومان
              </Typography>
            </Box>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              موجودی فعلی کیف پول شما
            </Typography>
          </Box>
          <Button
            onClick={() => {
              setModal(true);
            }}
            style={{
              width: !matches ? "100%" : null,
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "white",
              backgroundColor: "#FF5252",
              borderRadius: 10,
              marginTop: !matches ? 8 : 0,
              padding: 8,
            }}
          >
            <AddCircleOutline style={{ color: "white", marginLeft: 4 }} />
            افزایش موجودی کیف پول
          </Button>
        </Box>
        <GridList cols={1} style={{ display: "flex", flexDirection: "column" }}>
          {transactions?.data?.map((item) => (
            <MyTransactionsListItem data={item} />
          ))}
        </GridList>
      </Paper>
      <ChargeProfile setModal={setModal} modal={modal} />
    </Box>
  );
}
