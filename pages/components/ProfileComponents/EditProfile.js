import {
  Avatar,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  Link,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { AddCircleOutline, Close } from "@material-ui/icons";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { editProfile } from "../../../redux/profile/editProfile/actions";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { uploadProfilePic } from "../../../redux/profile/uploadProfilePic/actions";
import { useEffect, useRef, useState } from "react";
import { BASE_URL } from "../../../services/axios";
import { getProfile } from "../../../redux/profile/getProfile/actions";
import getBase64 from "../PublicComponents/base64";

export default function EditProfile(props) {
  const dispatch = useDispatch();
  const upload = useRef();
  const { isLoading: uploadLoading, ok: uploadOk } = useSelector(
    (state) => state.profile.uploadProfilePic
  );
  useEffect(() => {
    dispatch(getProfile());
  }, [uploadOk]);
  const editProfileForm = useFormik({
    initialValues: {
      first_name: props.data?.first_name,
      last_name: props.data?.last_name,
      mobile: props.data?.mobile,
      password: props.data?.password,
      password_check: props.data?.password_check,
      email: props.data?.email,
      birth_date: props.data?.birth_date,
      gender: props.data?.gender,
    },
    onSubmit: (values) => {
      dispatch(editProfile(values));
    },
  });
  return (
    <Dialog
      onBackdropClick={() => {
        props.setProfileEdit(false);
      }}
      open={props.profileEdit}
      fullWidth
    >
      <DialogTitle>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box style={{ visibility: "hidden" }}>asd</Box>
          <Box>
            <Typography
              variant="h2"
              style={{
                fontFamily: "ravi black",

                color: "#202439",
                textAlign: "center",
              }}
            >
              ویرایش مشخصات فردی
            </Typography>
          </Box>
          <Box>
            <IconButton
              onClick={() => {
                props.setProfileEdit(false);
              }}
            >
              <Close style={{ color: "#878A9B" }} />
            </IconButton>
          </Box>
        </Box>
      </DialogTitle>
      <DialogContent
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          padding: 0,
        }}
      >
        <Box style={{ overflow: "auto", padding: "8px 24px", width: "100%" }}>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar
              src={BASE_URL + "/image/" + props.data?.profile_picture}
              style={{ width: 96, height: 96 }}
            >
              {uploadLoading ? <CircularProgress /> : null}
            </Avatar>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                marginTop: 16,
              }}
            >
              <input
                style={{ display: "none" }}
                ref={upload}
                type="file"
                name="file"
                onChange={(event) => {
                  getBase64(event.target.files[0]).then((result) => {
                    dispatch(uploadProfilePic({ image: result }));
                  });
                }}
              />
              <AddCircleOutline style={{ color: "#1273EB" }} />
              <Link
                onClick={() => upload.current.click()}
                style={{
                  color: "#1273EB",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                }}
              >
                بارگذاری عکس پروفایل جدید
              </Link>
            </Box>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 32,
              width: "100%",
            }}
          >
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="نام"
              name="first_name"
              value={editProfileForm.values.first_name}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="نام خـانـوادگی"
              name="last_name"
              value={editProfileForm.values.last_name}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 32,
              width: "100%",
            }}
          >
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="شـماره هـمراه"
              name="mobile"
              value={editProfileForm.values.mobile}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 32,
              width: "100%",
            }}
          >
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="نشانی پست الکترونیک"
              name="email"
              value={editProfileForm.values.email}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
          </Box>

          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 32,
              width: "100%",
            }}
          >
            <Select
              name="gender"
              value={editProfileForm.values.gender || ""}
              onChange={editProfileForm.handleChange}
              style={{ margin: "0px 8px", width: "100%" }}
            >
              <MenuItem value={""}>
                <em>
                  <Typography
                    style={{
                      fontFamily: "ravi bold",
                      fontSize: 16,
                      color: "#202439",
                    }}
                  >
                    جنسیت خود را انتخاب کنید
                  </Typography>
                </em>
              </MenuItem>
              <MenuItem value={"male"}>
                <Typography
                  style={{
                    fontFamily: "ravi bold",
                    fontSize: 16,
                    color: "#202439",
                  }}
                >
                  من آقا هستم
                </Typography>
              </MenuItem>
              <MenuItem value={"female"}>
                <Typography
                  style={{
                    fontFamily: "ravi bold",
                    fontSize: 16,
                    color: "#202439",
                  }}
                >
                  من خانوم هستم
                </Typography>
              </MenuItem>
              <MenuItem value={"other"}>
                <Typography
                  style={{
                    fontFamily: "ravi bold",
                    fontSize: 16,
                    color: "#202439",
                  }}
                >
                  من از جنسیت های دیگر هستم
                </Typography>
              </MenuItem>
            </Select>
            <MuiPickersUtilsProvider utils={MomentUtils} locale="en">
              <KeyboardDatePicker
                value={editProfileForm.values.birth_date}
                format="YYYY-MM-DD"
                onChange={(value) => {
                  editProfileForm.setFieldValue(
                    "birth_date",
                    value.format("YYYY-MM-DD")
                  );
                }}
                style={{ width: "100%", margin: "0px 8px" }}
                label="تاریخ تولد"
                name="birth_date"
                InputLabelProps={{
                  style: {
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#202439",
                  },
                }}
                InputProps={{
                  style: {
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#202439",
                  },
                }}
              />
            </MuiPickersUtilsProvider>
          </Box>
          <Divider style={{ width: "100%", marginTop: 32 }} />
          <Box style={{ width: "100%", marginTop: 32 }}>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#202439",
                textAlign: "center",
              }}
            >
              تـغییر گـذرواژه
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 32,
              width: "100%",
            }}
          >
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="رمز عبور"
              name="password"
              value={editProfileForm.values.password}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
            <TextField
              style={{ margin: "0px 8px", width: "100%" }}
              label="تکرار رمز عبور"
              name="password_check"
              value={editProfileForm.values.password_check}
              onChange={editProfileForm.handleChange}
              variant="outlined"
              InputProps={{
                style: { fontFamily: "ravi medium", borderRadius: 16 },
              }}
              InputLabelProps={{
                style: {
                  fontFamily: "ravi medium",
                  fontSize: 16,
                },
              }}
            />
          </Box>
        </Box>
      </DialogContent>
      <DialogActions>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            alignItems: "center",
            marginTop: 32,
          }}
        >
          <Button
            onClick={editProfileForm.handleSubmit}
            style={{
              width: "100%",
              backgroundColor: "#FF5252",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "white",
              }}
            >
              تایـید و ذخـیره تـغییرات
            </Typography>
          </Button>
          <Button
            onClick={() => {
              props.setProfileEdit(false);
            }}
            style={{
              width: "100%",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              انصراف و برگشت
            </Typography>
          </Button>
        </Box>
      </DialogActions>
    </Dialog>
  );
}
