import { Box, Button, Paper, Typography } from "@material-ui/core";

export default function MyAddressesListItem(props) {
  return (
    <Paper
      style={{
        borderRadius: 20,
        marginTop: 20,
        height: "auto",
        width: "100%",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        padding: 0,
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 18,
              color: "#202439",
            }}
          >
            کرج - استان البرز
          </Typography>
        </Box>
        <Button
          onClick={() => {
            props.changeAddress(true);
          }}
          style={{
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "#1273EB",
            border: "1px solid #1273EB",
            borderRadius: 10,
          }}
        >
          ویرایش آدرس
        </Button>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          معین سپهری
        </Typography>
      </Box>
    </Paper>
  );
}
