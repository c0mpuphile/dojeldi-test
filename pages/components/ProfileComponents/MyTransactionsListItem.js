import {
  Box,
  Button,
  Divider,
  Hidden,
  IconButton,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import {
  AddCircleOutline,
  ExpandLess,
  ExpandMore,
  FileCopy,
} from "@material-ui/icons";

export default function MyTransactionsListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        flex: "1",
        padding: "16px 16px",
        display: "flex",
        flexDirection: matches ? "row" : "column",
        backgroundColor: "transparent",
        alignItems: "center",
        backgroundColor: matches ? "transparent" : "white",
        margin: matches ? 0 : 8,
        borderRadius: !matches ? 16 : 0,
        boxShadow: !matches ? "0px 0px 20px rgba(0,0,0,0.05)" : "none",
      }}
    >
      <Box
        style={{
          display: "flex",
          flex: "0 0 60%",
          flexDirection: matches ? "row" : "column",
        }}
      >
        <Box
          style={{
            flex: "1 0 0%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginLeft: matches ? 16 : 0,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            {props.data?.type === "ADD" ? (
              <ExpandLess style={{ color: "#2DB67D" }} />
            ) : (
              <ExpandMore style={{ color: "#FF5252" }} />
            )}
            <Typography
              style={{
                fontFamily: "ravi Bold",
                fontSize: 16,
                color: props.data?.type === "ADD" ? "#2DB67D" : "#FF5252",
                marginRight: 16,
              }}
            >
              {props.data?.amount} تومان
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "row-reverse",
            flex: "1 0 0%",
            justifyContent: matches ? "space-around" : "center",
            width: !matches ? "100%" : null,
            marginTop: !matches ? 8 : 0,
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 16,
              color: "#878A9B",
              marginRight: matches ? 32 : 0,
            }}
          >
            {props.data?.paid_at || "بدون تاریخ"}
          </Typography>
          <Hidden lgUp>
            <Divider
              flexItem
              orientation="vertical"
              style={{ margin: "0px 8px" }}
            />
          </Hidden>

          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              {props.data?.source === "WALLET" ? "کیف پول" : "درگاه پرداخت"}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Hidden lgUp>
        <Divider
          orientation="horizontal"
          style={{ width: "100%", margin: "8px 0px" }}
        />
      </Hidden>
      <Box
        style={{
          flex: "0 0 40%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-end",
          marginRight: matches ? 16 : 0,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 16,
              color: "#878A9B",
              marginRight: 32,
            }}
          >
            {props.data?.transaction_id
              ? props.data?.transaction_id?.toString().length > 6
                ? props.data?.transaction_id?.toString().slice(0, 6) + "..."
                : props.data?.transaction_id?.toString()
              : "بدون شناسه پرداخت"}
          </Typography>
          <IconButton style={{ marginRight: 16 }}>
            <FileCopy style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
