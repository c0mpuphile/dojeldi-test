import {
  Box,
  Button,
  Divider,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";
import { loadGetInitialProps } from "next/dist/next-server/lib/utils";
import moment from "moment-jalaali";

export default function MyOrdersListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Paper style={{ display: "flex", flexDirection: "column" }}>
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "column",
            justifyContent: "space-around",
            width: !matches ? "100%" : null,
            marginTop: !matches ? 8 : 0,
            alignItems: matches ? "center" : null,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 8 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
                textAlign: "center",
              }}
            >
              {props.data?.issue_tracking_number}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
                textAlign: "center",
              }}
            >
              شماره پیگیری سفارش
            </Typography>
          </Box>
          <Divider
            style={{ margin: "0px 24px" }}
            flexItem
            orientation="vertical"
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 8 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
                textAlign: "center",
              }}
            >
              {props.data?.paid_price} تومان
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
                textAlign: "center",
              }}
            >
              مبلغ کل پرداخت شده
            </Typography>
          </Box>
          <Divider
            style={{ margin: "0px 24px" }}
            flexItem
            orientation="vertical"
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 8 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
                textAlign: "center",
              }}
            >
              {moment(props.data?.ordered_at).format("jYYYY/jMM/jDD")}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
                textAlign: "center",
              }}
            >
              تـاریخ ثـبت سـفـارش
            </Typography>
          </Box>
          <Divider
            style={{ margin: "0px 24px" }}
            flexItem
            orientation="vertical"
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
              marginTop: !matches ? 8 : 0,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
                textAlign: "center",
              }}
            >
              {props.data?.shipment_cost || "رایگان"}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
                textAlign: "center",
              }}
            >
              هزینه ارسال
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            width: !matches ? "100%" : null,
            justifyContent: !matches ? "space-between" : null,
            marginTop: !matches ? 8 : 0,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              backgroundColor:
                props.statuses?.find(
                  (item) => item.status === props.data?.status
                )?.color + "22" || "gray",
              padding: "8px 16px",
              borderRadius: 10,
            }}
          >
            {props.statuses?.find((item) => item.status === props.data?.status)
              ?.icon || null}
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color:
                  props.statuses?.find(
                    (item) => item.status === props.data?.status
                  )?.color || "grey",
              }}
            >
              {
                props.statuses?.find(
                  (item) => item.status === props.data?.status
                )?.title
              }
            </Typography>
          </Box>
          <Button
            onClick={() => {
              props.details(true);
            }}
            style={{
              backgroundColor: "#F3F3F5",
              borderRadius: 10,
              marginRight: 8,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#202439",
              }}
            >
              مشاهده جزئیات
            </Typography>
            <ArrowBack style={{ color: "#202439", marginRight: 4 }} />
          </Button>
        </Box>
      </Box>
      <Divider style={{ width: "100%" }} />
      <Box
        style={{
          padding: matches ? 32 : 16,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <img
          src="/book1.jpg"
          height={matches ? "160px" : "110px"}
          style={{ borderRadius: 10 }}
        />
        <img
          src="/book1.jpg"
          height={matches ? "160px" : "110px"}
          style={{ borderRadius: 10, marginRight: 20 }}
        />
        <img
          src="/book1.jpg"
          height={matches ? "160px" : "110px"}
          style={{ borderRadius: 10, marginRight: 20 }}
        />
      </Box>
    </Paper>
  );
}
