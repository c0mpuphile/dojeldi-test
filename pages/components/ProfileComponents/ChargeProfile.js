import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  TextField,
  Typography,
} from "@material-ui/core";
import { Close, InfoOutlined } from "@material-ui/icons";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { chargeWallet } from "../../../redux/invoice/chargeWallet/actions";

export default function ChargeProfile(props) {
  const dispatch = useDispatch();
  const chargeForm = useFormik({
    initialValues: {
      amount: null,
    },
    onSubmit: (values) => {
      dispatch(chargeWallet(values));
    },
  });

  return (
    <Dialog
      onBackdropClick={() => {
        props.setModal(false);
      }}
      open={props.modal}
      fullWidth
    >
      <DialogTitle>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box style={{ visibility: "hidden" }}>asd</Box>
          <Box>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#202439",
                textAlign: "center",
              }}
            >
              افـزایش مـوجـودی کـیف پـول
            </Typography>
          </Box>
          <Box>
            <IconButton
              onClick={() => {
                props.setModal(false);
              }}
            >
              <Close style={{ color: "#878A9B" }} />
            </IconButton>
          </Box>
        </Box>
      </DialogTitle>
      <DialogContent
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 20,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="مـبلغ شـارژ مـوردنـظر"
            name="amount"
            onChange={chargeForm.handleChange}
            value={chargeForm.values.amount}
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "flex-start",
            width: "100%",
            margin: "16px 8px",
          }}
        >
          <InfoOutlined style={{ color: "#FF5252" }} />
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "#FF5252",
              marginRight: 4,
            }}
          >
            حداقل مبلغ شارژ ۵,۰۰۰ تومان و حداکثر مبلغ شارژ ۱,۰۰۰,۰۰۰ تومان است
          </Typography>
        </Box>
        {/* <Grid container justify="center" spacing={1}>
          <Grid item>
            <Box
              style={{
                width: 128,
                height: 128,
                border: "solid #E0E1E5 1px",
                borderRadius: 20,
              }}
            ></Box>
          </Grid>
          <Grid item>
            <Box
              style={{
                width: 128,
                height: 128,
                border: "solid #E0E1E5 1px",
                borderRadius: 20,
              }}
            ></Box>
          </Grid>
          <Grid item>
            <Box
              style={{
                width: 128,
                height: 128,
                border: "solid #E0E1E5 1px",
                borderRadius: 20,
              }}
            ></Box>
          </Grid>
          <Grid item>
            <Box
              style={{
                width: 128,
                height: 128,
                border: "solid #E0E1E5 1px",
                borderRadius: 20,
              }}
            ></Box>
          </Grid>
        </Grid> */}
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            alignItems: "center",
            marginTop: 32,
          }}
        >
          <Button
            onClick={chargeForm.handleSubmit}
            style={{
              width: "100%",
              backgroundColor: "#FF5252",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "white",
              }}
            >
              پرداخت و شارژ کیف پول
            </Typography>
          </Button>
          <Button
            style={{
              width: "100%",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              انصراف و برگشت
            </Typography>
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
}
