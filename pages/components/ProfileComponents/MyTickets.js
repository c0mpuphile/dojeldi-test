import {
  Box,
  Button,
  Divider,
  GridList,
  GridListTile,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";
import MyOrdersListItem from "./MyOrdersListItem";
import MyTicketsListItem from "./MyTicketsListItem";

export default function MyTickets(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          justifyContent: "space-between",
        }}
      >
        <Box
          style={{ display: "flex", flexDirection: "row", overflow: "auto" }}
        >
          <button
            style={{
              backgroundColor: true ? "white" : "transparent",
              borderColor: true ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: true ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              همه سفارش‌ها
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              در انتظار پرداخت
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              در حال پردازش
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              تحویل داده شده
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              مرجوعی
            </Typography>
          </button>
          <button
            style={{
              backgroundColor: false ? "white" : "transparent",
              borderColor: false ? "#FF5252" : "#D7E1EA",
              borderStyle: "solid",
              borderWidth: 2,
              borderRadius: 10,
              margin: "0px 4px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 14,
                color: false ? "#202439" : "#878A9B",
                margin: "4px 4px",
              }}
            >
              لغو شده
            </Typography>
          </button>
        </Box>
        <Button
          style={{ backgroundColor: "#1273EB", marginTop: !matches ? 16 : 0 }}
        >
          <Typography
            style={{ fontFamily: "ravi medium", fontSize: 14, color: "white" }}
          >
            ارسال تیکت جدید
          </Typography>
        </Button>
      </Box>
      <GridList
        cellHeight={matches ? 90 : 195}
        cols={1}
        style={{ marginTop: 24 }}
      >
        <GridListTile>
          <MyTicketsListItem />
        </GridListTile>
        <GridListTile>
          <MyTicketsListItem />
        </GridListTile>
      </GridList>
    </Box>
  );
}
