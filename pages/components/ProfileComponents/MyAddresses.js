import {
  Box,
  Button,
  Divider,
  GridList,
  GridListTile,
  Hidden,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { AddCircleOutlineOutlined, ArrowBack, Check } from "@material-ui/icons";
import { loadGetInitialProps } from "next/dist/next-server/lib/utils";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAddress } from "../../../redux/profile/getAddress/actions";
import AddressItem from "../ConfirmCartComponent/AddressItem";
import EditAddress from "./EditAddress";
import MyAddressesListItem from "./MyAddressesListItem";
import MyOrdersListItem from "./MyOrdersListItem";
import MyTicketsListItem from "./MyTicketsListItem";

export default function MyAddresses(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [address, setAddress] = useState();

  const {
    error: errorAddress,
    ok: okAddress,
    data: dataAddress,
    isLoading: loadingAddress,
  } = useSelector((state) => state.profile.getAddress);
  const [openEditAddres, setOpenEditAddress] = useState(false);
  const [editAddressData, setEditAddressData] = useState(null);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAddress());
    setAddress(localStorage.getItem("address"));
  }, []);
  useEffect(() => {
    // console.log(editAddressData);
  }, [openEditAddres]);
  useEffect(() => {
    localStorage.setItem("address", address);
  }, [address]);
  return (
    <Box>
      {openEditAddres ? (
        <EditAddress
          data={editAddressData}
          open={openEditAddres}
          close={() => {
            dispatch(getAddress());

            setOpenEditAddress(false);
          }}
        />
      ) : null}
      <GridList style={{ width: "100%", padding: 20 }}>
        <Hidden lgUp>
          <Button
            onClick={() => {
              setEditAddressData(null);
              setOpenEditAddress(true);
            }}
            style={{
              width: "100%",
              backgroundColor: "#1273EB",
              borderRadius: 10,
              padding: 8,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 14,
                color: "white",
              }}
            >
              افـزودن آدرس جـدید
            </Typography>
          </Button>
        </Hidden>
        {dataAddress?.data?.map((addressItem) => (
          <AddressItem
            checked={address == addressItem.id}
            setAddress={(id) => setAddress(id)}
            editAddress={() => {
              setEditAddressData(addressItem);
              setOpenEditAddress(true);
            }}
            data={addressItem}
          />
        ))}
      </GridList>
      <Hidden mdDown>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            padding: 20,
          }}
        >
          <AddCircleOutlineOutlined
            style={{ color: "#1273EB", fontSize: 16 }}
          />
          <Link
            onClick={() => {
              setEditAddressData(null);
              setOpenEditAddress(true);
            }}
            style={{
              fontFamily: "ravi medium",
              color: "#1273EB",
              fontSize: 14,
              textAlign: "center",
              marginRight: 4,
            }}
          >
            افـزودن آدرس جـدید
          </Link>
        </Box>
      </Hidden>
    </Box>
  );
}
