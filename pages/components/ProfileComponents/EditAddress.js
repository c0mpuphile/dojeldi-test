import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getLocations } from "../../../redux/locations/getLocations/actions";
import { addAddress } from "../../../redux/profile/addAddress/actions";
import { editAddress } from "../../../redux/profile/editAddress/actions";
import { getAddress } from "../../../redux/profile/getAddress/actions";

export default function EditAddress(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getLocations());
  }, []);

  const [province, setProvince] = useState();
  const [loaded, setLoaded] = useState(false);
  const { data: locationsData } = useSelector(
    (state) => state.locations.getLocations
  );
  const { ok: editOk, isLoading: editLoading } = useSelector(
    (state) => state.profile.editAddress
  );
  const { ok: addOk, isLoading: addLoading } = useSelector(
    (state) => state.profile.addAddress
  );

  useEffect(() => {
    if (addLoading || editLoading) {
      setLoaded(true);
    }
  }, [editLoading, addLoading]);
  useEffect(() => {
    if (props.data) {
      if (editOk && loaded) props.close();
    }
  }, [editOk]);

  useEffect(() => {
    if (!props.data) {
      if (addOk && loaded) props.close();
    }
  }, [addOk]);
  const addressForm = useFormik({
    initialValues: {
      city_id: null,
      recipient: props.data?.recipient,
      postal_address: props.data?.postal_address,
      postal_code: props.data?.postal_code,
      mobile: props.data?.mobile,
      phone: props.data?.phone,
    },
    onSubmit: (values) => {
      if (props.data)
        dispatch(editAddress({ address_id: props.data.id, ...values }));
      else dispatch(addAddress(values));
    },
  });
  useEffect(() => {
    if (props.data && locationsData) {
      const city = props.data.city_province
        .replace(/^'|'$/g, "")
        .split(/\s*\-\s*/g)[0];
      const pr = props.data.city_province
        .replace(/^'|'$/g, "")
        .split(/\s*\-\s*/g)[1]
        .replace("استان ", "");
      setProvince(
        locationsData?.data?.find((item) => item.province_name == pr)
          .province_id
      );
      addressForm.setFieldValue(
        "city_id",
        locationsData?.data
          ?.find((item) => item.province_name == pr)
          ?.cities?.find((cityItem) => cityItem.city_name == city).city_id
      );
    }
  }, [locationsData]);
  return (
    <Dialog
      onBackdropClick={() => {
        props.close();
      }}
      open={props.open}
      fullWidth
    >
      <DialogTitle>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box style={{ visibility: "hidden" }}>asd</Box>
          <Box>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#202439",
                textAlign: "center",
              }}
            >
              اصلاح آدرس پستی
            </Typography>
          </Box>
          <Box>
            <IconButton
              onClick={() => {
                props.close();
              }}
            >
              <Close style={{ color: "#878A9B" }} />
            </IconButton>
          </Box>
        </Box>
      </DialogTitle>
      <DialogContent
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            onChange={addressForm.handleChange}
            value={addressForm.values.recipient}
            label="نـام و نام خانوادگی گـیرنده"
            name="recipient"
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            onChange={addressForm.handleChange}
            value={addressForm.values.mobile}
            label="شـماره هـمراه"
            name="mobile"
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            onChange={addressForm.handleChange}
            value={addressForm.values.phone}
            label="شـماره تـلفن ثـابت"
            name="phone"
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <Select
            onChange={(event) => {
              setProvince(event.target.value);
            }}
            value={province}
            style={{ margin: "0px 8px", width: "100%" }}
          >
            {locationsData?.data?.map((item) => (
              <MenuItem value={item.province_id}>
                <Typography
                  style={{
                    fontFamily: "ravi bold",
                    fontSize: 16,
                    color: "#202439",
                  }}
                >
                  {item.province_name}
                </Typography>
              </MenuItem>
            ))}
          </Select>
          <Select
            onChange={addressForm.handleChange}
            name="city_id"
            value={addressForm.values.city_id}
            style={{ margin: "0px 8px", width: "100%" }}
          >
            {locationsData?.data
              ?.find((item) => item.province_id == province)
              ?.cities?.map((city) => (
                <MenuItem value={city.city_id}>
                  <Typography
                    style={{
                      fontFamily: "ravi bold",
                      fontSize: 16,
                      color: "#202439",
                    }}
                  >
                    {city.city_name}
                  </Typography>
                </MenuItem>
              ))}
          </Select>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="نـشانی پـستی"
            onChange={addressForm.handleChange}
            value={addressForm.values.postal_address}
            name="postal_address"
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="کـد پـستی ۱۰ رقـمی"
            onChange={addressForm.handleChange}
            value={addressForm.values.postal_code}
            name="postal_code"
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            alignItems: "center",
            marginTop: 32,
          }}
        >
          <Button
            disabled={addLoading || editLoading}
            onClick={addressForm.handleSubmit}
            style={{
              width: "100%",
              backgroundColor: "#FF5252",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            {addLoading || editLoading ? (
              <CircularProgress color="inherit" />
            ) : (
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  color: "white",
                }}
              >
                {props.data ? "آپدیت آدرس" : "ثبت آدرس جدید"}
              </Typography>
            )}
          </Button>
          <Button
            onClick={() => props.close()}
            style={{
              width: "100%",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              انصراف و برگشت
            </Typography>
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
}
