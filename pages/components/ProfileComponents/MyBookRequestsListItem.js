import {
  Box,
  Button,
  Divider,
  Hidden,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";
import moment from "moment-jalaali";

export default function MyBookRequestsListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Paper
      style={{
        display: "flex",
        flexDirection: "column",
        borderRadius: 10,
        boxShadow: "0px 5px 10px rgba(0,0,0,0.05)",
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          alignItems: "center",
          justifyContent: "space-between",
          padding: matches ? 24 : 16,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "column",
            justifyContent: "space-around",
            width: !matches ? "100%" : null,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              {props.data?.id}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              شماره درخواست
            </Typography>
          </Box>
          <Divider
            style={{ margin: matches ? "0px 24px" : "8px 0px" }}
            flexItem={matches}
            orientation={matches ? "vertical" : "horizontal"}
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              {moment(props.data?.created_at).format("jYYYY/jMM/jDD")}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              تـاریخ ثـبت درخواست
            </Typography>
          </Box>
          <Divider
            style={{ margin: matches ? "0px 24px" : "8px 0px" }}
            flexItem={matches}
            orientation={matches ? "vertical" : "horizontal"}
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              {props.data?.book_title}
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              نـام کـتاب
            </Typography>
          </Box>
        </Box>
        {/* <Box
          style={{
            display: "flex",
            flexDirection: "row",
            marginTop: !matches ? 16 : 0,
            width: !matches ? "100%" : null,
            justifyContent: !matches ? "space-around" : null,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              backgroundColor: "rgba(45,182,125,0.1)",
              padding: "8px 16px",
              borderRadius: 10,
            }}
          >
            <Check style={{ color: "#2DB67D" }} />
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#2DB67D",
              }}
            >
              موجود در فروشگاه
            </Typography>
          </Box>
          <Hidden lgUp>
            <Button
              style={{
                backgroundColor: theme.palette.primary.main,
                borderRadius: 8,
              }}
            >
              <Typography
                variant="h5"
                style={{ fontFamily: "ravi medium", color: "white" }}
              >
                مشاهده و خرید
              </Typography>
              <ArrowBack
                style={{ color: "white", fontSize: 16, marginRight: 4 }}
              />
            </Button>
          </Hidden>
        </Box> */}
      </Box>
      {/* <Hidden mdDown>
        <Divider style={{ width: "100%" }} />
      </Hidden>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          padding: 16,
          paddingTop: !matches ? 0 : 16,
          justifyContent: "space-between",
        }}
      >
        <Typography
          style={{ fontFamily: "ravi medium", fontSize: 14, color: "#878A9B" }}
        >
          کتاب درخواستی شما به فروشگاه اضافه شد. شما میتوانید از طرق لینک مقابل
          کتاب موردنظر خود را خریداری کنید
        </Typography>
        <Hidden mdDown>
          <Button
            style={{
              fontSize: 16,
              fontFamily: "ravi medium",
              color: "#1273EB",
            }}
          >
            مـشاهـده کـتاب و خـریـد
            <ArrowBack style={{ color: "#1273EB" }} />
          </Button>
        </Hidden>
      </Box>
      */}
    </Paper> 
  );
}
