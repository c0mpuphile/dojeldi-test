import {
  Box,
  GridList,
  GridListTile,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAudioBooks } from "../../../redux/profile/getAudioBooks/actions";
import { getElectronicBooks } from "../../../redux/profile/getElectronicBooks/actions";
import { getPrintBooks } from "../../../redux/profile/getPrintBooks/actions";
import Filter from "../PublicComponents/FilterGenerator";
import MyBooksListItem from "./MyBooksListItem";

export default function MyBooks(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const dispatch = useDispatch();
  const types = [
    { title: "کتاب های چاپی", value: "getPrintBooks" },
    { title: "کتاب های صوتی", value: "getAudioBooks" },
    { title: "کتاب های الکترونیک", value: "getElectronicBooks" },
  ];
  const [type, setType] = useState("getPrintBooks");
  useEffect(() => {
    dispatch(getPrintBooks());
    dispatch(getAudioBooks());
    dispatch(getElectronicBooks());
  }, []);
  const {
    error: booksError,
    ok: booksOk,
    data: books,
    isLoading: booksLoading,
  } = useSelector((state) => {
    if (type) return state.profile[type];
  });
  return (
    <Box>
      <Filter
        value={type}
        setValue={(value) => setType(value)}
        typeValue="value"
        data={types}
      />
      <GridList
        cellHeight={matches ? 200 : 140}
        cols={matches ? 2 : 1}
        style={{
          maxWidth: "100vw",
          marginTop: 16,
        }}
        spacing={32}
      >
        {books?.data?.map((item) => (
          <GridListTile style={{ maxWidth: "100vw" }}>
            <MyBooksListItem data={item} />
          </GridListTile>
        ))}
      </GridList>
    </Box>
  );
}
