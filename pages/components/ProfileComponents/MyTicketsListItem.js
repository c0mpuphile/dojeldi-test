import {
  Box,
  Button,
  Divider,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check } from "@material-ui/icons";

export default function MyTicketsListItem() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Paper style={{ display: "flex" }}>
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
          padding: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "column",
            justifyContent: "space-around",
            width: !matches ? "100%" : null,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              ۴۸۸۳۲۶
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              شماره تیکت
            </Typography>
          </Box>
          <Divider
            style={{ margin: matches ? "0px 24px" : "8px 0px" }}
            flexItem={matches}
            orientation={matches ? "vertical" : "horizontal"}
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              ۲۴ مرداد ۹۹
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              تـاریخ ثـبت تـیکـت
            </Typography>
          </Box>
          <Divider
            style={{ margin: matches ? "0px 24px" : "8px 0px" }}
            flexItem={matches}
            orientation={matches ? "vertical" : "horizontal"}
          />
          <Box
            style={{
              display: "flex",
              flexDirection: !matches ? "row-reverse" : "column",
              alignItems: "center",
              justifyContent: !matches ? "space-between" : null,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              مرجوعی مرسوله کتاب آسیب دیده
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              موضوع تیکت
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: !matches ? "space-between" : null,
            width: !matches ? "100%" : null,
            marginTop: !matches ? 16 : 0,
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              backgroundColor: "rgba(45,182,125,0.1)",
              padding: "8px 16px",
              borderRadius: 10,
            }}
          >
            <Check style={{ color: "#2DB67D" }} />
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#2DB67D",
              }}
            >
              پاسـخ داده شـده
            </Typography>
          </Box>
          <Button
            style={{
              backgroundColor: "#F3F3F5",
              borderRadius: 10,
              marginRight: 8,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#202439",
              }}
            >
              مشاهده جزئیات
            </Typography>
            <ArrowBack style={{ color: "#202439", marginRight: 4 }} />
          </Button>
        </Box>
      </Box>
    </Paper>
  );
}
