import {
  Box,
  Button,
  Divider,
  IconButton,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { ArrowBack, Check, MoreVert } from "@material-ui/icons";

export default function MyMessagesListItem() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Paper
      style={{ display: "flex", flexDirection: "column", borderRadius: 10 }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          <Box style={{ height: 60 }}>
            <img src="/Thumb1.png" height="100%" />
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-start",
              marginRight: 24,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              پـاکـسازی ذهـن بـرای مـوفـق شـدن در زنـدگـی
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              ۲۶,۰۰۰ تومان
            </Typography>
          </Box>
          <Divider
            flexItem
            orientation="vertical"
            style={{ marginRight: 24, backgroundColor: "#EFF0F2" }}
          />
          <Box style={{ height: 60, marginRight: 24 }}>
            <img src="/profile.png" height="100%" />
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-start",
              marginRight: 24,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 16,
                color: "#202439",
              }}
            >
              معین سپهری
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              فروشنده کتاب
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              backgroundColor: "rgba(45,182,125,0.1)",
              padding: "8px 16px",
              borderRadius: 10,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#2DB67D",
              }}
            >
              فروشنده
            </Typography>
          </Box>
          <IconButton>
            <MoreVert />
          </IconButton>
        </Box>
      </Box>
    </Paper>
  );
}
