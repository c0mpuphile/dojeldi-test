import { Box, Button, Link, Typography } from "@material-ui/core";
import { InfoOutlined } from "@material-ui/icons";
import { Form, Formik } from "formik";
import * as Yup from "yup";

export default function UserRegister(props) {
  return (
    <Formik
      initialValues={{
        first_name: "",
        last_name: "",
        mobile: "",
        password: "",
        password_check: "",
      }}
      onSubmit={(values, { setSubmitting }) => {
        props.submit(values);
      }}
      validationSchema={Yup.object().shape({
        first_name: Yup.string().required("Hey!"),
        last_name: Yup.string().required("Hey Hey!"),
        mobile: Yup.string().matches(
          /(0|\+98)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/gi
        ),
        password: Yup.string().required(),
        password_check: Yup.string()
          .required()
          .oneOf([Yup.ref("password"), null], "password must match"),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          dirty,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          submitForm,
          handleReset,
        } = props;
        return (
          <Form
            style={{
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography
              style={{
                color: "#202439",
                fontFamily: "ravi black",
                fontSize: 28,
                textAlign: "center",
              }}
            >
              ایـجاد حـساب کـاربری
            </Typography>
            <Box style={{ width: "100%", marginTop: 24 }}>
              <input
                placeholder="نام"
                name="first_name"
                value={values.first_name}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{
                  backgroundColor: "transparent",
                  border: "solid 1px #E0E1E5",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  borderRadius: 16,
                  padding: "16px 32px",
                  width: "100%",
                }}
              />
            </Box>
            <Box style={{ width: "100%", marginTop: 24 }}>
              <input
                placeholder="نام خـانـوادگی"
                name="last_name"
                value={values.last_name}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{
                  backgroundColor: "transparent",
                  border: "solid 1px #E0E1E5",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  borderRadius: 16,
                  padding: "16px 32px",
                  width: "100%",
                }}
              />
            </Box>
            <Box style={{ width: "100%", marginTop: 24 }}>
              <input
                placeholder="شـماره هـمراه"
                name="mobile"
                value={values.mobile}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{
                  backgroundColor: "transparent",
                  border: "solid 1px #E0E1E5",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  borderRadius: 16,
                  padding: "16px 32px",
                  width: "100%",
                }}
              />
            </Box>
            <Box style={{ width: "100%", marginTop: 24 }}>
              <input
                placeholder="گـذرواژه"
                name="password"
                type="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{
                  backgroundColor: "transparent",
                  border: "solid 1px #E0E1E5",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  borderRadius: 16,
                  padding: "16px 32px",
                  width: "100%",
                }}
              />
            </Box>
            <Box style={{ width: "100%", marginTop: 24 }}>
              <input
                placeholder="تـکرار گـذرواژه"
                name="password_check"
                type="password"
                value={values.password_check}
                onChange={handleChange}
                onBlur={handleBlur}
                style={{
                  backgroundColor: "transparent",
                  border: "solid 1px #E0E1E5",
                  fontFamily: "ravi medium",
                  fontSize: 16,
                  borderRadius: 16,
                  padding: "16px 32px",
                  width: "100%",
                }}
              />
            </Box>
            <Box
              style={{
                display: "flex",
                width: "100%",
                marginTop: 16,
                alignItems: "center",
              }}
            >
              <InfoOutlined style={{ color: "#FF5252", fontSize: 18 }} />
              <Typography
                style={{
                  fontFamily: "ravi",
                  fontSize: 14,
                  color: "#878A9B",
                  marginRight: 4,
                }}
              >
                گـذرواژه انـتخابی بـاید بـیش از ۸ کـاراکتر و شـامل حـروف و
                اعـداد باشـد
              </Typography>
            </Box>
            <Button
              type="submit"
              style={{
                backgroundColor: "#FF5252",
                borderRadius: 20,
                marginTop: 32,
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi bold",
                  fontSize: "18",
                  color: "white",
                  margin: "8px 72px",
                }}
              >
                ادامـه ثـبت نام
              </Typography>
            </Button>
            <Box
              style={{
                display: "flex",
                width: "100%",
                marginTop: 16,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi",
                  fontSize: 16,
                  color: "#878A9B",
                }}
              >
                حـساب کـاربری دارید؟
              </Typography>
              <Link
                href={"/Login"}
                style={{
                  fontFamily: "ravi",
                  fontSize: 16,
                  color: "#1273EB",
                }}
              >
                وارد شوید
              </Link>
            </Box>
          </Form>
        );
      }}
    </Formik>
  );
}
