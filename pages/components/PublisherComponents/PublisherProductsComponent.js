import {
  Box,
  Button,
  GridList,
  GridListTile,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Edit } from "@material-ui/icons";
import MyBooksListItem from "../ProfileComponents/MyBooksListItem";

export default function PublisherProducts(params) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <GridList
        cellHeight={matches ? 200 : 140}
        cols={matches ? 2 : 1}
        style={{
          marginTop: 24,
          marginLeft: 0,
          marginRight: 0,
          maxWidth: "100vw",
        }}
        spacing={8}
      >
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
        <GridListTile style={{ maxWidth: "100vw" }}>
          <MyBooksListItem
            icon={<Edit style={{ color: "white" }} />}
            title="مشاهده و اصلاح"
          />
        </GridListTile>
      </GridList>
    </Box>
  );
}
