import {
  Box,
  IconButton,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";
import { ExpandMore, Headset, PlayArrow } from "@material-ui/icons";
import styles from "../../../styles/Home.module.css";

export default function SellDetails() {
  return (
    <Box
      style={{
        padding: 20,
        display: "flex",
        flexDirection: "row",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        borderRadius: 20,
        backgroundColor: "white",
        minWidth: 320,
      }}
    >
      <Box
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <img src="/book.png" />
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          marginRight: 20,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            ۲,۹۵۶
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi bold",
              fontSize: 18,
              color: "#202439",
              marginRight: 8,
            }}
          >
            نسخه
          </Typography>
        </Box>
        <Box>
          <Select
            IconComponent={ExpandMore}
            displayEmpty
            disableUnderline
            style={{ color: "#878A9B" }}
          >
            <MenuItem>
              <Typography style={{ fontFamily: "ravi", fontSize: 14 }}>
                کتاب چاپی در ماه گذشته
              </Typography>
            </MenuItem>
            <MenuItem value={1}>
              <Typography style={{ fontFamily: "ravi", fontSize: 14 }}>
                نمونه
              </Typography>
            </MenuItem>
            <MenuItem value={1}>
              <Typography style={{ fontFamily: "ravi", fontSize: 14 }}>
                نمونه
              </Typography>
            </MenuItem>
            <MenuItem value={1}>
              <Typography style={{ fontFamily: "ravi", fontSize: 14 }}>
                نمونه
              </Typography>
            </MenuItem>
          </Select>
        </Box>
        <Box style={{ marginTop: 4 }}>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#2DB67D",
            }}
          >
            ۲۴.۵٪ افزایش فروش
          </Typography>
        </Box>
      </Box>
    </Box>
  );
}
