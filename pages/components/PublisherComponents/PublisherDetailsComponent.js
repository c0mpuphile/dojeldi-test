import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  makeStyles,
  Paper,
  SvgIcon,
  Tab,
  Tabs,
  Typography,
  withStyles,
} from "@material-ui/core";
import { Headset, PlayArrow, Settings } from "@material-ui/icons";
import { useRouter } from "next/router";
export default function PublisherDetails(params) {
  const router = useRouter();
  return (
    <Paper
      style={{
        display: "flex",
        flexDirection: "column",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        borderRadius: 20,
        padding: "32px 0px",
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          padding: "0px 32px",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Avatar style={{ width: 64, height: 64 }}>P</Avatar>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              marginRight: 20,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 16,
                color: "#202439",
              }}
            >
              انـتشارات خـوارزمـی
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              ۰۹۱۰۰۸۰۱۴۱۶
            </Typography>
          </Box>
        </Box>
        <IconButton>
          <Settings />
        </IconButton>
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: "0px 32px",
          marginTop: 32,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            ۲۸
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            کـتاب صوتی
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            ۱۳
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            کـتاب الـکترونـیک
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 24,
              color: "#202439",
            }}
          >
            ۳۴
          </Typography>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            عــلاقـمـنـدی
          </Typography>
        </Box>
      </Box>
      <Divider
        orientation="horizontal"
        style={{ width: "100%", marginTop: 20 }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          padding: "0px 32px",
          marginTop: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#FF5252",
              }}
            >
              ۷۴,۰۰۰
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 18,
                color: "#FF5252",
                marginRight: 4,
              }}
            >
              تومان
            </Typography>
          </Box>
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 14,
              color: "#878A9B",
            }}
          >
            مـوجودی کـیف پـول شـما
          </Typography>
        </Box>
        <Button
          fullWidth
          style={{
            backgroundColor: "#FF5252",
            borderRadius: 10,
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "white",
            marginTop: 16,
            alignItems: "center",
          }}
        >
          درخواست برداشت از کیف پول
        </Button>
        <Button
          fullWidth
          style={{
            backgroundColor: "rgba(255,82,82,0.15)",
            borderRadius: 10,
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "#FF5252",
            marginTop: 16,
            alignItems: "center",
          }}
        >
          مـشاهده تـراکنش‌هـا
        </Button>
      </Box>
      <Divider
        orientation="horizontal"
        style={{ width: "100%", marginTop: 32 }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          marginTop: 20,
        }}
      >
        <Link
          onClick={() => {
            localStorage.removeItem("token");
            localStorage.removeItem("address");

            router.push("/Login");
          }}
          style={{
            fontFamily: "ravi medium",
            color: "#FF5252",
            fontSize: 16,
          }}
        >
          خروج از حساب کاربری
        </Link>
      </Box>
    </Paper>
  );
}
