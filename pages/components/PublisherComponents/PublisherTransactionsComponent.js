import {
  Box,
  Button,
  GridList,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { AddCircleOutline, AddCircleOutlineOutlined } from "@material-ui/icons";
import MyTransactionsListItem from "../ProfileComponents/MyTransactionsListItem";
import PublisherTransactionItemList from "./PublisherTransactionItemList";

export default function PublisherTransactions() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Paper
        style={{
          borderRadius: 20,
          marginTop: 20,
          height: "auto",
          width: "100%",
          boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
          padding: 0,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: matches ? "row" : "column",
            alignItems: "center",
            justifyContent: "space-between",
            padding: 24,
            backgroundColor: matches ? "#F8F8F9" : "transparent",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: matches ? "flex-start" : "center",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi black",
                  fontSize: 18,
                  color: "#202439",
                }}
              >
                ۷۴,۰۰۰
              </Typography>
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 14,
                  color: "#202439",
                  marginRight: 4,
                }}
              >
                تومان
              </Typography>
            </Box>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 12,
                color: "#878A9B",
              }}
            >
              موجودی فعلی کیف پول شما
            </Typography>
          </Box>
          <Button
            style={{
              width: !matches ? "100%" : null,
              fontFamily: "ravi medium",
              fontSize: 14,
              color: "white",
              backgroundColor: "#FF5252",
              borderRadius: 10,
              marginTop: !matches ? 8 : 0,
              padding: 8,
            }}
          >
            درخواست برداشت از کیف پول
          </Button>
        </Box>
        <GridList cols={1} style={{ display: "flex", flexDirection: "column" }}>
          <MyTransactionsListItem />
          <MyTransactionsListItem />
          <MyTransactionsListItem />
          <MyTransactionsListItem />
        </GridList>
      </Paper>
    </Box>
  );
}
