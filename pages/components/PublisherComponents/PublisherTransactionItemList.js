import { Box, Button, IconButton, Paper, Typography } from "@material-ui/core";
import { AddCircleOutline, ExpandLess, FileCopy } from "@material-ui/icons";

export default function PublisherTransactionItemList() {
  return (
    <Box
      style={{
        flex: "1",
        padding: "16px 16px",
        display: "flex",
        flexDirection: "row",
        backgroundColor: "transparent",
        alignItems: "center",
      }}
    >
      <Box
        style={{
          flex: "0 0 40%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginLeft: 16,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <ExpandLess style={{ color: "#2DB67D" }} />
          <Typography
            style={{
              fontFamily: "ravi Bold",
              fontSize: 16,
              color: "#2DB67D",
              marginRight: 16,
            }}
          >
            ۵۲,۰۰۰ تومان
          </Typography>
        </Box>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 16,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          ۲۴ مرداد ۹۹
        </Typography>
      </Box>
      <Box
        style={{
          flex: "1 0 0%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginRight: 16,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 16,
              color: "#878A9B",
            }}
          >
            درگاه پرداخت بانک ملی
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi",
              fontSize: 16,
              color: "#878A9B",
              marginRight: 32,
            }}
          >
            ۵۹۸۸۴۷۷۲۳۵
          </Typography>
          <IconButton style={{ marginRight: 16 }}>
            <FileCopy style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
