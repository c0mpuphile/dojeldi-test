import { Box, Button, IconButton, Typography } from "@material-ui/core";
import { Delete, Headset, MoreVert } from "@material-ui/icons";
import { BASE_URL } from "../../../services/axios";

export default function PublisherProductsListItem(props) {
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        border: "2px rgba(157,160,177,0.3) solid",
        borderRadius: 5,
        padding: 0,
        height: "100%",
      }}
    >
      <img
        src={BASE_URL + "/image/" + props.data?.image}
        onError={(event) => {
          event.target.src = "/missingbook.png";
        }}
        style={{ height: 200, borderRadius: "0px 5px 5px 0px" }}
      />
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          padding: 20,
          justifyContent: "space-between",
          flex: "auto",
        }}
      >
        <Box>
          <Box style={{ marginTop: 8 }}>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 18,
                color: "#202439",
              }}
            >
              کتاب جنایت و مکافات از فئودور داستایوفسکی
            </Typography>
          </Box>
          <Box style={{ marginTop: 4 }}>
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 14,
                color: "#1273EB",
              }}
            >
              فئودور داستایوفسکی
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Button style={{ backgroundColor: "#2DB67D", height: 32 }}>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 14,
                    color: "white",
                  }}
                >
                  مـشـاهـده و اصـلاح
                </Typography>
              </Box>
            </Button>
            <Button
              style={{
                backgroundColor: "rgba(255,82,82,0.15)",
                marginRight: 4,
                height: 32,
              }}
            >
              <Delete style={{ color: "#FF5252" }} />
            </Button>
          </Box>
          <IconButton>
            <MoreVert style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
    </Box>
  );
}
