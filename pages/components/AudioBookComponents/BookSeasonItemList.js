import { Box, Divider, IconButton, Typography } from "@material-ui/core";
import { PlayCircleFilledWhiteOutlined } from "@material-ui/icons";

export default function BookSeasonItemList(props) {
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Box>
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 32,
              color: "#202439",
              padding: 16,
            }}
          >
            ۱
          </Typography>
        </Box>
        <Box
          style={{
            marginRight: 16,
          }}
        >
          <Box>
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#878A9B",
              }}
            >
              مردی به نام اوه و یک دوچرخه که باید در جای مخصوص خودش باشد
            </Typography>
          </Box>
          <Box style={{ display: "flex", marginTop: 8 }}>
            <Typography
              style={{
                fontFamily: "ravi medium",
                backgroundColor: "#F3F3F5",
                padding: "4px 8px",
                fontSize: 16,
                color: "#878A9B",
                borderRadius: 6,
              }}
            >
              ۱۲:۴۵
            </Typography>
          </Box>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            flex: "1 0 0%",
          }}
        >
          <IconButton>
            <PlayCircleFilledWhiteOutlined style={{ color: "#878A9B" }} />
          </IconButton>
        </Box>
      </Box>
      <Divider style={{ width: "100%", margin: "24px 0px" }} />
    </Box>
  );
}
