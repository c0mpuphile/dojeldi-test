import {
  Box,
  Divider,
  IconButton,
  Paper,
  Slider,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  Forward10Rounded,
  PlayCircleFilledRounded,
  Replay10Rounded,
  SkipNextRounded,
  SkipPreviousRounded,
  VolumeOffRounded,
  VolumeUpRounded,
} from "@material-ui/icons";
import MainImage from "../ProductPageComponents/MainImage";

export default function MediaPlayer(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const DojeldiSlider = withStyles({
    root: {
      color: "#FF5252",
      height: 8,
      width: "100%",
    },
    thumb: {
      height: 24,
      width: 24,
      backgroundColor: "#fff",
      border: "2px solid white",
      marginTop: -8,
      marginLeft: -12,
    },
    active: {},
    valueLabel: {
      left: "calc(-50% + 4px)",
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
    },
  })(Slider);
  return (
    <Box
      style={{ display: "flex", flexWrap: "nowrap", flexDirection: "column" }}
    >
      <Paper
        style={{
          minHeight: 100,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: matches ? "50%" : 10,
          boxShadow: "0px 30px 50px rgba(0,0,0,0.05)",
          flexDirection: "column",
          marginTop: matches ? 0 : 105,
        }}
      >
        <img
          src="/book1.jpg"
          style={{
            height: matches ? "100%" : 210,
            boxShadow: "20px 20px 40px rgba(0,0,0,0.15)",
            borderRadius: 20,
            marginTop: matches ? 0 : -105,
          }}
        />
        {!matches ? (
          <Box
            style={{
              boxShadow: "0px 0px 20px rgba(0,0,0,0.03)",
              borderRadius: 16,
              marginTop: 24,
              zIndex: 2,
              backgroundColor: "white",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                padding: "16px 32px",
              }}
            >
              <Box>
                <Typography
                  style={{
                    fontFamily: "ravi black",
                    fontSize: 32,
                    color: "#FF5252",
                    padding: 16,
                  }}
                >
                  ۱
                </Typography>
              </Box>
              <Box
                style={{
                  marginRight: 16,
                }}
              >
                <Box>
                  <Typography
                    style={{
                      fontFamily: "ravi",
                      fontSize: 14,
                      color: "#878A9B",
                    }}
                  >
                    مردی به نام اوه و یک دوچرخه که باید در جای مخصوص خودش باشد
                  </Typography>
                </Box>
              </Box>
            </Box>
            <Divider style={{ width: "100%" }} />
            <Box
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Box
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: "20px 16px",
                }}
              >
                <Box>
                  <Typography
                    style={{
                      fontFamily: "ravi medium",
                      fontSize: 16,
                      color: "#202439",
                      marginLeft: 8,
                    }}
                  >
                    ۰۲:۲۸
                  </Typography>
                </Box>
                <Box style={{ width: "100%" }}>
                  <DojeldiSlider value={30} />
                </Box>
                <Box>
                  <Typography
                    style={{
                      fontFamily: "ravi medium",
                      fontSize: 16,
                      color: "#202439",
                      marginRight: 8,
                    }}
                  >
                    ۰۲:۵۴
                  </Typography>
                </Box>
              </Box>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <IconButton>
                  <SkipNextRounded style={{ fontSize: 32 }} />
                </IconButton>
                <IconButton>
                  <Forward10Rounded style={{ fontSize: 40 }} />
                </IconButton>
                <IconButton>
                  <PlayCircleFilledRounded
                    style={{ color: "#FF5252", fontSize: 64 }}
                  />
                </IconButton>
                <IconButton>
                  <Replay10Rounded style={{ fontSize: 40 }} />
                </IconButton>
                <IconButton>
                  <SkipPreviousRounded style={{ fontSize: 32 }} />
                </IconButton>
              </Box>
            </Box>
            <Divider style={{ width: "100%" }} />

            <Box
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                padding: "20px 96px",
              }}
            >
              <Box>
                <VolumeUpRounded
                  style={{
                    color: "#202439",
                    marginLeft: 8,
                  }}
                />
              </Box>
              <Box style={{ width: "100%" }}>
                <DojeldiSlider value={30} />
              </Box>
              <Box>
                <VolumeOffRounded
                  style={{
                    color: "#202439",
                    marginRight: 8,
                  }}
                />
              </Box>
            </Box>
          </Box>
        ) : null}
      </Paper>
      {matches ? (
        <Box
          style={{
            boxShadow: "0px 0px 20px rgba(0,0,0,0.03)",
            borderRadius: 16,
            marginTop: 24,
            zIndex: 2,
            backgroundColor: "white",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              padding: "16px 32px",
            }}
          >
            <Box>
              <Typography
                style={{
                  fontFamily: "ravi black",
                  fontSize: 32,
                  color: "#FF5252",
                  padding: 16,
                }}
              >
                ۱
              </Typography>
            </Box>
            <Box
              style={{
                marginRight: 16,
              }}
            >
              <Box>
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 14,
                    color: "#878A9B",
                  }}
                >
                  مردی به نام اوه و یک دوچرخه که باید در جای مخصوص خودش باشد
                </Typography>
              </Box>
            </Box>
          </Box>
          <Divider style={{ width: "100%" }} />
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Box
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                padding: "20px 16px",
              }}
            >
              <Box>
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 16,
                    color: "#202439",
                    marginLeft: 8,
                  }}
                >
                  ۰۲:۲۸
                </Typography>
              </Box>
              <Box style={{ width: "100%" }}>
                <DojeldiSlider value={30} />
              </Box>
              <Box>
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 16,
                    color: "#202439",
                    marginRight: 8,
                  }}
                >
                  ۰۲:۵۴
                </Typography>
              </Box>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <IconButton>
                <SkipNextRounded style={{ fontSize: 32 }} />
              </IconButton>
              <IconButton>
                <Forward10Rounded style={{ fontSize: 40 }} />
              </IconButton>
              <IconButton>
                <PlayCircleFilledRounded
                  style={{ color: "#FF5252", fontSize: 64 }}
                />
              </IconButton>
              <IconButton>
                <Replay10Rounded style={{ fontSize: 40 }} />
              </IconButton>
              <IconButton>
                <SkipPreviousRounded style={{ fontSize: 32 }} />
              </IconButton>
            </Box>
          </Box>
          <Divider style={{ width: "100%" }} />

          <Box
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              padding: "20px 96px",
            }}
          >
            <Box>
              <VolumeUpRounded
                style={{
                  color: "#202439",
                  marginLeft: 8,
                }}
              />
            </Box>
            <Box style={{ width: "100%" }}>
              <DojeldiSlider value={30} />
            </Box>
            <Box>
              <VolumeOffRounded
                style={{
                  color: "#202439",
                  marginRight: 8,
                }}
              />
            </Box>
          </Box>
        </Box>
      ) : null}
    </Box>
  );
}
