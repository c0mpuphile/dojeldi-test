import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { isArray } from "lodash";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTree } from "../../../redux/category/getTree/actions";
import { getAuthors } from "../../../redux/filters/getAuthors/actions";
import { getBookTypes } from "../../../redux/filters/getBookTypes/actions";
import { getPublishers } from "../../../redux/filters/getPublishers/actions";
import { getTags } from "../../../redux/filters/getTags/actions";
import MultiSelect from "../PublicComponents/MultiSelect";

export default function CategoryFilter(props) {
  const dispatch = useDispatch();
  const {
    error: treeError,
    data: tree,
    isLoading: treeLoading,
    ok: treeOk,
  } = useSelector((state) => state.category.getTree);
  const {
    error: publishersError,
    data: publishers,
    isLoading: publishersLoading,
    ok: publishersOk,
  } = useSelector((state) => state.filters.getPublishers);
  const {
    error: authorsError,
    data: authors,
    isLoading: authorsLoading,
    ok: authorsOk,
  } = useSelector((state) => state.filters.getAuthors);

  const {
    error: bookTypesError,
    data: bookTypes,
    isLoading: bookTypesLoading,
    ok: bookTypesOk,
  } = useSelector((state) => state.filters.getBookTypes);
  const [mainCat, setMainCat] = useState("");
  const [subCat, setSubCat] = useState("");
  const [checked, setChecked] = useState(false);
  useEffect(() => {
    dispatch(getPublishers());
    dispatch(getAuthors());
    dispatch(getBookTypes());
    dispatch(getTree());
    setChecked(false);
  }, []);

  useEffect(() => {
    if (
      props.values.categories &&
      props.values.categories.length > 0 &&
      tree &&
      !mainCat &&
      !subCat
    ) {
      setMainCat(
        tree?.data?.find((item) => item.id == props.values.categories[0])?.id ||
          ""
      );
      props.setC(
        tree?.data?.find((item) => item.id == props.values.categories[0])?.name
      );
      tree?.data?.map((item) => {
        if (
          item?.children.find((child) => child.id == props.values.categories[0])
        ) {
          setMainCat(item.id);
          setSubCat(
            item?.children.find(
              (child) => child.id == props.values.categories[0]
            )?.id
          );
          props.setC(item.name);
          props.setSC(
            item?.children.find(
              (child) => child.id == props.values.categories[0]
            )?.name
          );
        }
      });
    }
    setChecked(true);
  }, [props.values, tree]);

  useEffect(() => {
    if (checked) {
      console.log(mainCat + " : " + JSON.stringify(props.values));
      // if (subCat && mainCat) {
      //   props.setValues({
      //     ...props.values,
      //     categories: [subCat, ""],
      //   });
      // } else if (mainCat) {
      //   props.setValues({
      //     ...props.values,
      //     categories: [mainCat, ""],
      //   });
      // } else if (
      //   props.values?.categories &&
      //   props.values?.categories[0] !== ""
      // ) {
      //   props.setValues({
      //     ...props.values,
      //     categories: [""],
      //   });
      // }

      if (subCat && mainCat) {
        if (!props.values?.categories || props.values?.categories[0] != subCat)
          props.setValues({
            ...props.values,
            categories: [subCat, ""],
          });
      } else if (mainCat) {
        if (!props.values?.categories || props.values?.categories[0] != mainCat)
          props.setValues({
            ...props.values,
            categories: [mainCat, ""],
          });
      } else {
        props.setValues({
          ...props.values,
          categories: [""],
        });
      }
    }
  }, [subCat, mainCat]);

  const DojeldiCheckBox = withStyles({
    label: {
      fontFamily: "ravi",
      fontSize: 14,
      color: "#878A9B",
    },
    root: {
      marginTop: 4,
      width: "100%",
    },
  })(FormControlLabel);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        flex: "0 0 20%",
        padding: matches ? "32px 16px" : "16px 8px",
        alignItems: "center",
        overflow: matches ? "auto" : "hidden",
        maxHeight: 830,
      }}
    >
      <Box style={{ width: "100%" }}>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginTop: 32,
          }}
        >
          <Typography
            variant="h3"
            style={{ fontFamily: "ravi black", color: "#202439" }}
          >
            دسـته‌بـندی‌هـا
          </Typography>
          <a
            onClick={() => {
              props.setValues({ types: [""], publishers: [""], authors: [""] });
            }}
            variant="h6"
            style={{ fontFamily: "ravi medium", color: "#FF5252" }}
          >
            حذف همه فیلتر ها
          </a>
        </Box>
        <Box style={{ width: "100%" }}>
          <FormControl
            variant="outlined"
            style={{ width: "100%", marginTop: 16, borderRadius: 16 }}
          >
            <InputLabel
              style={{
                fontFamily: "ravi",
                backgroundColor: "#f7f8fa",
                padding: "0px 8px",
              }}
              id="main-categories"
            >
              گروه بندی اصلی
            </InputLabel>
            <Select
              onChange={(event) => {
                setMainCat(event.target.value || "");
              }}
              labelId="main-categories"
              style={{ borderRadius: 16 }}
              value={mainCat || ""}
            >
              <MenuItem value="">
                <Typography variant="h6" style={{ fontFamily: "ravi medium" }}>
                  <em>همه دسته‌بندی ها</em>
                </Typography>
              </MenuItem>
              {tree?.data?.map((cat) => (
                <MenuItem value={cat.id}>
                  <Typography
                    variant="h6"
                    style={{ fontFamily: "ravi medium" }}
                  >
                    {cat.name}
                  </Typography>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
        {tree?.data.find((item) => item.id == mainCat)?.children?.length > 0 ? (
          <Box style={{ width: "100%" }}>
            <FormControl
              variant="outlined"
              style={{ width: "100%", marginTop: 16, borderRadius: 16 }}
            >
              <InputLabel
                style={{
                  fontFamily: "ravi",
                  backgroundColor: "#f7f8fa",
                  padding: "0px 8px",
                }}
                id="main-categories"
              >
                دسته بندی کتاب
              </InputLabel>
              <Select
                onChange={(event) => {
                  setSubCat(event.target.value || "");
                }}
                value={subCat || ""}
                labelId="main-categories"
                style={{ borderRadius: 16 }}
              >
                <MenuItem value="">
                  <Typography
                    variant="h6"
                    style={{ fontFamily: "ravi medium" }}
                  >
                    <em>همه دسته‌بندی ها</em>
                  </Typography>
                </MenuItem>
                {tree?.data
                  .find((item) => item.id == mainCat)
                  ?.children.map((cat) => (
                    <MenuItem value={cat.id}>
                      <Typography
                        variant="h6"
                        style={{ fontFamily: "ravi medium" }}
                      >
                        {cat.name}
                      </Typography>
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
          </Box>
        ) : null}
      </Box>
      <Box style={{ width: "100%" }}>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginTop: 32,
          }}
        >
          <Typography
            variant="h3"
            style={{ fontFamily: "ravi black", color: "#202439" }}
          >
            نـوع کـتاب
          </Typography>
        </Box>
        <Box style={{ width: "100%" }}>
          <FormGroup>
            {bookTypes && bookTypes.length > 0
              ? bookTypes[0].map((item) => (
                  <DojeldiCheckBox
                    control={
                      <Checkbox
                        value={item}
                        color="primary"
                        checked={
                          props.values &&
                          props.values["types"] &&
                          props.values["types"].length > 0 &&
                          props.values["types"].indexOf(item) > -1
                            ? true
                            : false
                        }
                        name="checkone"
                        onChange={(event) => {
                          let items = props.values["types"]
                            ? isArray(props.values["types"])
                              ? [...props.values["types"]]
                              : [props.values["types"]]
                            : [""];
                          if (items && items.indexOf(event.target.value) > -1) {
                            items.splice(items.indexOf(event.target.value), 1);
                            props.setValues({ ["types"]: items });
                          } else {
                            props.setValues({
                              ["types"]: [...items, event.target.value],
                            });
                          }
                        }}
                      />
                    }
                    label={item}
                  />
                ))
              : null}
          </FormGroup>
        </Box>
      </Box>

      <Box
        style={{
          width: "100%",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginTop: 32,
          }}
        >
          <Typography
            variant="h3"
            style={{ fontFamily: "ravi black", color: "#202439" }}
          >
            انـتشـارات
          </Typography>
        </Box>
        <Box
          style={{
            borderRadius: 20,
            border: "1px solid #D7E1EA",
            padding: 16,
            marginTop: 16,
          }}
        >
          <MultiSelect
            values={props.values}
            setValues={(value) => {
              props.setValues(value);
            }}
            type="publishers"
            items={publishers?.data}
          />
        </Box>
      </Box>

      <Box
        style={{
          width: "100%",
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginTop: 32,
          }}
        >
          <Typography
            variant="h3"
            style={{ fontFamily: "ravi black", color: "#202439" }}
          >
            نویسنده
          </Typography>
        </Box>
        <Box
          style={{
            borderRadius: 20,
            border: "1px solid #D7E1EA",
            padding: 16,
            marginTop: 16,
          }}
        >
          <MultiSelect
            values={props.values}
            setValues={(value) => {
              props.setValues(value);
            }}
            type="authors"
            items={authors?.data}
          />
        </Box>
      </Box>
    </Box>
  );
}
