import { Grid, useMediaQuery, useTheme } from "@material-ui/core";
import FreeSend from "../Icons/FreeSend";
import FreeSuggest from "../Icons/FreeSuggest";
import Guarantee from "../Icons/Guarantee";
import PhoneSupport from "../Icons/PhoneSupport";
import TermsItem from "./TermsItem";

export default function Terms(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Grid justify={"center"} alignItems={"center"} container sm={12}>
      <Grid item xl={3} lg={6} sm={12}>
        <TermsItem
          title={"ضمانت اصالت کتاب"}
          desc="تمامی کتاب‌های ارائه شده در دوجلدی کاملا معتبر و مورد تایید ناشر است"
          icon={<Guarantee />}
        />
      </Grid>
      <Grid item xl={3} lg={6} sm={12}>
        <TermsItem
          title={"ارسـال رایـگان"}
          desc="با خیال راحت خرید کنید و کتاب‌های خریداری شده را با ارسال رایگان دریافت کنید"
          icon={<FreeSend />}
        />
      </Grid>
      <Grid item xl={3} lg={6} sm={12}>
        <TermsItem
          title={"پشتیبانی تلفنی"}
          desc="سوالی دارید؟ تیم پشتیبانی دوجلدی بصورت تمام وقت پاسخگوی سوالات شماست"
          icon={<PhoneSupport />}
        />
      </Grid>
      <Grid item xl={3} lg={6} sm={12}>
        <TermsItem
          title={"مشاوره رایگان"}
          desc="در مورد خرید خود شک دارید؟ با ما در ارتباط باشید و مشاوره رایگان دریافت کنید"
          icon={<FreeSuggest />}
        />
      </Grid>
    </Grid>
  );
}
