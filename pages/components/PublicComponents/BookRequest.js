import { Box, Button, Dialog, DialogContent, DialogTitle, IconButton, TextField, Typography } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { useFormik } from "formik";
import instance from "../../../services/axios";

export default function BookRequest(props) {
    const form = useFormik({initialValues : {
        book_title : "",
        description : "",
        author:"",
        translator:"", 
        address : "",
        },onSubmit : (values) => {
            instance.post("/user/book/request",{...values}).then(response => props.setBookRequest(false))
        }})
    return(
      <Dialog
      onBackdropClick={() => {
        props.setBookRequest(false);
      }}
      open={props.bookRequest}
      fullWidth
    >
      <DialogTitle>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box style={{ visibility: "hidden" }}>asd</Box>
          <Box>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 24,
                color: "#202439",
                textAlign: "center",
              }}
            >
              درخواست کتاب
            </Typography>
          </Box>
          <Box>
            <IconButton
              onClick={() => {
                props.setBookRequest(false);
              }}
            >
              <Close style={{ color: "#878A9B" }} />
            </IconButton>
          </Box>
        </Box>
      </DialogTitle>
      <DialogContent
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box style={{ marginTop: 16 }}>
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 14,
              textAlign: "center",
              color: "#878A9B",
            }}
          >
            مشخصات کتاب درخواستی خود را بصورت کامل در اختیار ما بگذارید. پس از
            ثبت، درخواست شما توسط تیم پشتیبانی بررسی شده و نتیجه آن در بخش
            «درخواست کتاب» در پروفایل شما قابل مشاهده است
          </Typography>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="نـام کـتاب"
            name="book_title"
            onChange={form.handleChange}
            value={form.values.book_title}
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="نـام نـویسـنده"
            name="author"
            onChange={form.handleChange}
            value={form.values.author}
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            style={{ margin: "0px 8px", width: "100%" }}
            label="نـام مـترجم (اخـتیاری)"
            name="translator"
            onChange={form.handleChange}
            value={form.values.translator}
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
           style={{ margin: "0px 8px", width: "100%" }}
           label="توضیحات"
           name="description"
           onChange={form.handleChange}
           value={form.values.description}
           multiline
           variant="outlined"
           InputProps={{
             style: { fontFamily: "ravi medium", borderRadius: 16 },
           }}
           InputLabelProps={{
             style: {
               fontFamily: "ravi medium",
               fontSize: 16,
             },
           }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: 32,
            width: "100%",
          }}
        >
          <TextField
            rows={10}
            style={{ margin: "0px 8px", width: "100%" }}
            label="نـشانی پـستی"
            name="address"
            onChange={form.handleChange}
            value={form.values.address}
            variant="outlined"
            InputProps={{
              style: { fontFamily: "ravi medium", borderRadius: 16 },
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: 16,
              },
            }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            alignItems: "center",
            marginTop: 32,
          }}
        >
          <Button
            style={{
              width: "100%",
              backgroundColor: "#FF5252",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
            onClick={form.handleSubmit}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "white",
              }}
            >
              ثبت درخواست جدید
            </Typography>
          </Button>
          <Button
            style={{
              width: "100%",
              padding: 16,
              borderRadius: 16,
              margin: "0px 8px",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 16,
                color: "#878A9B",
              }}
              onClick={() => props.setBookRequest(false)}
            >
              انصراف و برگشت
            </Typography>
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  
    )
}