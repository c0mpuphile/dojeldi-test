const getBase64 = (file) => {
  if (typeof window !== "undefined") {
    return new Promise((resolve) => {
      let fileInfo;
      let baseURL = "";
      // Make new FileReader
      let reader = new window.FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {
        // Make a fileInfo Object
        baseURL = reader.result;
        if (!file.type.includes("image")) baseURL = baseURL.split("base64,")[1];
        resolve(baseURL);
      };
      console.log(fileInfo);
    });
  } else {
    return null;
  }
};
export default getBase64;
