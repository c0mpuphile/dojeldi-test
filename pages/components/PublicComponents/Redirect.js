import { useRouter } from "next/dist/client/router";
import { route } from "next/dist/next-server/server/router";
import { useEffect } from "react";

export default function Redirect({ to }) {
  const router = useRouter();
  useEffect(() => {
    router.push(to);
  }, [to]);
  return null;
}
