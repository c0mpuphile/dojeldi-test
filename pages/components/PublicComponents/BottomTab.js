import {
  BottomNavigation,
  BottomNavigationAction,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Hidden,
  IconButton,
  Typography,
} from "@material-ui/core";
import {
  ArrowBackIos,
  ArrowForward,
  BookmarkBorderOutlined,
  CategoryOutlined,
  Close,
  HomeOutlined,
  MoreHorizOutlined,
  PersonOutline,
} from "@material-ui/icons";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTree } from "../../../redux/category/getTree/actions";
import { BASE_URL } from "../../../services/axios";
import CafeDojeldi from "../Icons/CafeDojeldi";
import Categories from "../Icons/Categories";
import MobileTabHome from "../Icons/MobileTabHome";
import MobileTabMore from "../Icons/MobileTabMore";
import MobileTabProfile from "../Icons/MobileTabProfile";

export default function BottomTab(props) {
  const [catDialogVisible, setCatDialogVisible] = useState(false);
  const router = useRouter();
  const { data, isLoading } = useSelector((state) => state.category.getTree);
  const dispatch = useDispatch();
  useEffect(() => {
    if (catDialogVisible) {
      dispatch(getTree());
    }
  }, [catDialogVisible]);
  return (
    <Hidden lgUp>
      <BottomNavigation
        showLabels
        style={{
          position: "fixed",
          bottom: 0,
          left: 0,
          width: "100%",
          boxShadow: "0px 0px 25px rgba(0,0,0,0.15)",
          zIndex: 1,
        }}
      >
        <BottomNavigationAction
          onClick={() => router.push("/")}
          label={
            <Typography style={{ fontFamily: "ravi bold", fontSize: 10 }}>
              خانه
            </Typography>
          }
          icon={<MobileTabHome />}
        />
        <BottomNavigationAction
          onClick={() => {
            setCatDialogVisible((value) => !value);
          }}
          label={
            <Typography style={{ fontFamily: "ravi bold", fontSize: 10 }}>
              دسته‌بندی
            </Typography>
          }
          icon={<Categories />}
        />
        <BottomNavigationAction
          onClick={() => router.push("/Profile")}
          label={
            <Typography style={{ fontFamily: "ravi bold", fontSize: 10 }}>
              پروفایل
            </Typography>
          }
          icon={<MobileTabProfile />}
        />
        <BottomNavigationAction
          label={
            <Typography style={{ fontFamily: "ravi bold", fontSize: 10 }}>
              کافه‌دو‌جلدی
            </Typography>
          }
          icon={<CafeDojeldi />}
        />
        <BottomNavigationAction
          label={
            <Typography
              style={{ fontFamily: "ravi bold", fontSize: 10, marginTop: 8 }}
            >
              بیشتر
            </Typography>
          }
          icon={<MobileTabMore />}
        />
      </BottomNavigation>
      <Dialog
        fullWidth={true}
        open={catDialogVisible}
        onClose={() => {
          setCatDialogVisible((value) => !value);
        }}
      >
        <DialogTitle>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <IconButton>
              <ArrowForward />
            </IconButton>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 14,
                color: "#202439",
              }}
            >
              انـتخاب دسـته‌بـندی
            </Typography>
            <IconButton
              onClick={() => {
                setCatDialogVisible((value) => !value);
              }}
            >
              <Close />
            </IconButton>
          </Box>
        </DialogTitle>
        <DialogContent dividers={true}>
          {isLoading ? (
            <CircularProgress />
          ) : (
            <>
              {data?.data.map((item) => (
                <Box
                  style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "space-between",
                    minHeight: 48,
                  }}
                >
                  <Box style={{ display: "flex", alignItems: "center" }}>
                    <img src={BASE_URL + "/image/" + item.icon} />
                    <Typography
                      style={{ fontFamily: "ravi medium", marginRight: 8 }}
                      variant="h4"
                    >
                      {item.name}
                    </Typography>
                  </Box>
                  {item?.children?.length > 0 ? (
                    <Box>
                      <IconButton>
                        <ArrowBackIos style={{ color: "#878A9B" }} />
                      </IconButton>
                    </Box>
                  ) : null}
                </Box>
              ))}
            </>
          )}
        </DialogContent>
        <DialogActions style={{ justifyContent: "center" }}>
          <Button
            onClick={() => setCatDialogVisible(false)}
            style={{ fontFamily: "ravi", fontSize: 12, color: "#878A9B" }}
          >
            انصراف و برگشت
          </Button>
        </DialogActions>
      </Dialog>
    </Hidden>
  );
}
