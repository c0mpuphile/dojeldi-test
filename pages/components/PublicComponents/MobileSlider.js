import { Box, Typography } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import { getSliders } from "../../../redux/sliders/getSliders/actions";
import { BASE_URL } from "../../../services/axios";

export default function MobileSlider(props) {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    centerMode: true,
    centerPadding: "32px",
  };
  const dispatch = useDispatch();
  const {
    isLoading: sliderLoading,
    data: sliderData,
    error: sliderError,
  } = useSelector((state) => state.sliders.getSliders);
  useEffect(() => {
    dispatch(getSliders());
  }, []);

  return (
    <Slider
      style={{ marginTop: 20, paddingBottom: 0, maxWidth: "100%" }}
      {...settings}
    >
      {sliderData?.data.slice(0, 5).map((item) => (
        <div>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src={`${BASE_URL}/image/${item.image_url}`}
              style={{
                width: "75vw",
                boxShadow: "0px 10px 30px rgba(0,0,0,0.2)",
                borderRadius: 8,
              }}
            />

            <Typography
              style={{
                fontFamily: "ravi bold",
                fontSize: 12,
                color: "#202439",
                marginTop: 8,
              }}
            >
              {item.title}
            </Typography>
            <Typography
              style={{ fontFamily: "ravi", fontSize: 10, color: "#878A9B" }}
            >
              {item.description}
            </Typography>
            <Box
              style={{ display: "flex", flexDirection: "row", marginTop: 4 }}
            >
              <ArrowBack style={{ fontSize: 12, color: "#1273EB" }} />

              <Typography
                href={item.link}
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 10,
                  color: "#1273EB",
                }}
              >
                مـشاهده و خـرید
              </Typography>
            </Box>
          </Box>
        </div>
      ))}
    </Slider>
  );
}
