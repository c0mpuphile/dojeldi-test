import {
  Box,
  Button,
  Card,
  CardMedia,
  GridListTile,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import Link from "next/link";
import {
  AccessibilityNew,
  AddShoppingCart,
  ArrowBack,
  Headset,
  StarRounded,
} from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import { useState } from "react";
import Tech from "../Icons/Tech";
import { BASE_URL } from "../../../services/axios";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function CategoryListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));

  const [hover, setHover] = useState(0);
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        backgroundImage: `url("${BASE_URL + "/image/" + props.data?.image}")`,
        backgroundSize: "100% auto",
        backgroundRepeat: "no-repeat",
        borderRadius: 4,
        height: "auto",

        boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
      }}
      onMouseEnter={() => {
        setHover(3);
      }}
      onMouseLeave={() => {
        setHover(0);
      }}
    >
      <Box
        style={{
          flex: "100%",
          backgroundColor: "transparent",
          backgroundImage: "linear-gradient(transparent, rgba(0,0,0,0.8))",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
        }}
      >
        <img
          src={BASE_URL + "/image/" + props.data?.image}
          style={{
            height: "auto",
            visibility: "hidden",
          }}
        />
      </Box>

      <Box
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          display: "flex",
          alignItems: "flex-start",
          flexDirection: "column",
          justifyContent: "space-evenly",
          padding: matches ? "32px 32px" : "8px 16px",
        }}
      >
        <Card
          style={{
            backdropFilter: "blur(30px)",
            backgroundColor: "transparent",
            padding: 16,
          }}
          elevation={0}
        >
          <img src={BASE_URL + "/image/" + props.data?.icon} />
        </Card>
        <Typography
          style={{
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "white",
            display: matches ? "block" : "none",
            marginTop: 24,
          }}
        >
          {props.data?.description}
        </Typography>
        <Typography
          variant="h2"
          style={{
            fontFamily: "ravi black",
            color: "white",
            marginTop: 16,
          }}
        >
          {props.data?.name}
        </Typography>
        <Link href={`/Category?categories=${props.data?.id}&categories=`}>
          <Button
            style={{
              border: matches ? "2px solid white" : "none",
              borderRadius: 10,
              marginTop: 16,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: matches ? 16 : 10,
                color: "white",
                margin: matches ? "0px 4px" : null,
              }}
            >
              مشاهده همه
            </Typography>
            <ArrowBack
              style={{
                margin: "0px 4px",
                color: "white",
                fontSize: matches ? null : 12,
              }}
            />
          </Button>
        </Link>
      </Box>
    </Box>
  );
}
