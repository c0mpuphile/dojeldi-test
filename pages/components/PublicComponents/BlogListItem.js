import {
  Avatar,
  Box,
  Button,
  Card,
  CardMedia,
  GridListTile,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AccessibilityNew,
  AddShoppingCart,
  ArrowBack,
  Favorite,
  Headset,
  Share,
  StarRounded,
} from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import { useState } from "react";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function BlogListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));

  const [hover, setHover] = useState(1);
  return (
    <Box
      boxShadow={hover}
      style={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
      }}
      onMouseEnter={() => {
        setHover(3);
      }}
      onMouseLeave={() => {
        setHover(1);
      }}
    >
      <Box
        style={{
          flex: "0 0 40%",
          backgroundImage: 'url("/Mask.png")',
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          display: "flex",
          padding: 30,
          flexDirection: "column",
        }}
      >
        <Box style={{ display: "flex", flexDirection: "row" }}>
          <Box
            style={{
              border: "1px solid #E0E1E5",
              borderRadius: 5,
              padding: "0px 12px",
              margin: "0px 2px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h6"
              style={{ fontFamily: "ravi medium", color: "white" }}
            >
              معرفی کتاب
            </Typography>
          </Box>
          <Box
            style={{
              border: "1px solid #E0E1E5",
              borderRadius: 5,
              padding: "0px 12px",
              margin: "0px 2px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Typography
              variant="h6"
              style={{ fontFamily: "ravi medium", color: "white" }}
            >
              کسب و کار و مـوفـقیت
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box
        style={{
          flex: "0 0 60%",
          display: "flex",
          flexDirection: "column",
          padding: 30,
          backgroundColor: "white",
        }}
      >
        <Avatar
          src="/profile.png"
          style={{
            marginTop: -48,
            width: matches ? 60 : 40,
            height: matches ? 60 : 40,
            border: "3px solid white",
          }}
        />
        <Box
          style={{
            flex: "100%",
            justifyContent: "space-around",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography
            variant="h3"
            style={{
              fontFamily: "ravi black",
              color: "#202439",
              marginTop: 24,
            }}
          >
            معرفی کتاب چـهار اثر فلورانس اسـکاول شین؛ مـقدمه‌ای بـر مـوفـقیت
          </Typography>
          <Typography
            variant="h5"
            style={{
              fontFamily: "ravi medium",

              color: "#B8BAC2",
            }}
          >
            چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از ۶۰ بار
            مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است. نویسنده این
            کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
          </Typography>
        </Box>
        <Box
          style={{
            flex: "20%",
            justifyContent: "space-between",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Box>
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi medium",
                color: "#1273EB",
              }}
            >
              ۱۳۹۹/۰۶/۲۱
            </Typography>
          </Box>
          <Box style={{ display: "flex", alignItems: "center" }}>
            <IconButton>
              <Favorite style={{ fontSize: matches ? null : 16 }} />
            </IconButton>
            <IconButton>
              <Share style={{ fontSize: matches ? null : 16 }} />
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
