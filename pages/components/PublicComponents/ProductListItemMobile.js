import {
  Badge,
  Box,
  Card,
  CardMedia,
  GridListTile,
  IconButton,
  Link,
  makeStyles,
  Paper,
  SvgIcon,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { Add, AddShoppingCart, Headset, StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import Router from "next/router";
import { useState } from "react";
import { BASE_URL } from "../../../services/axios";
import Book from "../Icons/Book";
import BookAudio from "../Icons/BookAudio";
import BookElec from "../Icons/BookElec";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);

const Bascket = (props) => {
  return (
    <SvgIcon>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="23.504"
        height="23.5"
        viewBox="0 0 23.504 23.5"
      >
        <g id="Bag" transform="translate(-0.052 -0.028)">
          <path
            id="Path_33955"
            d="M16.071,16.8H5.917c-3.73,0-6.591-1.268-5.778-6.37L1.085,3.52C1.586.974,3.312,0,4.826,0H17.207c1.536,0,3.162,1.048,3.741,3.52l.946,6.914C22.585,14.96,19.8,16.8,16.071,16.8Z"
            transform="translate(0.801 5.974)"
            fill="none"
            stroke="#200e32"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-miterlimit="10"
            stroke-width="1.5"
          />
          <path
            id="Path_33956"
            d="M10.533,4.945A5.107,5.107,0,0,0,5.277,0h0a5.429,5.429,0,0,0-3.73,1.441A4.8,4.8,0,0,0,0,4.945H0"
            transform="translate(6.507 0.778)"
            fill="none"
            stroke="#200e32"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-miterlimit="10"
            stroke-width="1.5"
          />
          <path
            id="Line_192"
            d="M.49.458H.435"
            transform="translate(14.901 10.419)"
            fill="none"
            stroke="#200e32"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-miterlimit="10"
            stroke-width="1.5"
          />
          <path
            id="Line_193"
            d="M.49.458H.435"
            transform="translate(7.809 10.419)"
            fill="none"
            stroke="#200e32"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-miterlimit="10"
            stroke-width="1.5"
          />
        </g>
      </svg>
    </SvgIcon>
  );
};
export default function ProductListItemMobile(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Link
      style={{ textDecoration: "none" }}
      onClick={() => {
        Router.push({
          pathname: "/Product",
          query: { id: props.data?.id },
        });
      }}
    >
      <Box
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Paper
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            marginTop: 45,
            boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
            justifyContent: "center",
          }}
        >
          <img
            src={BASE_URL + "/image/" + props.data?.image}
            onError={(event) => {
              event.target.src = "/missingbook.png";
            }}
            width="140px"
            style={{ marginTop: -45, borderRadius: 4 }}
          />
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              minWidth: 60,
              marginTop: 8,
            }}
          >
            {props.data?.has_print_version ? (
              <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                <Book width="16" height="18" color="#1273EB" />
              </Box>
            ) : null}
            {props.data?.has_electronic_version ? (
              <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                <BookElec width="20" height="18" color="#FF5252" />
              </Box>
            ) : null}
            {props.data?.has_audio_version ? (
              <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                <BookAudio width="18" height="18" color="#FF9A27" />
              </Box>
            ) : null}
          </Box>
          <Box style={{ margin: "0px 16px" }}>
            <Typography
              variant="h3"
              style={{
                fontFamily: "ravi medium",
                color: "#202439",
                marginTop: 8,
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Box
            style={{
              marginTop: 8,
              marginBottom: 8,
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              padding: "0px 16px",
              width: "100%",
              alignItems: "center",
            }}
          >
            <IconButton>
              <Badge
                badgeContent={"+"}
                color={"primary"}
                anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
              >
                <Bascket />
              </Badge>
            </IconButton>
            <Box
              style={{
                flex: "1 0 0%",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end",
              }}
            >
              {props.data?.print_version_sale_price !==
              props.data?.print_version_price ? (
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "row",
                  }}
                >
                  <Typography
                    variant="h5"
                    style={{
                      fontFamily: "ravi light",

                      textDecorationLine: "line-through",
                      color: "#878A9B",
                    }}
                  >
                    {props.data?.print_version_price}
                  </Typography>
                  <Typography
                    variant="h5"
                    style={{
                      backgroundColor: "#FF5252",
                      borderRadius: 5,
                      color: "white",
                      fontFamily: "ravi",
                      padding: "0px 4px",
                    }}
                  >
                    ۲۵٪
                  </Typography>
                </Box>
              ) : null}
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="h3"
                  style={{
                    color: "black",
                    fontFamily: "ravi black",
                  }}
                >
                  {props.data?.print_version_sale_price}
                </Typography>
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    color: "#878A9B",
                    fontSize: 12,
                    marginRight: 4,
                  }}
                >
                  تومان
                </Typography>
              </Box>
            </Box>
          </Box>
        </Paper>
      </Box>
    </Link>
  );
}
