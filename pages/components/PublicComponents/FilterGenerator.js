import { Box, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import { isArray } from "lodash";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function Filter(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        overflow: "auto",
        justifyContent: "flex-start",
      }}
    >
      {props.data?.map((filter) => (
        <button
          onClick={(event) => {
            if (props.value != filter[props.typeValue])
              props.setValue(filter[props.typeValue]);
            else props.setValue("");
          }}
          style={{
            backgroundColor:
              props.value == filter[props.typeValue] ? "white" : "transparent",
            borderColor:
              props.value == filter[props.typeValue] ? "#FF5252" : "#D7E1EA",
            borderStyle: "solid",
            borderWidth: 2,
            borderRadius: 10,
            margin: "0px 4px",
            alignItems: "center",
          }}
        >
          <Typography
            variant="h5"
            style={{
              fontFamily: "ravi",
              color:
                props.value == filter[props.typeValue] ? "#202439" : "#878A9B",
              margin: "4px 4px",
            }}
            noWrap
          >
            {filter.title}
          </Typography>
        </button>
      ))}
    </Box>
  );
}
