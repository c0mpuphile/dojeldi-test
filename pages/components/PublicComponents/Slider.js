import {
  Box,
  Button,
  Card,
  CardMedia,
  Fade,
  Grid,
  GridListTile,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { AccessibilityNew } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSliders } from "../../../redux/sliders/getSliders/actions";
import { BASE_URL } from "../../../services/axios";
import Carrot from "../Icons/Carrot";
import Love from "../Icons/Love";
import Student from "../Icons/Student";
import Tech from "../Icons/Tech";
import Triangle from "../Icons/Triangle";

export default function Slider(props) {
  const dispatch = useDispatch();
  const {
    isLoading: sliderLoading,
    data: sliderData,
    error: sliderError,
  } = useSelector((state) => state.sliders.getSliders);
  useEffect(() => {
    dispatch(getSliders());
  }, []);

  useEffect(() => {
    if (sliderData?.data?.length > 0) {
      setExpand(sliderData.data[0].id);
    }
  }, [sliderData]);
  const theme = useTheme();
  const [expand, setExpand] = useState(2);
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box style={{ maxheight: "100%", padding: "0px 8px" }}>
      <Grid
        container
        spacing={2}
        lg={12}
        style={{
          marginTop: "100px",
          justifyContent: "center",
        }}
      >
        {sliderData &&
          sliderData?.data.slice(0, 5).map((item) => (
            <Grid
              item
              lg={expand === item.id ? 6 : 1}
              style={{
                minHeight: "600px",
                transition: theme.transitions.create("all", {
                  easing: theme.transitions.easing.sharp,
                  duration: theme.transitions.duration.leavingScreen,
                }),
              }}
            >
              <Card
                onClick={() => {
                  setExpand(item.id);
                }}
                style={{ borderRadius: "16px" }}
                elevation={8}
              >
                <CardMedia
                  image={`${BASE_URL}/image/${item.image_url}`}
                  style={{
                    height: "600px",
                    display: "flex",
                    alignItems: "flex-end",
                  }}
                >
                  <Box
                    style={{
                      display: "flex",
                      width: "100%",
                      position: "relative",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Box
                      style={{
                        width: "100%",
                        backdropFilter: "blur(30px)",
                        padding: 16,
                        justifyContent: "center",
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      {expand == item.id ? (
                        <Box
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            width: "100%",
                          }}
                        >
                          <Box
                            style={{
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <Fade
                              in={expand === item.id}
                              style={{ transitionDelay: "100ms" }}
                            >
                              <Card
                                style={{
                                  width: 60,
                                  height: 60,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  borderRadius: 16,
                                  boxShadow: "0px 5px 20px rgba(0,0,0,0.10)",
                                }}
                              >
                                <img
                                  src={BASE_URL + "/image/" + item.icon_url}
                                  style={{ width: 32 }}
                                />
                              </Card>
                            </Fade>
                            <Fade
                              in={expand === item.id}
                              style={{ transitionDelay: "100ms" }}
                            >
                              <Box
                                style={{
                                  display: "flex",
                                  flexDirection: "column",
                                  marginRight: 16,
                                }}
                              >
                                <Typography
                                  variant="h3"
                                  style={{
                                    fontFamily: "ravi bold",
                                    color: "white",
                                  }}
                                >
                                  {item.title}
                                </Typography>
                                <Typography
                                  variant="h6"
                                  style={{
                                    fontFamily: "ravi light",
                                    color: "white",
                                    opacity: "50%",
                                  }}
                                >
                                  {item.description}
                                </Typography>
                              </Box>
                            </Fade>
                          </Box>
                          <Fade
                            in={expand === item.id}
                            style={{ transitionDelay: "100ms" }}
                          >
                            <Button
                              href={item.link}
                              style={{
                                margin: "0px 32px",
                                background: "rgba(255, 255, 255, 0.3)",
                              }}
                            >
                              <Typography
                                variant="h6"
                                style={{
                                  fontFamily: "ravi medium",
                                  color: "white",
                                }}
                              >
                                مـشاهده و خـرید
                              </Typography>
                            </Button>
                          </Fade>
                        </Box>
                      ) : (
                        <Box
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Card
                            style={{
                              width: 60,
                              height: 60,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              borderRadius: 16,
                              boxShadow: "0px 5px 20px rgba(0,0,0,0.10)",
                            }}
                          >
                            <img
                              src={BASE_URL + "/image/" + item.icon_url}
                              style={{ width: 32 }}
                            />
                          </Card>
                        </Box>
                      )}
                    </Box>
                  </Box>
                </CardMedia>
              </Card>
            </Grid>
          ))}
      </Grid>
    </Box>
  );
}
