import { Box, Button, Typography, withStyles } from "@material-ui/core";
import { AddCircleOutlineOutlined, Notifications } from "@material-ui/icons";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../../redux/cart/addToCart/actions";
import { getCartOnline } from "../../../redux/cart/getCartOnline/actions";

export default function ProductCartType(props) {
  const dispatch = useDispatch();
  const { ok, error, isLoading: loading, data } = useSelector(
    (state) => state.cart.addToCart
  );
  const {
    ok: okCart,
    error: errorCart,
    isLoading: loadingCart,
    data: dataCart,
  } = useSelector((state) => state.cart.getCartOnline);
  const [added, setAdded] = useState();
  const router = useRouter();
  useEffect(() => {
    if (ok) {
      dispatch(getCartOnline());
    }
  }, [ok]);
  useEffect(() => {
    setAdded(dataCart?.data?.items.find((item) => item.id == router.query.id));
  }, [dataCart, router]);
  const AddToCartButton = withStyles({
    root: {
      borderStyle: !props.disable && added ? "solid" : "none",
      backgroundColor:
        !props.disable && !added
          ? "#FF5252"
          : props.disable
          ? "#F3F3F5"
          : "white",

      display: "flex",
      flexDirection: "row",
      borderRadius: 10,
      maxHeight: 40,
      borderColor: !props.disable && added ? "#FF5252" : null,
      borderWidth: !props.disable && added ? 1 : null,
    },
  })(Button);
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        minHeight: 72,
        margin: "16px 32px 16px 24px",
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
          alignItems: "center",
        }}
      >
        {props.icon}
        <Typography
          variant="h6"
          style={{
            fontFamily: "ravi bold",
            color: !props.disable
              ? props.color
                ? props.color
                : "#1273EB"
              : "#B7B9C3",
            maxWidth: 5,
            margin: "0px 8px",
          }}
        >
          {props.typeFa}
        </Typography>
      </Box>

      <Box
        style={{
          flex: "0 0 75%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        {!props.disable ? (
          <Box
            style={{
              flex: "1 0 0%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi light",

                  textDecorationLine: "line-through",
                  color: "#878A9B",
                }}
              >
                {props.price}
              </Typography>
              <Typography
                variant="h5"
                style={{
                  backgroundColor: "#FF5252",
                  borderRadius: 5,
                  color: "white",
                  fontFamily: "ravi",
                  padding: "0px 4px",
                }}
              >
                ۲۵٪
              </Typography>
            </Box>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h3"
                style={{
                  fontFamily: "ravi black",
                }}
              >
                {props.salePrice}
              </Typography>
              <Typography
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                  fontSize: 12,
                }}
              >
                تومان
              </Typography>
            </Box>
          </Box>
        ) : (
          <Box
            style={{ alignItems: "center", flex: "1 0 0%", marginRight: 16 }}
          >
            <Typography
              variant="h4"
              style={{
                fontFamily: "ravi bold",
                color: "#B7B9C3",
                textAlign: "center",
              }}
            >
              به‌زودی
            </Typography>
          </Box>
        )}
        <AddToCartButton
          disabled={props.disable || loading || added}
          onClick={() => {
            dispatch(addToCart({ id: router.query.id, type: props.type }));
          }}
        >
          {!props.disable && !added ? (
            <AddCircleOutlineOutlined
              style={{
                color: "white",
                fontSize: 16,
                marginLeft: 4,
              }}
            />
          ) : props.disable ? (
            <Notifications
              style={{
                color: "#B7B9C3",
                fontSize: 16,
                marginLeft: 4,
              }}
            />
          ) : null}
          <Typography
            variant="h6"
            style={{
              fontFamily: "ravi medium",
              color: !props.disable && !added ? "white" : "black",
            }}
          >
            {props.disable
              ? "موجود شد خبرم کن"
              : added
              ? "اضافه شده به سبد خرید"
              : "اضافه کردن به سبد خرید"}
          </Typography>
        </AddToCartButton>
      </Box>
    </Box>
  );
}
