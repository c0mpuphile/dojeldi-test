import {
  Box,
  Button,
  Divider,
  Grid,
  Hidden,
  IconButton,
  TableFooter,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import {
  ArrowBack,
  Instagram,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import Terms from "./Terms";

export default function Footer(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box>
      <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
        }}
      >
        <Terms />
      </Box>
      <Box
        style={{
          display: "flex",
          marginTop: 80,
          justifyContent: "center",
        }}
      >
        <Grid
          container
          disableGutters
          spacing={2}
          style={{ justifyContent: "center", maxWidth: "100%" }}
        >
          <Grid item lg={10} style={{ maxWidth: "100%" }}>
            <Box
              style={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <a
                href="/"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Typography
                  variant="h2"
                  style={{ fontFamily: "mreg", color: "#202439" }}
                >
                  DOJELDI
                </Typography>
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi medium",
                    color: "#B2B2B2",
                    marginRight: 8,
                  }}
                >
                  مـرجع کـتاب ایـران
                </Typography>
              </a>
              <Box style={{ padding: "0px 16px" }}>
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: matches ? 16 : 10,
                    marginTop: 32,
                    textAlign: "center",
                    color: matches ? "black" : "#B8BAC2",
                  }}
                >
                  مجموعه دوجلـــدی انواع کتاب را به درخواست مشتریان گرانقــــدر
                  برایشان تهیه و ارسال میکند، دو جلدی همان دوبال کتاب است؛ که یک
                  بال آن علم و ایده است و بال دیگرش عملکرد این مجموعه که با نام
                  کتاب متاب در سال ۱۳۸۶ فعالیت خود را به صورت فروش تلفنی آغاز
                  کرده و درسال ۱۳۹۴ با راه اندازی سایت و صفحات فضای مجازی نام
                  خود را به دو جلدی تغییرداد؛ با افتخار بعد از گذشت حدود سه سال
                  خدمت به مشتریان خود، درحال حاضر این مجموعه افتخار خدمت به بیش
                  از صد هزار مشتری عزیز در سراسر کشور را دارد. دوجلدی همواره در
                  تکاپوی رشد و بروز رسانی خود بوده و با توجه به مسائل مربوط به
                  کاغذ قدم در راه کتاب الکترونیک و صوتی نهاده است
                </Typography>
              </Box>
              <Box style={{ marginTop: 32 }}>
                <IconButton>
                  <Twitter style={{ color: "#202439" }} />
                </IconButton>
                <IconButton>
                  <Instagram style={{ color: "#202439" }} />
                </IconButton>
                <IconButton>
                  <WhatsApp style={{ color: "#202439" }} />
                </IconButton>
                <IconButton>
                  <Telegram style={{ color: "#202439" }} />
                </IconButton>
              </Box>
            </Box>
            <Divider
              style={{ backgroundColor: "#E0E1E5", marginTop: 32 }}
              variant="middle"
            />
            <Grid
              disableGutters
              container
              spacing={2}
              style={{ justifyContent: "center", maxWidth: "100%" }}
            >
              <Hidden mdDown>
                <Grid
                  item
                  lg={3}
                  style={{ marginTop: 20, justifyContent: "center" }}
                >
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        صـفحه اصـلی
                      </Button>
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـتاب‌هـای عمومی
                      </Button>
                    </Box>
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        دسـته‌بـندی‌هـا
                      </Button>
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـتاب‌هـای کمک درسی
                      </Button>
                    </Box>
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـافه دوجـلدی
                      </Button>
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـتاب‌هـای دانشگاهی
                      </Button>
                    </Box>
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        پشـتیبـانی
                      </Button>
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـتاب‌هـای کودک و نوجوان
                      </Button>
                    </Box>
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        دربـاره مـا
                      </Button>
                      <Button
                        style={{
                          flex: "100%",
                          fontFamily: "ravi",
                          fontSize: 14,
                        }}
                      >
                        کـتاب‌هـای زبان خارجی
                      </Button>
                    </Box>
                  </Box>
                  <Divider orientation="vertical" flexItem />
                </Grid>
              </Hidden>
              <Grid item lg={4} style={{ marginTop: 20, maxWidth: "100%" }}>
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "ravi black",
                      fontSize: 21,
                      textAlign: "center",
                    }}
                  >
                    مـجوزهـای مـجمـوعه دوجـلدی
                  </Typography>
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-around",
                      marginTop: 32,
                    }}
                  >
                    <img
                      style={{ width: matches ? null : "25vw" }}
                      src="logo_x.png"
                    />
                    <img
                      style={{ width: matches ? null : "25vw" }}
                      src="logo_z.png"
                    />
                    <img
                      style={{ width: matches ? null : "25vw" }}
                      src="logoe.png"
                    />
                  </Box>
                </Box>
              </Grid>
              <Grid item lg={3} style={{ marginTop: 20, maxWidth: "100%" }}>
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "ravi black",
                      fontSize: 21,
                      textAlign: "center",
                    }}
                  >
                    عضویت در خبرنامه
                  </Typography>
                  <Box
                    style={{
                      border: "1px solid #E0E1E5",
                      borderRadius: 10,
                      display: "flex",
                      marginTop: 20,
                      padding: "4px 16px",
                      marginLeft: "16px",
                      marginRight: "16px",
                    }}
                  >
                    <input
                      style={{
                        border: "none",
                        flex: "90%",
                        borderRadius: 10,
                        fontFamily: "ravi",
                        fontSize: 16,
                        backgroundColor: "transparent",
                      }}
                      placeholder="نشانی پست الکترونیک"
                    />
                    <IconButton style={{ flex: "10%" }}>
                      <ArrowBack />
                    </IconButton>
                  </Box>
                  <Typography
                    style={{
                      fontFamily: "ravi medium",
                      fontSize: 14,
                      color: "#B8BAC2",
                      marginTop: 24,
                    }}
                  >
                    با عضویت در خبرنامه دوجلدی، از جدیدترین محصولات، تخفیف‌ها و
                    اخبار دنیای کتاب و مطالعه باخبر شوید
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <Hidden lgUp>
        <Box style={{ height: 64, visibility: "hidden" }}></Box>
      </Hidden>
    </Box>
  );
}
