import {
  Badge,
  Box,
  Card,
  CardMedia,
  GridListTile,
  IconButton,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { AddShoppingCart, Headset, StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import { useState } from "react";
import Bascket from "../Icons/Bascket";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import Book from "../Icons/Book";
import BookElec from "../Icons/BookElec";
import BookAudio from "../Icons/BookAudio";
import Router from "next/router";
import { BASE_URL } from "../../../services/axios";
const StyledRating = withStyles({
  iconFilled: {
    color: "#ff6453",
  },
  iconHover: {
    color: "#ff6453",
  },
})(Rating);
export default function ProductListItem(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));

  const [hover, setHover] = useState(0);
  return (
    <GridListTile>
      <Box
        boxShadow={hover}
        style={{
          display: "flex",
          flexDirection: "column",
        }}
        onMouseEnter={() => {
          setHover(3);
        }}
        onMouseLeave={() => {
          setHover(0);
        }}
      >
        {props.data?.print_version_discount.length > 0 ? (
          <Box
            style={{
              display: "flex",
              backgroundColor: "transparent",
            }}
          >
            <Typography
              variant="h6"
              style={{
                textAlign: "center",
                fontFamily: "ravi light",
                color: "white",
                flex: "0 1 40%",
                backgroundColor: theme.palette.primary.main,
                padding: 8,
              }}
            >
              {props.data?.print_version_discount[0]} درصد تحفیف
            </Typography>
          </Box>
        ) : null}
        <Card
          onClick={() => {
            Router.push({
              pathname: "/Product",
              query: { id: props.data?.id },
            });
          }}
          style={{
            flex: "100%",
            display: "flex",
            flexDirection: "row",
            backgroundColor: "transparent",
          }}
        >
          <Box style={{ flex: "0 1 40%" }}>
            <CardMedia
              component="img"
              src={BASE_URL + "/image/" + props.data?.image}
              onError={(event) => {
                event.target.src = "/missingbook.png";
              }}
              style={{
                backgroundSize: "cover",
                backgroundPosition: "center",
                height: "100%",
              }}
            />
          </Box>
          <Box
            style={{
              display: "flex",
              flex: "0 1 60%",
              flexDirection: "column",
              border: "solid #e0e1e5 1px",
              borderTopLeftRadius: 8,
              borderBottomLeftRadius: 8,
              borderRight: "none",
              padding: 8,
              backgroundColor: "transparent",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-start",
                margin: "8px 0px",
              }}
            >
              {props.data?.has_print_version ? (
                <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                  <Book width="16" height="18" color="#1273EB" />
                </Box>
              ) : null}
              {props.data?.has_electronic_version ? (
                <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                  <BookElec width="20" height="18" color="#FF5252" />
                </Box>
              ) : null}
              {props.data?.has_audio_version ? (
                <Box style={{ height: "0.75rem", margin: "0px 8px" }}>
                  <BookAudio width="18" height="18" color="#FF9A27" />
                </Box>
              ) : null}
            </Box>
            <Box
              style={{
                flex: "100%",
                alignItems: "center",
                justifyContent: "flex-start",
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Typography
                variant="h4"
                style={{
                  fontFamily: "ravi bold",
                  color: "#202439",
                  textAlign: "right",
                  margin: "8px 8px",
                }}
              >
                {props.data?.persian_title}
              </Typography>
            </Box>
            <Box
              style={{
                flex: "100%",
                alignItems: "center",
                justifyContent: "flex-start",
                display: "flex",
                flexDirection: "row",
                zIndex: 2,
              }}
            >
              <a
                onClick={(e) => {
                  e.stopPropagation();
                  Router.push(
                    `/Category?authors=${props.data?.author.map(
                      (item) => "&authors=" + item.id
                    )}`
                  );
                }}
              >
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#1273eb",
                    textAlign: "right",
                    margin: "8px 8px",
                  }}
                >
                  {props.data?.author.map((item) => item.title + " ")}
                </Typography>
              </a>
            </Box>
            <Box
              style={{
                flex: "100%",
                alignItems: "center",
                justifyContent: "flex-start",
                display: "flex",
                flexDirection: "row",
              }}
            >
              <StyledRating
                icon={<StarRounded fontSize="inherit" />}
                name="read-only"
                value={4}
                readOnly
                style={{ marginRight: "4px" }}
              />
            </Box>

            <Box
              style={{
                flex: "100%",
                alignItems: "center",
                justifyContent: "space-between",
                display: "flex",
                flexDirection: "row",
                marginTop: 32,
                marginBottom: 16,
              }}
            >
              <Box>
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi",
                    color: "#c0c1c9",
                    textAlign: "right",
                    margin: 8,
                    textDecoration: "line-through",
                  }}
                >
                  {props.data?.print_version_price} تومان
                </Typography>
                <Typography
                  variant="h3"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#202439",
                    textAlign: "right",
                    marginRight: "8px",
                    flex: "100%",
                  }}
                >
                  {props.data?.print_version_sale_price} تومان
                </Typography>
              </Box>

              <IconButton>
                <Badge
                  badgeContent={"+"}
                  color={"primary"}
                  anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                >
                  <Bascket />
                </Badge>
              </IconButton>
            </Box>
          </Box>
        </Card>
      </Box>
    </GridListTile>
  );
}
