import styles from "../../../styles/Home.module.css";
import {
  AppBar,
  Toolbar,
  Grid,
  Box,
  Select,
  MenuItem,
  Typography,
  Badge,
  IconButton,
  withStyles,
  Avatar,
  Button,
  useTheme,
  Fade,
  Divider,
  Hidden,
  useMediaQuery,
  SvgIcon,
  ClickAwayListener,
  makeStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from "@material-ui/core";
import Link from "next/link";
import {
  AddBoxOutlined,
  ArrowDropDown,
  BookmarkBorderOutlined,
  Close,
  DescriptionOutlined,
  ExpandMore,
  FolderOutlined,
  HelpOutlineOutlined,
  Public,
  ShoppingCartOutlined,
  WidgetsOutlined,
} from "@material-ui/icons";

import { useEffect, useState } from "react";
import Bascket from "../Icons/Bascket";
import Categories from "../Icons/Categories";
import CafeDojeldi from "../Icons/CafeDojeldi";
import Support from "../Icons/Support";
import AboutUs from "../Icons/AboutUs";
import FAQ from "../Icons/FAQ";
import Universal from "../Icons/Universal";
import HelpBook from "../Icons/HelpBook";
import University from "../Icons/University";
import Child from "../Icons/Child";
import Language from "../Icons/Language";
import Search from "../Icons/Search";
import { useDispatch, useSelector } from "react-redux";
import { getProfile } from "../../../redux/profile/getProfile/actions";
import SearchHeader from "../HeaderComponents/SearchHeaderComponent";
import ProfileHeader from "../HeaderComponents/ProfileHeaderComponent";
import BookRequest from "./BookRequest";
import { BASE_URL } from "../../../services/axios";

export default function Header(props) {
  const theme = useTheme();
  const [openMenu, setOpenMenu] = useState(false);
  const [bookRequest, setBookRequest] = useState(false);
  const { data, loading } = useSelector((state) => state.category.getTree);
  const menuHandleHoverToggle = () => {
    setOpenMenu(!openMenu);
  };
  const matches = useMediaQuery(theme.breakpoints.up("lg"));

  return (
    <AppBar
      position="sticky"
      style={{ boxShadow: "0px 0px 20px rgba(0,0,0,0.1)", zIndex: 1020 }}
    >
      <Toolbar
        style={{
          backgroundColor: "white",
          flexDirection: matches ? "column" : "",
        }}
        disableGutters
      >
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          style={{ paddingLeft: "16px", paddingRight: "16px" }}
        >
          <Hidden lgUp>
            <Grid item>
              <IconButton className={styles.cart_button}>
                <Search />
              </IconButton>
            </Grid>
          </Hidden>
          <Grid item xs={matches ? 0 : 6}>
            <a
              href="/"
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Typography
                variant="h2"
                style={{ fontFamily: "mreg", color: "#202439" }}
              >
                DOJELDI
              </Typography>
              <Typography
                variant="h4"
                noWrap
                style={{
                  fontFamily: "ravi medium",
                  color: "#B2B2B2",
                  marginRight: 8,
                }}
              >
                مـرجع کـتاب ایـران
              </Typography>
            </a>
          </Grid>
          <Hidden mdDown>
            <Grid item lg={5}>
              <SearchHeader />
            </Grid>
          </Hidden>
          <Grid item>
            <ProfileHeader />
          </Grid>
        </Grid>
        <Hidden mdDown>
          <Divider style={{ width: "100%" }} />
          <Box
            style={{
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                flex: "0 0 80%",
              }}
            >
              <ClickAwayListener
                onClickAway={() => {
                  openMenu ? setOpenMenu(false) : null;
                }}
              >
                <Box>
                  <Button
                    onClick={menuHandleHoverToggle}
                    style={{
                      fontFamily: "ravi",
                      padding: 16,
                      borderBottom: openMenu
                        ? "solid 2px #FF5252"
                        : "solid 2px transparent",
                      borderRadius: 0,
                    }}
                  >
                    <Box
                      style={{
                        marginLeft: 8,
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      <Categories />
                    </Box>
                    دسته‌بندی
                  </Button>
                  <Fade in={openMenu}>
                    <Box
                      style={{
                        position: "absolute",
                        boxShadow: "0px 20px 20px rgba(0,0,0,0.1)",
                        top: "100%",
                        color: "black",
                        width: "100%",
                        maxWidth: 800,
                        backgroundColor: "white",
                        display: "flex",
                        flexDirection: "row",
                        borderRadius: "0px 0px 5px 5px",
                        borderTop: "solid 1px #ECEDEF",
                      }}
                    >
                      {data?.data.map((category) => (
                        <>
                          <Box
                            style={{
                              flex: "100%",
                              display: "flex",
                              flexDirection: "column",
                              margin: 24,
                            }}
                          >
                            <img
                              src={BASE_URL + "/image/" + category.icon}
                              style={{ width: 32 }}
                            />
                            <Link
                              href={`/Category?categories=&categories=${category.id}`}
                            >
                              <Typography
                                style={{
                                  fontFamily: "ravi bold",
                                  fontSize: 16,
                                  marginTop: 16,
                                }}
                              >
                                {category.name}
                              </Typography>
                            </Link>
                            <Box
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "space-evenly",
                                marginTop: 16,
                              }}
                            >
                              {category.children?.map((child) => (
                                <Link
                                  href={`/Category?categories=&categories=${child.id}`}
                                >
                                  <Typography
                                    style={{
                                      fontFamily: "ravi",
                                      fontSize: 12,
                                      color: "#878A9B",
                                      textAlign: "right",
                                      margin: "16px 0px",
                                    }}
                                  >
                                    {child.name}
                                  </Typography>
                                </Link>
                              ))}
                            </Box>
                          </Box>
                          <Divider
                            orientation="vertical"
                            flexItem
                            style={{ backgroundColor: "#ECEDEF" }}
                          />
                        </>
                      ))}
                    </Box>
                  </Fade>
                </Box>
              </ClickAwayListener>

              <Button
                href="https://dojeldi.org/cafedojeldi/"
                style={{
                  fontFamily: "ravi",
                  padding: 16,
                }}
              >
                <Box
                  style={{
                    marginLeft: 8,
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <CafeDojeldi />
                </Box>
                کافه دوجلدی
              </Button>
              <Link href="/ContactUs">
                <Button style={{ fontFamily: "ravi", padding: 16 }}>
                  <Box
                    style={{
                      marginLeft: 8,
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <Support />
                  </Box>
                  پشتیبانی
                </Button>
              </Link>
              <Link href="/AboutUs">
                <Button style={{ fontFamily: "ravi", padding: 16 }}>
                  <Box
                    style={{
                      marginLeft: 8,
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <AboutUs />
                  </Box>
                  درباره ما
                </Button>
              </Link>
              <Link href="/FAQ">
                <Button style={{ fontFamily: "ravi", padding: 16 }}>
                  <Box
                    style={{
                      marginLeft: 8,
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <FAQ />
                  </Box>
                  سوالات متداول
                </Button>
              </Link>
              <Button
                style={{ fontFamily: "ravi", padding: 16 }}
                onClick={() => {
                  setBookRequest(true);
                }}
              >
                <Box
                  style={{
                    marginLeft: 8,
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <AddBoxOutlined
                    style={{ color: theme.palette.primary.main }}
                  />
                </Box>
                درخواست کتاب
              </Button>
            </Box>
          </Box>
        </Hidden>
      </Toolbar>
      <BookRequest
        bookRequest={bookRequest}
        setBookRequest={(value) => setBookRequest(value)}
      />
    </AppBar>
  );
}
