import { Box, Paper, Typography } from "@material-ui/core";
import Guarantee from "../Icons/Guarantee";

export default function TermsItem(props) {
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "row",
        margin: 24,
        alignItems: "center",
      }}
    >
      <Paper
        style={{
          display: "flex",
          padding: 24,
          borderRadius: 16,
          boxShadow: "0px 5px 20px rgba(0,0,0,0.05)",
          alignItems: "center",
          width: 120,
          height: 120,
        }}
      >
        {props.icon}
      </Paper>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          justifyContent: "center",
          margin: 32,
        }}
      >
        <Typography
          variant="h3"
          style={{ fontFamily: "ravi black", color: "#202439" }}
        >
          {props.title}
        </Typography>
        <Typography
          variant="h6"
          style={{
            fontFamily: "ravi medium",
            color: "#B8BAC2",
          }}
        >
          {props.desc}
        </Typography>
      </Box>
    </Box>
  );
}
