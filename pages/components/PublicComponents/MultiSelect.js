import {
  Box,
  Checkbox,
  Divider,
  FormControlLabel,
  withStyles,
} from "@material-ui/core";
import { isArray } from "lodash";
import { useEffect, useState } from "react";
import Search from "../Icons/Search";

export default function MultiSelect(props) {
  const [text, setText] = useState(null);

  const DojeldiCheckBox = withStyles({
    label: {
      fontFamily: "ravi",
      fontSize: 14,
      color: "#878A9B",
    },
    root: {
      marginTop: 4,
      width: "100%",
    },
  })(FormControlLabel);
  return (
    <Box style={{ width: "100%" }}>
      <Box
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <input
          placeholder="جستجوی نام انتشارات"
          value={text}
          onChange={(value) => {
            setText(value.target.value);
          }}
          style={{
            fontFamily: "ravi",
            fontSize: 16,
            border: "none",
            width: "100%",
            padding: 16,
            backgroundColor: "transparent",
          }}
        />
        <Search />
      </Box>
      <Divider style={{ width: "100%" }} />
      <Box style={{ overflowY: "auto", maxHeight: 240, width: "100%" }}>
        {props?.items
          ?.filter((value) => (text ? value.name.includes(text) : true))
          .map((value) => {
            return (
              <DojeldiCheckBox
                control={
                  <Checkbox
                    value={value.id}
                    color="primary"
                    checked={
                      props.values &&
                      props.values[props.type] &&
                      props.values[props.type].length > 0 &&
                      props.values[props.type].indexOf(value.id.toString()) > -1
                        ? true
                        : false
                    }
                    name="checkone"
                    onChange={(event) => {
                      let items = props.values[props.type]
                        ? isArray(props.values[props.type])
                          ? [...props.values[props.type]]
                          : [props.values[props.type]]
                        : [""];
                      if (items && items.indexOf(event.target.value) > -1) {
                        items.splice(items.indexOf(event.target.value), 1);
                        props.setValues({ [props.type]: items });
                      } else {
                        props.setValues({
                          [props.type]: [...items, event.target.value],
                        });
                      }
                    }}
                  />
                }
                label={value.name}
              />
            );
          })}
      </Box>
    </Box>
  );
}
