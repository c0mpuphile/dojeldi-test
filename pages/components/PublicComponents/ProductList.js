import {
  Box,
  Card,
  CardMedia,
  Container,
  Divider,
  GridList,
  GridListTile,
  IconButton,
  List,
  ListItem,
  makeStyles,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { AddShoppingCart, Headset, StarRounded } from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import ProductListItem from "./ProductListItem";
import styles from "../ProductList.module.css";
import { useEffect, useState } from "react";
import instance from "../../../services/axios";
import QueryString from "qs";
import SearchAPI from "../../../services/search";
import Link from "next/link";

export default function ProductList(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [items, setItems] = useState();
  useEffect(() => {
    if (props.url) {
      SearchAPI.searchFull(
        QueryString.parse(
          props.url?.slice(props.url?.indexOf("?") + 1, props.url?.length)
        )
      )
        .then((resp) => resp.data)
        .then((resp) => {
          setItems(resp.data?.slice(0, 4));
        });
    }
  }, []);

  useEffect(() => {
    if (props.data) {
      setItems(props.data);
    }
  }, [props.data]);

  return (
    <Container maxWidth="xl" disableGutters>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Divider
          style={{ flex: "100%", display: matches ? "block" : "none" }}
        />
        <Typography
          variant="h2"
          style={{
            textAlign: "center",
            flexShrink: 0,
            margin: "0px 24px",
            fontFamily: "ravi bold",
            color: "#202439",
          }}
        >
          {props.title}
        </Typography>
        <Divider style={{ flex: "100%" }} />
        {props.url ? (
          <Link href={props.url || ""}>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi",
                textAlign: "center",
                color: "#1273eb",
                margin: "0px 24px",
                display: matches ? "none" : "block",
              }}
            >
              مشاهده همه
            </Typography>
          </Link>
        ) : null}
      </Box>
      <Box
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginTop: "8px",
          display: matches ? "flex" : "none",
        }}
      >
        {props.url ? (
          <Link href={props.url || ""}>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi",
                textAlign: "center",
                color: "#1273eb",
              }}
            >
              مشاهده همه
            </Typography>
          </Link>
        ) : null}
      </Box>
      <GridList
        style={{
          flexWrap: "nowrap",
          transform: "translateZ(0)",
          marginTop: 24,
        }}
        cellHeight={props.itemHeight}
        cols={props.showItem ? props.showItem : matches ? 4 : 1.5}
        spacing={16}
      >
        {items?.map((item) => props.component(item))}
      </GridList>
    </Container>
  );
}
