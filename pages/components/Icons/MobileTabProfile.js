import { SvgIcon } from "@material-ui/core";

export default function MobileTabProfile(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="14.214"
      height="17.2"
      viewBox="0 0 14.214 17.2"
    >
      <g id="Profile" transform="translate(0.6 0.6)">
        <ellipse
          id="Ellipse_736"
          cx="4.1"
          cy="3.977"
          rx="4.1"
          ry="3.977"
          transform="translate(2.404 0)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Path_33945"
          d="M0,2.51A1.8,1.8,0,0,1,.189,1.7,3.492,3.492,0,0,1,2.608.354,14.826,14.826,0,0,1,4.619.08a22.162,22.162,0,0,1,3.762,0,15,15,0,0,1,2.011.274A3.359,3.359,0,0,1,12.811,1.7a1.838,1.838,0,0,1,0,1.623,3.327,3.327,0,0,1-2.419,1.341,13.878,13.878,0,0,1-2.011.282,22.843,22.843,0,0,1-3.063.046,3.6,3.6,0,0,1-.7-.046,13.619,13.619,0,0,1-2-.282A3.343,3.343,0,0,1,.189,3.326,1.85,1.85,0,0,1,0,2.51Z"
          transform="translate(0 10.975)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
      </g>
    </svg>
  );
}
