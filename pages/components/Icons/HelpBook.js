import { SvgIcon } from "@material-ui/core";

export default function HelpBook(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xlink="http://www.w3.org/1999/xlink"
      width="24"
      height="24"
      viewBox="0 0 31 32"
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="1.095"
          y1="-0.045"
          x2="-0.045"
          y2="0.93"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#ffdc50" />
          <stop offset="1" stop-color="#ff711e" />
        </linearGradient>
      </defs>
      <g id="icons8_school_bell" transform="translate(-21 -11)">
        <path
          id="Path_49"
          data-name="Path 49"
          d="M49.347,86.075H37.069A3.053,3.053,0,0,1,34,83.057V74H52.416v9.057A3.053,3.053,0,0,1,49.347,86.075Z"
          transform="translate(-9.01 -43.981)"
          fill="#fff"
        />
        <path
          id="Path_51"
          data-name="Path 51"
          d="M36.277,14A12.077,12.077,0,1,0,48.554,26.075,12.178,12.178,0,0,0,36.277,14Z"
          transform="translate(-2.079 -2.094)"
          fill="url(#linear-gradient)"
        />
        <path
          id="Path_54"
          data-name="Path 54"
          d="M58.455,50.83a2.415,2.415,0,1,1,2.455-2.415A2.443,2.443,0,0,1,58.455,50.83Z"
          transform="translate(-24.257 -24.434)"
          fill="#fff"
        />
        <path
          id="Path_55"
          data-name="Path 55"
          d="M52,23.981a.921.921,0,0,0-1.842,0,15.523,15.523,0,0,1-5.832,12.106v-3.8a12.893,12.893,0,0,0,3.069-8.3,13.2,13.2,0,0,0-26.4,0,12.771,12.771,0,0,0,3.069,8.3v6.792A3.95,3.95,0,0,0,28.059,43H40.337a3.95,3.95,0,0,0,3.99-3.925v-.694A17.439,17.439,0,0,0,52,23.981Zm-29.158,0A11.358,11.358,0,1,1,34.2,35.151,11.278,11.278,0,0,1,22.842,23.981ZM42.485,39.075a2.118,2.118,0,0,1-2.149,2.113H28.059a2.118,2.118,0,0,1-2.149-2.113v-3.8a.626.626,0,0,1,.951-.513,13.311,13.311,0,0,0,14.671,0,.616.616,0,0,1,.951.513Z"
          fill="#444b54"
        />
      </g>
    </svg>
  );
}
