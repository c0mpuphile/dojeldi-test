import { SvgIcon } from "@material-ui/core";

export default function Support(props) {
  return (
    <svg
      id="Discovery"
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
    >
      <path
        id="Discovery-2"
        data-name="Discovery"
        d="M0,8a8,8,0,1,1,8,8A8.01,8.01,0,0,1,0,8ZM1.158,8A6.842,6.842,0,1,0,8,1.158,6.849,6.849,0,0,0,1.158,8Zm3.947,2.774a.544.544,0,0,1-.08-.489L6.194,6.554a.549.549,0,0,1,.36-.36l3.731-1.168A.559.559,0,0,1,10.453,5a.552.552,0,0,1,.441.226.544.544,0,0,1,.081.489L9.806,9.446a.552.552,0,0,1-.361.361L5.715,10.975A.565.565,0,0,1,5.548,11,.552.552,0,0,1,5.106,10.774ZM7.157,7.157,6.388,9.61l2.454-.768L9.61,6.388Z"
        fill="#878a9b"
      />
    </svg>
  );
}
