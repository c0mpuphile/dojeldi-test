import { SvgIcon } from "@material-ui/core";

export default function CafeDojeldi(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="14.2"
      height="17.25"
      viewBox="0 0 14.2 17.25"
    >
      <g id="Bookmark" transform="translate(0.804 -0.178)">
        <path
          id="Path_33968"
          d="M6.181,13.2,1.206,15.9A.833.833,0,0,1,.1,15.573h0a.863.863,0,0,1-.1-.389V3.2C0,.914,1.574,0,3.839,0H9.161C11.357,0,13,.853,13,3.048V15.185a.812.812,0,0,1-.241.576.825.825,0,0,1-.581.239.912.912,0,0,1-.4-.1L6.773,13.2A.626.626,0,0,0,6.181,13.2Z"
          transform="translate(-0.204 0.778)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Line_209"
          d="M0,.458H5.5"
          transform="translate(3.796 5.32)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
      </g>
    </svg>
  );
}
