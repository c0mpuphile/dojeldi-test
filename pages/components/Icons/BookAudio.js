import { SvgIcon } from "@material-ui/core";

export default function BookAudio(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={`0 0 22 20`}
    >
      <path
        id="icons8_headphones"
        d="M10,1a9.007,9.007,0,0,0-9,9v6.923A2.084,2.084,0,0,0,3.077,19H5.154V12.077H3.077a2.1,2.1,0,0,0-.692.13V10a7.615,7.615,0,0,1,15.231,0v2.207a2.1,2.1,0,0,0-.692-.13H14.846V19h2.077A2.084,2.084,0,0,0,19,16.923V10A9.007,9.007,0,0,0,10,1ZM3.077,13.462h.692v4.154H3.077a.683.683,0,0,1-.692-.692V14.154A.683.683,0,0,1,3.077,13.462Zm13.154,0h.692a.683.683,0,0,1,.692.692v2.769a.683.683,0,0,1-.692.692h-.692Z"
        fill={props.color}
      />
    </svg>
  );
}
