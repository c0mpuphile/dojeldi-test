import { SvgIcon } from "@material-ui/core";

export default function FAQ(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="17.2"
      height="17.2"
      viewBox="0 0 17.2 17.2"
    >
      <g id="Folder" transform="translate(-0.176 -0.176)">
        <path
          id="Path_33957"
          d="M15.977,11.253c0,2.987-1.757,4.747-4.74,4.747H4.748C1.757,16,0,14.24,0,11.253V4.747C0,1.76,1.1,0,4.078,0H5.744A1.9,1.9,0,0,1,7.266.762l.761,1.013a1.909,1.909,0,0,0,1.522.762h2.359C14.9,2.537,16,4.061,16,7.109Z"
          transform="translate(0.776 0.776)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Line_194"
          d="M0,.456H8"
          transform="translate(4.776 10.319)"
          fill="none"
          stroke="#878a9b"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
      </g>
    </svg>
  );
}
