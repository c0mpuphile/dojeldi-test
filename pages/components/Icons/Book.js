import { SvgIcon } from "@material-ui/core";

export default function Book(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={`0 0 20 22`}
    >
      <path
        id="icons8_book_1"
        d="M5.4,2A2.337,2.337,0,0,0,3,4.25v13.5A2.14,2.14,0,0,0,5.08,20H19V18.5H5.08a.752.752,0,1,1,0-1.5H19V2Zm0,1.5h12v12H5.4a2.566,2.566,0,0,0-.8.141V4.25A.766.766,0,0,1,5.4,3.5Zm.8,3V8h9.6V6.5Z"
        fill={props.color}
      />
    </svg>
  );
}
