import { SvgIcon } from "@material-ui/core";

export default function University(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xlink="http://www.w3.org/1999/xlink"
      width="24"
      height="24"
      viewBox="0 0 32 26"
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="0.5"
          x2="0.5"
          y2="1"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#ffdc50" />
          <stop offset="1" stop-color="#ff711e" />
        </linearGradient>
      </defs>
      <g id="icons8_student_male_2" transform="translate(-7 -26.036)">
        <path
          id="Path_57"
          data-name="Path 57"
          d="M10,35.876l15.448,6.876,14.9-6.876L25.448,29Z"
          transform="translate(-2.172 -2.149)"
          fill="url(#linear-gradient)"
        />
        <path
          id="Path_59"
          data-name="Path 59"
          d="M33.8,74.992a15.944,15.944,0,0,1-10.317-3.658A3.923,3.923,0,0,1,22.1,68.308V58.525a.828.828,0,0,1,1.655,0v9.783a2.351,2.351,0,0,0,.8,1.76,15.055,15.055,0,0,0,18.814-.028,2.164,2.164,0,0,0,.8-1.733V58.58a.828.828,0,0,1,1.655,0v9.728a3.791,3.791,0,0,1-1.434,3A16.942,16.942,0,0,1,33.8,74.992Z"
          transform="translate(-10.934 -22.955)"
          fill="#454b54"
        />
        <path
          id="Path_60"
          data-name="Path 60"
          d="M23.276,41.428a.736.736,0,0,1-.331-.083L7.5,34.47a.8.8,0,0,1,0-1.485l15.448-6.876a.86.86,0,0,1,.69,0l14.9,6.876a.81.81,0,0,1,.469.743.86.86,0,0,1-.469.743l-14.9,6.876A.852.852,0,0,1,23.276,41.428Zm-13.407-7.7L23.276,39.7l12.938-5.968L23.276,27.759Zm28.3,8.773a.812.812,0,0,1-.828-.825v-5.2a.828.828,0,0,1,1.655,0v5.2A.829.829,0,0,1,38.172,42.5Z"
          fill="#454b54"
        />
      </g>
    </svg>
  );
}
