import { SvgIcon } from "@material-ui/core";

export default function FreeSuggest(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xlink="http://www.w3.org/1999/xlink"
      width="70"
      height="70"
      viewBox="0 0 70 70"
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="1.077"
          x2="0.112"
          y2="0.95"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#ffdc50" />
          <stop offset="1" stop-color="#ff711e" />
        </linearGradient>
      </defs>
      <g id="Icon" transform="translate(-15.956 -9)">
        <path
          id="Path_38"
          data-name="Path 38"
          d="M79,22.592A13.636,13.636,0,0,1,92.6,9a13.38,13.38,0,0,1,9.18,3.6c.272.272-.476.68-.612,1.087-.068.408.476.883.68,1.155a3.835,3.835,0,0,0,1.632,1.223c.408.068.748-.476.816-.272a13.567,13.567,0,1,1-25.3,6.8Z"
          transform="translate(-20.176)"
          fill="url(#linear-gradient)"
        />
        <path
          id="Path_39"
          data-name="Path 39"
          d="M104.969,14.3,95.857,24.494l-3.264-4.214A2.033,2.033,0,0,0,89.4,22.8l4.76,6.117a2.218,2.218,0,0,0,1.564.816h.068a1.949,1.949,0,0,0,1.5-.68l10.268-11.553A14.4,14.4,0,0,0,104.969,14.3Z"
          transform="translate(-23.365 -1.698)"
          fill="#fff"
        />
        <path
          id="Path_40"
          data-name="Path 40"
          d="M48.625,81.243h-30.6a2.042,2.042,0,0,1-1.5-3.466l7.48-7.748A32.607,32.607,0,0,1,48.625,16c.884,0,1.7,0,2.584.068a2.043,2.043,0,1,1-.272,4.078c-.816-.068-1.564-.068-2.312-.068A28.543,28.543,0,0,0,28.293,68.67a2.03,2.03,0,0,1,0,2.854l-5.44,5.641H48.625A28.548,28.548,0,0,0,77.184,48.621c0-.748,0-1.5-.068-2.243a2.008,2.008,0,0,1,1.9-2.175,2.048,2.048,0,0,1,2.176,1.9c.068.816.068,1.7.068,2.515A32.639,32.639,0,0,1,48.625,81.243Z"
          transform="translate(0 -2.243)"
          fill="#444b54"
        />
        <path
          id="Path_41"
          data-name="Path 41"
          d="M69.159,56.078H46.04a2.039,2.039,0,1,1,0-4.078H69.159a2.039,2.039,0,1,1,0,4.078ZM58.96,68.311H46.04a2.039,2.039,0,1,1,0-4.078H58.96a2.039,2.039,0,1,1,0,4.078Z"
          transform="translate(-8.975 -13.777)"
          fill="#d3d8dd"
        />
        <g
          id="Group_4"
          data-name="Group 4"
          transform="translate(58.144 50.456)"
        >
          <path
            id="Path_42"
            data-name="Path 42"
            d="M80.72,74.078h-.68a2.039,2.039,0,1,1,0-4.078h.68a2.039,2.039,0,1,1,0,4.078Z"
            transform="translate(-78 -70)"
            fill="#d3d8dd"
          />
        </g>
      </g>
    </svg>
  );
}
