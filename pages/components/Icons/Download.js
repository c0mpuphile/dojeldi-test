import { SvgIcon } from "@material-ui/core";

export default function Download(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      viewBox="0 0 20 20"
    >
      <path
        id="icons8_below"
        d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,2a8,8,0,1,1-8,8A8,8,0,0,1,12,4ZM11,7v6.586L8.707,11.293,7.293,12.707,12,17.414l4.707-4.707-1.414-1.414L13,13.586V7Z"
        transform="translate(-2 -2)"
        fill="#ff5252"
      />
    </svg>
  );
}
