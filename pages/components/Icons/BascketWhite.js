export default function BascketWhite(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="17.203"
      height="17.2"
      viewBox="0 0 17.203 17.2"
    >
      <g id="Bag" transform="translate(-0.201 -0.178)">
        <path
          id="Path_33955"
          d="M11.688,12.221H4.3C1.591,12.221-.49,11.3.1,7.589L.789,2.56A2.817,2.817,0,0,1,3.51,0h9a2.9,2.9,0,0,1,2.721,2.56l.688,5.029C16.425,10.88,14.4,12.221,11.688,12.221Z"
          transform="translate(0.801 4.557)"
          fill="none"
          stroke="#fff"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Path_33956"
          d="M7.66,3.6A3.714,3.714,0,0,0,3.838,0h0A3.949,3.949,0,0,0,1.125,1.048,3.492,3.492,0,0,0,0,3.6H0"
          transform="translate(4.951 0.778)"
          fill="none"
          stroke="#fff"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Line_192"
          d="M.475.458H.435"
          transform="translate(10.937 7.665)"
          fill="none"
          stroke="#fff"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
        <path
          id="Line_193"
          d="M.475.458H.435"
          transform="translate(5.779 7.665)"
          fill="none"
          stroke="#fff"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.2"
        />
      </g>
    </svg>
  );
}
