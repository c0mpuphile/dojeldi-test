import { SvgIcon } from "@material-ui/core";

export default function Comment(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="21.094"
      viewBox="0 0 20 21.094"
    >
      <path
        id="Reply"
        d="M12,2C6.549,2,2,5.974,2,11s4.549,9,10,9c.346,0,.665-.069,1-.1v3.2l1.541-.988c2.162-1.388,6.844-4.99,7.4-10.16A8.255,8.255,0,0,0,22,11C22,5.974,17.451,2,12,2Zm0,2c4.491,0,8,3.19,8,7a6.254,6.254,0,0,1-.043.723v0l0,0c-.346,3.2-2.908,5.714-4.955,7.353V17.572l-1.2.242A9.1,9.1,0,0,1,12,18c-4.491,0-8-3.19-8-7S7.509,4,12,4Z"
        transform="translate(-2 -2)"
        fill="#b7b9c3"
      />
    </svg>
  );
}
