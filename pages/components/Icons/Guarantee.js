import { SvgIcon } from "@material-ui/core";

export default function Guarantee(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xlink="http://www.w3.org/1999/xlink"
      width="66"
      height="72"
      viewBox="0 0 66 72"
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="1.108"
          x2="0"
          y2="1.192"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#ffdc50" />
          <stop offset="1" stop-color="#ff711e" />
        </linearGradient>
      </defs>
      <g id="Icon" transform="translate(-19 -6)">
        <path
          id="Path_26"
          data-name="Path 26"
          d="M67.991,48.345H40.85a1.862,1.862,0,0,1,0-3.724h27.14a1.862,1.862,0,0,1,0,3.724ZM61.822,29.724h-14.8a1.862,1.862,0,0,1,0-3.724h14.8a1.862,1.862,0,0,1,0,3.724Zm6.168,27.931H40.85a1.862,1.862,0,0,1,0-3.724h27.14a1.862,1.862,0,0,1,0,3.724Zm-9.869,9.31H40.85a1.862,1.862,0,0,1,0-3.724H58.121a1.862,1.862,0,0,1,0,3.724Z"
          transform="translate(-7.664 -7.586)"
          fill="#e9eef4"
        />
        <path
          id="Path_27"
          data-name="Path 27"
          d="M98.336,82a12.414,12.414,0,1,0,12.336,12.414A12.375,12.375,0,0,0,98.336,82Z"
          transform="translate(-25.673 -28.828)"
          fill="url(#linear-gradient)"
        />
        <path
          id="Path_28"
          data-name="Path 28"
          d="M111.1,86.3l-8.882,9.931-2.961-3.848a1.846,1.846,0,0,0-2.591-.31,1.874,1.874,0,0,0-.308,2.607l4.318,5.586a2.008,2.008,0,0,0,1.419.745h.062a1.763,1.763,0,0,0,1.357-.621l9.931-11.172A13.139,13.139,0,0,0,111.1,86.3Z"
          transform="translate(-29.492 -30.459)"
          fill="#fff"
        />
        <g id="Group_3" data-name="Group 3" transform="translate(19 6)">
          <path
            id="Path_29"
            data-name="Path 29"
            d="M27.019,77.551H58.477a1.85,1.85,0,1,0,0-3.7H27.019A4.291,4.291,0,0,1,22.7,69.533V14.019A4.291,4.291,0,0,1,27.019,9.7H66.5A4.245,4.245,0,0,1,69.579,11a4.335,4.335,0,0,1,1.234,3.084l-.062,32.692a1.817,1.817,0,0,0,1.85,1.85h0a1.817,1.817,0,0,0,1.85-1.85l.062-32.692A8.109,8.109,0,0,0,66.5,6H27.019A8,8,0,0,0,19,14.019V69.533A8,8,0,0,0,27.019,77.551Z"
            transform="translate(-19 -6)"
            fill="#444b54"
          />
        </g>
      </g>
    </svg>
  );
}
