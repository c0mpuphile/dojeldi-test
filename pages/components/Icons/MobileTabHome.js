import { SvgIcon } from "@material-ui/core";

export default function MobileTabHome(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="17.2"
      height="17.292"
      viewBox="0 0 17.2 17.292"
    >
      <path
        id="Home"
        d="M5.606,15.017V12.564a1.172,1.172,0,0,1,1.2-1.135h2.43a1.173,1.173,0,0,1,1.207,1.135h0v2.461A1.006,1.006,0,0,0,11.455,16h1.62A2.842,2.842,0,0,0,16,13.249h0V6.27a1.921,1.921,0,0,0-.81-1.524L9.649.548a2.79,2.79,0,0,0-3.322,0L.81,4.754A1.906,1.906,0,0,0,0,6.278v6.972A2.842,2.842,0,0,0,2.925,16h1.62a1.015,1.015,0,0,0,1.045-.983h0"
        transform="translate(0.6 0.692)"
        fill="none"
        stroke="#202439"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-miterlimit="10"
        stroke-width="1.2"
      />
    </svg>
  );
}
