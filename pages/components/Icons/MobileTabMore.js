import { SvgIcon } from "@material-ui/core";

export default function MobileTabMore(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="15"
      height="3"
      viewBox="0 0 15 3"
    >
      <path
        id="More"
        d="M4.5,7A1.5,1.5,0,1,0,6,8.5,1.5,1.5,0,0,0,4.5,7Zm6,0A1.5,1.5,0,1,0,12,8.5,1.5,1.5,0,0,0,10.5,7Zm6,0A1.5,1.5,0,1,0,18,8.5,1.5,1.5,0,0,0,16.5,7Z"
        transform="translate(18 10) rotate(180)"
        fill="#878a9b"
      />
    </svg>
  );
}
