import { SvgIcon } from "@material-ui/core";

export default function BookElec(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox={`0 0 22 22`}
    >
      <path
        id="icons8_e-learning"
        d="M4.572,2A2.207,2.207,0,0,0,2.428,4.25V14.545l-1.195,2.18-.011.018A2.274,2.274,0,0,0,3.143,20h15.72a2.274,2.274,0,0,0,1.92-3.258l-.017-.018-1.189-2.18V4.25A2.207,2.207,0,0,0,17.434,2Zm0,1.5H17.434a.723.723,0,0,1,.715.75V14H3.857V4.25A.724.724,0,0,1,4.572,3.5ZM6.715,6.286v1.5H15.29v-1.5Zm0,3v1.5h5.717v-1.5ZM3.556,15.5H18.45L19.5,17.422a.727.727,0,0,1-.642,1.078H3.143a.723.723,0,0,1-.636-1.078v-.006Z"
        fill={props.color}
      />
    </svg>
  );
}
