import { SvgIcon } from "@material-ui/core";

export default function Search(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="23.811"
      height="23.811"
      viewBox="0 0 23.811 23.811"
    >
      <g id="Search" transform="translate(1.061 0.75)">
        <ellipse
          id="Ellipse_739"
          cx="10.539"
          cy="10.288"
          rx="10.539"
          ry="10.288"
          transform="translate(0.923)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
        <path
          id="Line_181"
          d="M4.132,0,0,4.023"
          transform="translate(0 17.977)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
      </g>
    </svg>
  );
}
