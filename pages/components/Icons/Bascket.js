import { SvgIcon } from "@material-ui/core";

export default function Bascket(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="23.504"
      height="23.5"
      viewBox="0 0 23.504 23.5"
    >
      <g id="Bag" transform="translate(-0.052 -0.028)">
        <path
          id="Path_33955"
          d="M16.071,16.8H5.917c-3.73,0-6.591-1.268-5.778-6.37L1.085,3.52C1.586.974,3.312,0,4.826,0H17.207c1.536,0,3.162,1.048,3.741,3.52l.946,6.914C22.585,14.96,19.8,16.8,16.071,16.8Z"
          transform="translate(0.801 5.974)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
        <path
          id="Path_33956"
          d="M10.533,4.945A5.107,5.107,0,0,0,5.277,0h0a5.429,5.429,0,0,0-3.73,1.441A4.8,4.8,0,0,0,0,4.945H0"
          transform="translate(6.507 0.778)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
        <path
          id="Line_192"
          d="M.49.458H.435"
          transform="translate(14.901 10.419)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
        <path
          id="Line_193"
          d="M.49.458H.435"
          transform="translate(7.809 10.419)"
          fill="none"
          stroke="#200e32"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-miterlimit="10"
          stroke-width="1.5"
        />
      </g>
    </svg>
  );
}
