import {
  Box,
  Divider,
  Hidden,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { AssignmentReturned } from "@material-ui/icons";
import { BASE_URL } from "../../../services/axios";
import Book from "../Icons/Book";
import BookAudio from "../Icons/BookAudio";
import BookElec from "../Icons/BookElec";

export default function AnonCartComponentItem(props) {
  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        borderRadius: 20,
        marginTop: 20,
        height: "auto",
        width: "100%",
      }}
    >
      <Box style={{ display: "flex", flexDirection: "row" }}>
        <Box
          style={{
            maxWidth: matches ? 64 : 24,
            margin: matches ? 16 : 8,
          }}
        >
          <img
            src={BASE_URL + "/image/" + props.data?.image}
            onError={(event) => {
              event.target.src = "/missingbook.png";
            }}
            style={{ borderRadius: 10, width: "100%" }}
          />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            margin: matches ? 16 : 8,
            marginRight: 0,
            flex: "1 0 0%",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "center",
            }}
          >
            {props.data?.type === "PRINT" ? (
              <>
                <Book width={16} height={16} color={"#1273EB"} />
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه چاپی
                </Typography>
              </>
            ) : props.data?.type === "AUDIO" ? (
              <>
                <BookAudio width={16} height={16} color={"#FF9A27"} />
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه صوتی
                </Typography>
              </>
            ) : (
              <>
                <BookElec width={16} height={16} color={"#FF5252"} />
                <Typography
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه الکترونیک
                </Typography>
              </>
            )}
          </Box>
          <Box style={{ marginTop: 8 }}>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: matches ? 14 : 8,
                color: "#202439",
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Hidden mdDown>
            <Box
              style={{
                marginTop: 12,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "75%",
                  justifyContent: "space-between",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#1273EB",
                  }}
                >
                  {props.data?.authors.map((author) => author.name).join(" و ")}
                </Typography>
              </Box>
            </Box>
          </Hidden>
          <Hidden lgUp>
            <Typography
              style={{
                fontSize: 8,
                fontFamily: "ravi medium",
                color: "#1273EB",
                marginTop: 8,
              }}
            >
              {props.data?.authors.map((author) => author.name).join(" و ")}
            </Typography>
          </Hidden>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            margin: 16,
            flex: "0 1 0%",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: matches ? 18 : 12,
                color: "black",
                margin: "0px 2px",
              }}
            >
              {props.data?.sale_price}
            </Typography>
            <Typography
              variant="h6"
              style={{
                fontFamily: "ravi medium",
                margin: "0px 2px",
                color: "#878A9B",
              }}
            >
              تومان
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
