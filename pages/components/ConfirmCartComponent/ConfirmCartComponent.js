import {
  Box,
  Button,
  Divider,
  GridList,
  Hidden,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import ConfirmCartComponentItem from "./ConfirmCartCompoentItem";

export default function ConfirmCartComponent(props) {
  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
      }}
    >
      {props.data?.map((item) => (
        <ConfirmCartComponentItem data={item} />
      ))}
    </Box>
  );
}
