import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  Paper,
  Radio,
  Typography,
} from "@material-ui/core";

export default function AddressItem(props) {
  return (
    <Paper
      style={{
        borderRadius: 20,
        marginTop: 20,
        height: "auto",
        width: "100%",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        padding: 0,
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 24,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <FormControlLabel
            control={
              <Radio
                onClick={() => props.setAddress(props.data?.id)}
                name="Address"
                checked={props.checked}
                style={{ color: "#FF5252" }}
              />
            }
          />
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 18,
              color: "#202439",
            }}
          >
            {props.data?.recipient}
          </Typography>
        </Box>
        <Button
          onClick={() => {
            props.editAddress();
          }}
          style={{
            fontFamily: "ravi medium",
            fontSize: 14,
            color: "#1273EB",
            border: "1px solid #1273EB",
            borderRadius: 10,
          }}
        >
          ویرایش آدرس
        </Button>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          گیرنده
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.recipient}
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          استان و شهر
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.city_province}
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          آدرس
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.postal_address}
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          کد پستی
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.postal_code}
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "rgba(32,36,51,0.03)",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          شماره ثابت
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.phone}
        </Typography>
      </Box>
      <Box
        style={{
          padding: "16px 72px",
          display: "flex",
          flexDirection: "row",
          backgroundColor: "transparent",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi Bold",
            fontSize: 14,
            color: "#202439",
          }}
        >
          شماره همراه
        </Typography>
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 14,
            color: "#878A9B",
            marginRight: 32,
          }}
        >
          {props.data?.mobile}
        </Typography>
      </Box>
    </Paper>
  );
}
