import {
  Box,
  Divider,
  Hidden,
  Link,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { AssignmentReturned } from "@material-ui/icons";
import Book from "../Icons/Book";
import BookAudio from "../Icons/BookAudio";
import BookElec from "../Icons/BookElec";

export default function ConfirmCartComponentItem(props) {
  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  return (
    <Paper
      style={{
        borderRadius: 20,
        marginTop: 20,
        height: "auto",
        width: "100%",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
      }}
    >
      <Box style={{ display: "flex", flexDirection: "row" }}>
        <Box
          style={{
            maxWidth: matches ? 128 : 50,
            margin: matches ? 32 : 16,
          }}
        >
          <img src="/book1.jpg" style={{ borderRadius: 10, width: "100%" }} />
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            margin: matches ? 32 : 16,
            marginRight: 0,
            flex: "1 0 0%",
          }}
        >
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "center",
            }}
          >
            {props.data?.type === "PRINT" ? (
              <>
                <Book width={16} height={16} color={"#1273EB"} />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه چاپی
                </Typography>
              </>
            ) : props.data?.type === "AUDIO" ? (
              <>
                <BookAudio width={16} height={16} color={"#FF9A27"} />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه صوتی
                </Typography>
              </>
            ) : (
              <>
                <BookElec width={16} height={16} color={"#FF5252"} />
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#B7B9C3",

                    margin: "0px 4px",
                  }}
                >
                  نسخه الکترونیک
                </Typography>
              </>
            )}
          </Box>
          <Box style={{ marginTop: 8 }}>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: matches ? 18 : 10,
                color: "#202439",
              }}
            >
              {props.data?.persian_title}
            </Typography>
          </Box>
          <Hidden mdDown>
            <Box
              style={{
                marginTop: 12,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "75%",
                  justifyContent: "space-between",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#878A9B",
                  }}
                >
                  نویسنده
                </Typography>

                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#1273EB",
                  }}
                >
                  {props.data?.authors.map((author) => author.name).join(" و ")}
                </Typography>
              </Box>

              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "75%",
                  justifyContent: "space-between",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#878A9B",
                  }}
                >
                  مترجم
                </Typography>
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#1273EB",
                  }}
                >
                  {props.data?.translators
                    .map((translator) => translator.name)
                    .join(" و ")}
                </Typography>
              </Box>

              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "75%",
                  justifyContent: "space-between",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#878A9B",
                  }}
                >
                  انتشارات
                </Typography>
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#1273EB",
                  }}
                >
                  {props.data?.publisher?.name}
                </Typography>
              </Box>

              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  width: "75%",
                  justifyContent: "space-between",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#878A9B",
                  }}
                >
                  دسته‌بندی
                </Typography>
                <Typography
                  style={{
                    fontFamily: "ravi",
                    fontSize: 12,
                    color: "#1273EB",
                  }}
                >
                  {props.data?.categories
                    .map((category) => category.name)
                    .join(" و ")}
                </Typography>
              </Box>
            </Box>
          </Hidden>
          <Hidden lgUp>
            <Typography
              style={{
                fontSize: 10,
                fontFamily: "ravi medium",
                color: "#1273EB",
                marginTop: 8,
              }}
            >
              {props.data?.authors.map((author) => author.name).join(" و ")}
            </Typography>
          </Hidden>
        </Box>
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            margin: 32,
            flex: "1 0 0%",
          }}
        >
          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Typography
              variant="h4"
              style={{
                fontFamily: "ravi light",
                textDecorationLine: "line-through",
                color: "#878A9B",
                margin: "0px 2px",
              }}
            >
              {props.data?.price}
            </Typography>
            {props.data?.discount ? (
              <Typography
                variant="h4"
                style={{
                  backgroundColor: "#FF5252",
                  borderRadius: 5,
                  color: "white",
                  fontFamily: "ravi",
                  padding: "0px 4px",
                  margin: "0px 2px",
                }}
              >
                ۲۵٪
              </Typography>
            ) : null}
          </Box>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: matches ? 21 : 16,

                margin: "0px 2px",
              }}
            >
              {props.data?.sale_price}
            </Typography>
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi medium",
                margin: "0px 2px",
                color: "#878A9B",
              }}
            >
              تومان
            </Typography>
          </Box>
        </Box>
      </Box>
      {props.data?.type !== "PRINT" ? (
        <>
          <Divider style={{ width: "100%" }} />
          <Box
            style={{
              margin: "16px 32px",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Typography
              variant="h6"
              style={{ fontFamily: "ravi", color: "#878A9B" }}
            >
              این محصول پس از پرداخت، به صورت دیجیتال{" "}
              {props.data?.type === "AUDIO" ? "کتاب صوتی" : "کتاب الکترونیک"} در
              کتابخانه شما قرار می‌گیرد
            </Typography>
            <Link variant="h6" style={{ fontFamily: "ravi", color: "#1273EB" }}>
              اطلاعات بیشتر
            </Link>
          </Box>
        </>
      ) : null}
    </Paper>
  );
}
