import {
  Box,
  Button,
  Checkbox,
  CircularProgress,
  Divider,
  FormControlLabel,
  FormGroup,
  GridList,
  GridListTile,
  Link,
  Paper,
  Typography,
} from "@material-ui/core";
import { AddCircleOutlineOutlined, Label } from "@material-ui/icons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAddress } from "../../../redux/profile/getAddress/actions";
import AddressItem from "./AddressItem";

export default function SetAddressComponent(props) {
  const {
    error: errorAddress,
    ok: okAddress,
    data: dataAddress,
    isLoading: loadingAddress,
  } = useSelector((state) => state.profile.getAddress);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAddress());
  }, []);
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "100%",
      }}
    >
      {!loadingAddress ? (
        <>
          {dataAddress?.data?.map((address) => (
            <AddressItem
              checked={props.address == address.id}
              setAddress={(id) => props.setAddress(id)}
              data={address}
            />
          ))}
        </>
      ) : (
        <CircularProgress color="primary" style={{ marginTop: 16 }} />
      )}
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          marginTop: 16,
        }}
      >
        <AddCircleOutlineOutlined style={{ color: "#1273EB", fontSize: 16 }} />
        <Link
          style={{
            fontFamily: "ravi medium",
            color: "#1273EB",
            fontSize: 14,
            textAlign: "center",
            marginRight: 4,
          }}
        >
          افـزودن آدرس جـدید
        </Link>
      </Box>
    </Box>
  );
}
