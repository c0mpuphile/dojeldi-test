import { Box, Button, IconButton, Paper, Typography } from "@material-ui/core";
import {
  CancelOutlined,
  CheckCircleOutline,
  FileCopy,
} from "@material-ui/icons";
import Router from "next/router";

export default function Success(props) {
  return (
    <Paper
      style={{
        padding: 40,
        marginTop: 60,
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        borderRadius: 20,
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          marginTop: 20,
          width: "100%",
          justifyContent: "center",
        }}
      >
        {props.status === "success" ? (
          <CheckCircleOutline style={{ fontSize: 96, color: "#2DB67D" }} />
        ) : (
          <CancelOutlined style={{ fontSize: 96, color: "red" }} />
        )}
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          margin: "16px 8px",
          justifyContent: "center",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi black",
            fontSize: 24,
            color: "#202439",
          }}
        >
          {props.status === "success"
            ? "پرداخت موفقیت آمیز بود"
            : "مشکلی در پرداخت به وجود آمده"}
        </Typography>
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          margin: "0px 8px",
          justifyContent: "center",
        }}
      >
        <Typography
          style={{
            fontFamily: "ravi",
            fontSize: 12,
            color: "#878A9B",
          }}
        >
          {props.status === "success"
            ? "پرداخت موفقیت آمیز بود کتاب ها برای شما ارسال می شوند یا در صورت الکترونیکی یا صوتی بودن کتاب ها در کتابخانه شما قرار میگیرند"
            : ""}
        </Typography>
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          width: "100%",
          margin: "0px 8px",
          justifyContent: "space-between",
          backgroundColor: "#F3F3F5",
          borderRadius: 10,
          padding: 16,
          marginTop: 16,
        }}
      >
        <Box style={{ visibility: "hidden" }}>asdas</Box>
        <Typography
          style={{
            fontFamily: "ravi bold",
            fontSize: 16,
            color: "#202439",
          }}
        >
          کد پیگیری سفارش {props.reference}
        </Typography>
        <IconButton>
          <FileCopy style={{ color: "#202439" }} />
        </IconButton>
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          alignItems: "center",
          marginTop: 32,
        }}
      >
        <Button
          onClick={() => Router.push("/Profile")}
          style={{
            width: "100%",
            backgroundColor: "#FF5252",
            padding: 16,
            borderRadius: 16,
            margin: "0px 8px",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 16,
              color: "white",
            }}
          >
            مشاهده وضعیت سفارش
          </Typography>
        </Button>
        <Button
          onClick={() => Router.push("/")}
          style={{
            width: "100%",
            padding: 16,
            borderRadius: 16,
            margin: "0px 8px",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi medium",
              fontSize: 16,
              color: "#878A9B",
            }}
          >
            بازگشت به صفحه اصلی
          </Typography>
        </Button>
      </Box>
    </Paper>
  );
}
