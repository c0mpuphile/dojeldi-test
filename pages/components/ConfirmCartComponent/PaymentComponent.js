import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControlLabel,
  FormGroup,
  GridList,
  GridListTile,
  Hidden,
  Icon,
  Link,
  Paper,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import { AddCircleOutlineOutlined, Check, Label } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCartOnline } from "../../../redux/cart/getCartOnline/actions";
import { calculateCoupon } from "../../../redux/invoice/calculateCoupon/actions";
import { chargeWalletByCode } from "../../../redux/invoice/chargeWalletByCode/actions";
import { getBalance } from "../../../redux/invoice/getBalance/actions";

export default function PaymentComponent(props) {
  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [gift, setGift] = useState();
  const {
    error: errorCWBC,
    ok: okCWBC,
    data: dataCWBC,
    isLoading: loadingCWBC,
  } = useSelector((state) => state.invoice.chargeWalletByCode);
  const {
    error: errorCoupon,
    ok: okCoupon,
    data: dataCoupon,
    isLoading: loadingCoupon,
  } = useSelector((state) => state.invoice.calculateCoupon);

  useEffect(() => {
    if (okCWBC) dispatch(getBalance());
  }, [okCWBC]);
  const dispatch = useDispatch();
  return (
    <Paper
      style={{
        borderRadius: 20,
        marginTop: 20,
        height: "auto",
        width: "100%",
        boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
        padding: 0,
      }}
    >
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          padding: 24,
          alignItems: "center",
        }}
      >
        <Box>
          <Checkbox
            checked={!props.useWallet}
            onClick={() => props.setUseWallet(!props.useWallet)}
            name="Address"
            style={{ color: "#FF5252" }}
          />
        </Box>
        <Box>
          <Typography
            variant="h4"
            style={{
              fontFamily: "ravi",
              color: matches ? "#878A9B" : "#202439",
            }}
          >
            پرداخت اینترنتی (از طریق درگاه شتاب)
          </Typography>
        </Box>
      </Box>
      <Divider style={{ width: "100%", color: "#ECEDEF" }} />
      <Box
        style={{
          display: "flex",
          flexDirection: matches ? "row" : "column",
          padding: matches ? 24 : 0,
          alignItems: "center",
          justifyContent: "space-around",
        }}
      >
        <Box
          style={{
            padding: !matches ? 16 : 0,
            width: "100%",
            margin: "0px 8px",
          }}
        >
          <TextField
            value={props.coupon}
            onChange={(event) => props.setCoupon(event.target.value)}
            style={{ width: "100%" }}
            label="کد تخفیف"
            variant="outlined"
            InputProps={{
              style: {
                fontFamily: "ravi medium",
                borderRadius: 8,

                fontSize: matches ? 16 : 12,
              },
              endAdornment: (
                <Button
                  disabled={loadingCoupon}
                  onClick={() => {
                    dispatch(
                      calculateCoupon({ id: props.coupon, price: props.price })
                    );
                  }}
                >
                  <Typography
                    variant="h4"
                    style={{ fontFamily: "ravi medium", color: "#878A9B" }}
                  >
                    ثبت
                  </Typography>
                </Button>
              ),
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: matches ? 16 : 12,
              },
            }}
          />
          {okCoupon ? (
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 12,
                }}
              >
                <Check
                  style={{ color: "#2DB67D", fontSize: 18, marginLeft: 4 }}
                />
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    color: "#2DB67D",
                  }}
                >
                  کد تخفیف با موفقیت اعمال شد
                </Typography>
              </Box>
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    color: "#FF5252",
                  }}
                >
                  {dataCoupon?.discount}-
                </Typography>
                {/* <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    backgroundColor: "#FF5252",
                    color: "white",
                    padding: "4px 8px",
                    borderRadius: 8,
                    marginRight: 4,
                  }}
                >
                  ۲۵٪
                </Typography> */}
              </Box>
            </Box>
          ) : null}
        </Box>
        <Hidden lgUp>
          <Divider
            orientation="horizontal"
            style={{ width: "100%", margin: "8px 0px" }}
          />
        </Hidden>
        <Box
          style={{
            padding: !matches ? 16 : 0,
            width: "100%",
            margin: "0px 8px",
          }}
        >
          <TextField
            style={{
              width: "100%",
              margin: "0px 8px",
            }}
            value={gift}
            onChange={(event) => setGift(event.target.value)}
            label="کارت هدیه"
            variant="outlined"
            InputProps={{
              style: {
                fontFamily: "ravi medium",
                borderRadius: 8,
                fontSize: matches ? 16 : 12,
              },
              endAdornment: (
                <Button
                  onClick={() => {
                    dispatch(chargeWalletByCode({ code: gift }));
                  }}
                >
                  <Typography
                    variant="h4"
                    style={{ fontFamily: "ravi medium", color: "#878A9B" }}
                  >
                    ثبت
                  </Typography>
                </Button>
              ),
            }}
            InputLabelProps={{
              style: {
                fontFamily: "ravi medium",
                fontSize: matches ? 16 : 12,
              },
            }}
          />
          {okCWBC ? (
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 12,
                }}
              >
                <Check
                  style={{ color: "#2DB67D", fontSize: 18, marginLeft: 4 }}
                />
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    color: "#2DB67D",
                  }}
                >
                  {dataCWBC?.message}
                </Typography>
              </Box>
              {/* <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 12,
                }}
              >
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    color: "#FF5252",
                  }}
                >
                  ۴۷,۰۰۰-
                </Typography>
                <Typography
                  style={{
                    fontFamily: "ravi medium",
                    fontSize: 14,
                    backgroundColor: "#FF5252",
                    color: "white",
                    padding: "4px 8px",
                    borderRadius: 8,
                    marginRight: 4,
                  }}
                >
                  ۲۵٪
                </Typography>
              </Box> */}
            </Box>
          ) : null}
        </Box>
      </Box>
    </Paper>
  );
}
