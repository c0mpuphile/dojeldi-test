import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  SvgIcon,
  Typography,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  InfoOutlined,
  Instagram,
  Search,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Pagination, PaginationItem, Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";

export default function AboutUs(props) {
  return (
    <Container maxWidth="xl" disableGutters>
      <Head>صفحه مورد نظر پیدا نشد</Head>

      <Grid
        container
        style={{ alignItems: "center", justifyContent: "center" }}
      >
        <Grid
          item
          md={6}
          style={{
            padding: 16,
          }}
        >
          <Box style={{ marginTop: 64 }}>
            <Typography
              variant="h3"
              style={{
                fontFamily: "ravi black",
                color: "#202439",
              }}
            >
              درباره ما
            </Typography>
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi",
                color: "#202439",
                marginTop: 16,
                lineHeight: 2,
              }}
            >
              مجموعه دوجلـــدی انواع کتاب را به درخواست مشتریان گرانقــــدر
              برایشان تهیه و ارسال میکند، دو جلدی همان دوبال کتاب است؛ که یک بال
              آن علم و ایده است و بال دیگرش عملکرد این مجموعه که با نام کتاب
              متاب در سال ۱۳۸۶ فعالیت خود را به صورت فروش تلفنی آغاز کرده و
              درسال ۱۳۹۴ با راه اندازی سایت و صفحات فضای مجازی نام خود را به دو
              جلدی تغییرداد؛ با افتخار بعد از گذشت حدود سه سال خدمت به مشتریان
              خود، درحال حاضر این مجموعه افتخار خدمت به بیش از صد هزار مشتری
              عزیز در سراسر کشور را دارد. دوجلدی همواره در تکاپوی رشد و بروز
              رسانی خود بوده و با توجه به مسائل مربوط به کاغذ قدم در راه کتاب
              الکترونیک و صوتی نهاده است
            </Typography>
          </Box>
          <Box style={{ marginTop: 64 }}>
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi black",
                color: "#202439",
              }}
            >
              چرا کتاب الکترونیک
            </Typography>
            <Typography
              variant="h5"
              style={{
                fontFamily: "ravi",
                color: "#202439",
                marginTop: 16,
                lineHeight: 2,
              }}
            >
              آیا می دانستید برای تولید یک تن کاغذ باید 17 درخت تنومند قطع شود؟
              در حالی که در شرایط مصاعد حداقل 20 سال طول میکشد آن درخت به آن
              میزان از رشد خود برسد. و باتوجه به مصرف ایرانیان (با اینکه سطح
              مطالعه پایینی داریم) یعنی چیزی حدود 910 میلیون کیلو گرم کاغذ، باید
              سالانه بیش از 55 هزار درخت تنومند قطع شود. و طبق خبر خبرگذاری
              ایسنا این سرعت تخریب جنگل باعث میشود تا 50 سال آینده جنگلی در کشور
              نداشته باشیم. البته کاغذ یکی از عوامل آسیب زننده به جنگل است، و
              یکی از راه های پیشگیری از این آسیب زیست محیطی ، فرهنگ سازی استفاده
              مردم از کتابهای الکترونیک، اپیکیشنها، و کتابهای صوتی میباشد. جالب
              تر اینکه کتاب های چاپی ، برای ناشرین و مصرف کنندگان نیز دردسر هایی
              دارد که به سمع و نظر شما می رسد.
            </Typography>
          </Box>
          <Box style={{ marginTop: 64 }}>
            <Typography
              variant="h3"
              style={{
                fontFamily: "ravi black",
                color: "#202439",
              }}
            >
              مشکلات کتاب چاپی برای ناشرین محترم
            </Typography>
            <Box style={{ marginTop: 16 }}>
              <ul>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  رفت و آمد دردسر دار برای تهیه کتاب
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  عدم موجود بودن کتاب در همه کتاب فروشیها و گاهی چاپ تمام بودن
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  حمل و نقل و جاگیری کتاب ها و سنگینی آن برای رفتن به مدرسه و
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  انبار شدن کتاب در منزل و جاگیری آن
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  کهنه شدن،پارگی ، خط خوردگی و
                </li>
              </ul>
            </Box>
          </Box>
          <Box style={{ marginTop: 64 }}>
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 21,
                color: "#202439",
              }}
            >
              مشکلات کتاب چاپی برای ناشرین محترم
            </Typography>
            <Box style={{ marginTop: 16 }}>
              <ul>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  رفت و آمد دردسر دار برای تهیه کتاب
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  عدم موجود بودن کتاب در همه کتاب فروشیها و گاهی چاپ تمام بودن
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  حمل و نقل و جاگیری کتاب ها و سنگینی آن برای رفتن به مدرسه و
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  انبار شدن کتاب در منزل و جاگیری آن
                </li>
                <li
                  style={{ fontFamily: "ravi", fontSize: 16, color: "#202439" }}
                >
                  کهنه شدن،پارگی ، خط خوردگی و
                </li>
              </ul>
            </Box>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 16,
                color: "#202439",
                marginTop: 64,
                lineHeight: 2,
              }}
            >
              برخی ناشرین بر این تفکر هستند که با فروش کتاب چاپی سود بیشتری
              عائدشان میشود،در صورتی که با یک حساب سر انگشتی میتوان دید که سود
              فروش برای ناشرین در کتاب الکترونیک بیشتر خواهد بود: هزینه تولید
              کتاب چاپی حدود 25 تا 30 % قیمت کتاب را شامل میشود،حال اگر ما برای
              قیمت گذاری در کتاب الکترونیک 30% مبلغ را کم کنیم ناشر میتواند
              هزینه هایی مثل انبارداری ، جابجایی و ... را در جیبش بگذارد. و
              مشتری نیز راغب تر میشود زیرا هم هزینه حمل و نقل ندارد، هم کتاب در
              دسترس تر است و هم قیمت مناسب تری دارد. پس فروش کتاب قطعا بیشتر
              خواهد شد.
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi",
                fontSize: 16,
                color: "#202439",
                marginTop: 8,
                lineHeight: 2,
              }}
            >
              به امید پیشرفت روز افزون در فرهنگ کشورمان
            </Typography>
          </Box>
        </Grid>
      </Grid>
      <Footer />
    </Container>
  );
}
