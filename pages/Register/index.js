import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  Snackbar,
  SvgIcon,
  Tab,
  Tabs,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  InfoOutlined,
  Instagram,
  Search,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Alert, Pagination, PaginationItem, Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { register } from "../../redux/auth/register/actions";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Router from "next/router";
import UserRegister from "../components/RegisterComponents/UserRegisterComponent";
import PublisherRegister from "../components/RegisterComponents/PublisherRegisterComponent";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function Register(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [tab, setTab] = React.useState(0);
  const dispatch = useDispatch();
  const { isLoading, data, error, ok } = useSelector(
    (state) => state.auth.register
  );
  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  const [showMessage, setShowMessage] = useState(false);
  useEffect(() => {
    if (error) setShowMessage(true);
  }, [error]);
  useEffect(() => {
    if (ok) Router.push("/");
  }, [ok]);

  return (
    <Container maxWidth="xl" disableGutters>
      <Head>صفحه مورد نظر پیدا نشد</Head>
      <Snackbar
        onClose={() => {
          setShowMessage(false);
        }}
        anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
        open={showMessage}
        autoHideDuration={5000}
      >
        <Alert
          severity="error"
          onClose={() => {
            setShowMessage(false);
          }}
        >
          {error?.response?.data?.message}
        </Alert>
      </Snackbar>

      <Box
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginTop: 32,
          height: "100%",
        }}
      >
        <img
          src="/Illustration.png"
          style={{ display: matches ? "block" : "none" }}
        />
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          margin: matches ? "0px 96px" : "0px 16px",
        }}
      >
        <Box style={{ height: "100%", display: matches ? "block" : "none" }}>
          <img src="Library-rafiki (1)-right.png" />
        </Box>
        <Box style={{ height: "100%", display: matches ? "block" : "none" }}>
          <img src="Library-rafiki (1)-left.png" />
        </Box>
        <Box style={{ position: matches ? "absolute" : "relative" }}>
          <Card
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              minWidth: matches ? 480 : null,
              padding: matches ? "24px 40px" : "16px 16px",
              justifySelf: "center",
              borderRadius: 20,
              boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
            }}
          >
            <Tabs
              style={{ marginBottom: 16 }}
              centered
              value={tab}
              onChange={handleChange}
              aria-label="simple tabs example"
            >
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="کاربر"
                {...a11yProps(0)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="انتشارات"
                {...a11yProps(1)}
              />
            </Tabs>
            <TabPanel value={tab} index={0}>
              <UserRegister submit={(values) => dispatch(register(values))} />
            </TabPanel>
            <TabPanel value={tab} index={1}>
              <PublisherRegister
                submit={(values) => dispatch(register(values))}
              />
            </TabPanel>
          </Card>
        </Box>
      </Box>
    </Container>
  );
}
