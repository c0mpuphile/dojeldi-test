import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  SvgIcon,
  Typography,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  InfoOutlined,
  Instagram,
  Search,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Pagination, PaginationItem, Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";

export default function ContactUs(props) {
  return (
    <Container maxWidth="xl" disableGutters>
      <Head>صفحه مورد نظر پیدا نشد</Head>

      <Grid
        container
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginTop: 64,
        }}
      >
        <Grid item lg={6}>
          <Card
            style={{
              borderRadius: 30,
              boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
            }}
          >
            <Grid container style={{ justifyContent: "center" }}>
              <Grid item lg={6} style={{ width: "100%" }}>
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    margin: 32,
                    alignItems: "center",
                    flex: "0 0 50%",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "ravi black",
                      fontSize: 28,
                      color: "#202439",
                    }}
                  >
                    ارسال پیام به دو جلدی
                  </Typography>
                  <Box style={{ width: "100%", marginTop: 24 }}>
                    <input
                      placeholder="نام و نام خـانـوادگی"
                      style={{
                        backgroundColor: "transparent",
                        border: "solid 1px #E0E1E5",
                        fontFamily: "ravi medium",
                        fontSize: 16,
                        borderRadius: 16,
                        padding: "16px 32px",
                        width: "100%",
                      }}
                    />
                  </Box>
                  <Box style={{ width: "100%", marginTop: 24 }}>
                    <input
                      placeholder="پست الکترونیک"
                      style={{
                        backgroundColor: "transparent",
                        border: "solid 1px #E0E1E5",
                        fontFamily: "ravi medium",
                        fontSize: 16,
                        borderRadius: 16,
                        padding: "16px 32px",
                        width: "100%",
                      }}
                    />
                  </Box>
                  <Box style={{ width: "100%", marginTop: 24 }}>
                    <input
                      placeholder="شـماره هـمراه"
                      style={{
                        backgroundColor: "transparent",
                        border: "solid 1px #E0E1E5",
                        fontFamily: "ravi medium",
                        fontSize: 16,
                        borderRadius: 16,
                        padding: "16px 32px",
                        width: "100%",
                      }}
                    />
                  </Box>
                  <Box style={{ width: "100%", marginTop: 24 }}>
                    <input
                      placeholder="مـوضـوع پـیام"
                      style={{
                        backgroundColor: "transparent",
                        border: "solid 1px #E0E1E5",
                        fontFamily: "ravi medium",
                        fontSize: 16,
                        borderRadius: 16,
                        padding: "16px 32px",
                        width: "100%",
                      }}
                    />
                  </Box>
                  <Box style={{ width: "100%", marginTop: 24 }}>
                    <textarea
                      placeholder="مـتن پـیام"
                      style={{
                        backgroundColor: "transparent",
                        border: "solid 1px #E0E1E5",
                        fontFamily: "ravi medium",
                        fontSize: 16,
                        borderRadius: 16,
                        padding: "16px 32px",
                        width: "100%",
                        resize: "none",
                      }}
                    />
                  </Box>
                  <Button
                    style={{
                      backgroundColor: "#FF5252",
                      borderRadius: 15,
                      marginTop: 24,
                      width: "100%",
                    }}
                  >
                    <Typography
                      style={{
                        fontFamily: "ravi bold",
                        fontSize: "18",
                        color: "white",
                        margin: "10px 72px",
                      }}
                    >
                      ارسـال پـیام
                    </Typography>
                  </Button>
                </Box>
              </Grid>
              <Grid item lg={6}>
                <Box
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    margin: 32,
                    alignItems: "center",
                    flex: "1 0 0%",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "ravi black",
                      fontSize: 28,
                      color: "#202439",
                    }}
                  >
                    ارتباط با دوجلدی
                  </Typography>
                  <Typography
                    style={{
                      textAlign: "center",
                      fontFamily: "ravi medium",
                      fontSize: 14,
                      color: "#878A9B",
                      marginTop: 24,
                      lineHeight: 2,
                    }}
                  >
                    کـاربـر گـرامی ضـمن عرض خوش‌آمد گویی به سایت دوجـلدی شما
                    میتوانید با شماره هـای ذیل تمـاس حاصـل فرمـایید و یا اینکه
                    از فرم استفاده نمایید تا کارشناسـان ما در اسرع وقت با شما
                    تماس بگیرند
                  </Typography>
                  <Typography
                    style={{
                      textAlign: "center",
                      fontFamily: "ravi black",
                      fontSize: 24,
                      color: "#202439",
                      marginTop: 40,
                      lineHeight: 2,
                      direction: "ltr",
                    }}
                  >
                    ۰۲۱ - ۵۴۰۶۳
                  </Typography>
                  <Typography
                    style={{
                      textAlign: "center",
                      fontFamily: "ravi black",
                      fontSize: 24,
                      color: "#202439",
                      direction: "ltr",
                    }}
                  >
                    ۰۲۱ - ۹۱۳۰۰۹۸۰
                  </Typography>
                  <Typography
                    style={{
                      fontFamily: "ravi medium",
                      fontSize: 14,
                      color: "#1273EB",
                      marginTop: 32,
                    }}
                  >
                    پاسخگویی ۲۴ ساعته، ٧ روز هفته
                  </Typography>
                  <Box style={{ marginTop: 80 }}>
                    <IconButton>
                      <Twitter style={{ color: "#878A9B", fontSize: 18 }} />
                    </IconButton>
                    <IconButton>
                      <Instagram style={{ color: "#878A9B", fontSize: 18 }} />
                    </IconButton>
                    <IconButton>
                      <WhatsApp style={{ color: "#878A9B", fontSize: 18 }} />
                    </IconButton>
                    <IconButton>
                      <Telegram style={{ color: "#878A9B", fontSize: 18 }} />
                    </IconButton>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
