import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  Snackbar,
  SvgIcon,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  InfoOutlined,
  Instagram,
  Search,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Alert, Pagination, PaginationItem, Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";
import { login } from "../../redux/auth/login/actions";
import { connect, useDispatch, useSelector } from "react-redux";
import { Form, Formik } from "formik";

import * as Yup from "yup";
import { useEffect, useState } from "react";
import Router from "next/router";

function Login(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const dispatch = useDispatch();
  const { isLoading, data, error, ok } = useSelector(
    (state) => state.auth.login
  );
  const [showMessage, setShowMessage] = useState(false);
  useEffect(() => {
    if (error) setShowMessage(true);
  }, [error]);
  useEffect(() => {
    if (ok) Router.push("/");
  }, [ok]);
  return (
    <Container maxWidth="xl" disableGutters>
      <Head>صفحه مورد نظر پیدا نشد</Head>
      <Snackbar
        onClose={() => {
          setShowMessage(false);
        }}
        anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
        open={showMessage}
        autoHideDuration={5000}
      >
        <Alert
          severity="error"
          onClose={() => {
            setShowMessage(false);
          }}
        >
          {error?.response?.data?.message}
        </Alert>
      </Snackbar>

      <Box
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginTop: 32,
          marginBottom: 32,
        }}
      >
        <img src="/Key.png" style={{ height: matches ? "100%" : "50%" }} />
      </Box>
      <Box
        style={{
          display: "flex",
          flexDirection: "row",
          margin: matches ? "0px 96px" : "0px 16px",
          justifyContent: "space-around",
        }}
      >
        <Box style={{ height: "100%", display: matches ? "block" : "none" }}>
          <img src="Library-rafiki-right.png" />
        </Box>
        <Box style={{ height: "100%", display: matches ? "block" : "none" }}>
          <img src="Library-rafiki-left.png" />
        </Box>
        <Box style={{ position: matches ? "absolute" : "relative" }}>
          <Card
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              minWidth: matches ? 480 : null,
              padding: matches ? "24px 40px" : "16px 16px",
              justifySelf: "center",
              borderRadius: 20,
              boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
            }}
          >
            <Formik
              initialValues={{ username: "", password: "" }}
              onSubmit={(values, { setSubmitting }) => {
                dispatch(login(values));
              }}
              validationSchema={Yup.object().shape({
                username: Yup.string().required(
                  "لطفا نام کاربری خود را وارد کنید"
                ),
                password: Yup.string().required(
                  "لطفا رمز عبور خود را وارد کنید"
                ),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  submitForm,
                  handleReset,
                } = props;
                return (
                  <Form
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Typography
                      style={{
                        color: "#202439",
                        fontFamily: "ravi black",
                        fontSize: 28,
                        textAlign: "center",
                      }}
                    >
                      ورود به حساب کاربری
                    </Typography>
                    <Box style={{ width: "100%", marginTop: 24 }}>
                      <input
                        placeholder="ایمیل یا شـماره هـمراه"
                        name="username"
                        value={values.username}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={{
                          backgroundColor: "transparent",
                          border: "solid 1px #E0E1E5",
                          fontFamily: "ravi medium",
                          fontSize: 16,
                          borderRadius: 16,
                          padding: "16px 32px",
                          width: "100%",
                        }}
                      />
                    </Box>
                    <Box style={{ width: "100%", marginTop: 24 }}>
                      <input
                        placeholder="گـذرواژه"
                        name="password"
                        type="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={{
                          backgroundColor: "transparent",
                          border: "solid 1px #E0E1E5",
                          fontFamily: "ravi medium",
                          fontSize: 16,
                          borderRadius: 16,
                          padding: "16px 32px",
                          width: "100%",
                        }}
                      />
                    </Box>
                    <Box
                      style={{
                        display: "flex",
                        width: "100%",
                        marginTop: 16,
                        alignItems: "center",
                      }}
                    >
                      <InfoOutlined
                        style={{ color: "#1273EB", fontSize: 18 }}
                      />
                      <Link
                        style={{
                          fontFamily: "ravi",
                          fontSize: 14,
                          color: "#1273EB",
                          marginRight: 4,
                        }}
                      >
                        گـذرواژه خـود را فـراموش کـرده‌اید؟
                      </Link>
                    </Box>
                    <Button
                      type="submit"
                      style={{
                        backgroundColor: "#FF5252",
                        borderRadius: 20,
                        marginTop: 80,
                      }}
                    >
                      <Typography
                        style={{
                          fontFamily: "ravi bold",
                          fontSize: "18",
                          color: "white",
                          margin: "8px 72px",
                        }}
                      >
                        ورود به حساب کاربری
                      </Typography>
                    </Button>
                    <Box
                      style={{
                        display: "flex",
                        width: "100%",
                        marginTop: 16,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Typography
                        style={{
                          fontFamily: "ravi",
                          fontSize: 16,
                          color: "#878A9B",
                        }}
                      >
                        حـساب کـاربری ندارید؟
                      </Typography>
                      <Link
                        href={"/Register"}
                        style={{
                          fontFamily: "ravi",
                          fontSize: 16,
                          color: "#1273EB",
                        }}
                      >
                        ثبت‌نام کنید
                      </Link>
                    </Box>
                  </Form>
                );
              }}
            </Formik>
          </Card>
        </Box>
      </Box>
    </Container>
  );
}

export default Login;
