import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Paper,
  SvgIcon,
  Typography,
  withStyles,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import Head from "next/head";
import Header from "../components/PublicComponents/Header";
import Link from "next/link";

export default function FAQ(props) {
  return (
    <Container maxWidth="xl" disableGutters>
      <Head>صفحه مورد نظر پیدا نشد</Head>

      <Grid
        container
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Grid
          item
          lg={6}
          style={{
            justifyContent: "center",
            alignItems: "center",
            margin: "0px 24px",
          }}
        >
          <Typography
            style={{
              fontFamily: "ravi black",
              fontSize: 42,
              marginTop: 40,
              textAlign: "center",
            }}
          >
            سوالات تداول
          </Typography>
          <Box
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: 24,
              width: "100%",
            }}
          >
            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                خرید کتاب
              </Typography>
            </Paper>

            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                کتاب الکترونیک
              </Typography>
            </Paper>

            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                کتاب صوتی
              </Typography>
            </Paper>

            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                کتاب چاپی
              </Typography>
            </Paper>

            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                کتاب دست دوم
              </Typography>
            </Paper>

            <Paper
              style={{
                padding: "8px 16px",
                boxShadow: "0px 0px 10px rgba(0,0,0,0.05)",
                border: "1px solid #D7E1EA",
                borderRadius: 10,
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontFamily: "ravi medium",
                  color: "#878A9B",
                }}
              >
                متفرقه
              </Typography>
            </Paper>
          </Box>
          <Box style={{ marginTop: 32, width: "100%" }}>
            <Accordion
              style={{
                borderRadius: 20,
                marginTop: 8,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                position: "inherit",
              }}
            >
              <AccordionSummary
                style={{ padding: "16px 24px", alignItems: "center" }}
                expandIcon={<Add style={{ color: "#2DB67D" }} />}
              >
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#878A9B",
                  }}
                >
                  شرایط خرید و بازگرداندن کالا برای کتاب‌های چاپی چگونه است؟
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از
                  ۶۰ بار مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است.
                  نویسنده این کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Accordion
              style={{
                borderRadius: 20,
                marginTop: 8,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                position: "inherit",
              }}
            >
              <AccordionSummary
                style={{ padding: "16px 24px", alignItems: "center" }}
                expandIcon={<Add style={{ color: "#2DB67D" }} />}
              >
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#878A9B",
                  }}
                >
                  شرایط خرید و بازگرداندن کالا برای کتاب‌های چاپی چگونه است؟
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از
                  ۶۰ بار مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است.
                  نویسنده این کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Accordion
              style={{
                borderRadius: 20,
                marginTop: 8,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                position: "inherit",
              }}
            >
              <AccordionSummary
                style={{ padding: "16px 24px", alignItems: "center" }}
                expandIcon={<Add style={{ color: "#2DB67D" }} />}
              >
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#878A9B",
                  }}
                >
                  شرایط خرید و بازگرداندن کالا برای کتاب‌های چاپی چگونه است؟
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از
                  ۶۰ بار مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است.
                  نویسنده این کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Accordion
              style={{
                borderRadius: 20,
                marginTop: 8,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                position: "inherit",
              }}
            >
              <AccordionSummary
                style={{ padding: "16px 24px", alignItems: "center" }}
                expandIcon={<Add style={{ color: "#2DB67D" }} />}
              >
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#878A9B",
                  }}
                >
                  شرایط خرید و بازگرداندن کالا برای کتاب‌های چاپی چگونه است؟
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از
                  ۶۰ بار مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است.
                  نویسنده این کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Accordion
              style={{
                borderRadius: 20,
                marginTop: 8,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                position: "inherit",
              }}
            >
              <AccordionSummary
                style={{ padding: "16px 24px", alignItems: "center" }}
                expandIcon={<Add style={{ color: "#2DB67D" }} />}
              >
                <Typography
                  variant="h4"
                  style={{
                    fontFamily: "ravi bold",
                    color: "#878A9B",
                  }}
                >
                  شرایط خرید و بازگرداندن کالا برای کتاب‌های چاپی چگونه است؟
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography
                  variant="h5"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  چهار اثر فلورانس اسکاول شین کتابی که در ایران تابه‌حال بیش از
                  ۶۰ بار مورد چاپ قرار گرفته و خوانندگان بسیاری هم داشته است.
                  نویسنده این کتاب که در حقیقت نام خود را بر روی کتاب گذاشته است
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Typography
              variant="h4"
              style={{
                marginTop: 32,
                textAlign: "center",
                fontFamily: "ravi",
                color: "#878A9B",
              }}
            >
              پاسخ سوال خود را نیافتید؟
              <Link href="/ContactUs">
                <a style={{ color: "#1273EB" }}>با ما در ارتباط باشید</a>
              </Link>
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
