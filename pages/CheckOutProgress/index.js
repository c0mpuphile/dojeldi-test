import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  CircularProgress,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Hidden,
  Icon,
  IconButton,
  Link,
  makeStyles,
  Paper,
  Step,
  StepButton,
  StepConnector,
  StepLabel,
  Stepper,
  SvgIcon,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";

import clsx from "clsx";

import Head from "next/head";
import Header from "../components/PublicComponents/Header";
import { useEffect, useState } from "react";
import ConfirmCartComponent from "../components/ConfirmCartComponent/ConfirmCartComponent";
import SetAddressComponent from "../components/ConfirmCartComponent/SetAddressComponent";
import PaymentComponent from "../components/ConfirmCartComponent/PaymentComponent";
import Success from "../components/ConfirmCartComponent/Success";
import { useDispatch, useSelector } from "react-redux";
import { payByWallet } from "../../redux/invoice/payByWallet/actions";
import { payInvoice } from "../../redux/invoice/payInvoice/actions";
import { calculateCoupon } from "../../redux/invoice/calculateCoupon/actions";
import { useRouter } from "next/router";

export default function CheckOutProgress(props) {
  const {
    error: errorCart,
    ok: okCart,
    data: dataCart,
    isLoading: loadingCart,
  } = useSelector((state) => state.cart.getCartOnline);
  const {
    error: errorCoupon,
    ok: okCoupon,
    data: dataCoupon,
    isLoading: loadingCoupon,
  } = useSelector((state) => state.invoice.calculateCoupon);
  const {
    error: errorPay,
    ok: okPay,
    data: dataPay,
    isLoading: loadingPay,
  } = useSelector((state) => state.invoice.payInvoice);
  useEffect(() => {
    if (okPay && dataPay) {
      window.open(dataPay?.link, "_blank");
    }
  }, [okPay, dataPay]);
  const [coupon, setCoupon] = useState(null);
  const [address, setAddress] = useState(null);
  const [useWallet, setUseWallet] = useState(true);

  const router = useRouter();

  useEffect(() => {
    if (
      !localStorage.getItem("token") ||
      localStorage.getItem("token") === null
    ) {
      router.push("/Login");
    }
    setAddress(localStorage.getItem("address"));
  }, []);
  useEffect(() => {
    localStorage.setItem("address", address);
  }, [address]);

  const QontoConnector = withStyles({
    alternativeLabel: {
      top: 10,
      left: "calc(-50% + 16px)",
      right: "calc(50% + 16px)",
    },
    active: {
      "& $line": {
        borderColor: "#FF5252",
      },
    },
    completed: {
      "& $line": {
        borderColor: "#FF5252",
      },
    },
    line: {
      borderColor: "#eaeaf0",
      borderTopWidth: 3,
      borderRadius: 1,
    },
  })(StepConnector);

  const useQontoStepIconStyles = makeStyles({
    root: {
      color: "#eaeaf0",
      display: "flex",
      height: 22,
      alignItems: "center",
    },
    active: {
      color: "#FF5252",
    },
    circle: {
      width: 8,
      height: 8,
      borderRadius: "50%",
      backgroundColor: "currentColor",
    },
    completed: {
      width: 8,
      height: 8,
      borderRadius: "50%",
      backgroundColor: "#FF5252",
    },
  });

  function QontoStepIcon(props) {
    const classes = useQontoStepIconStyles();
    const { active, completed } = props;

    return (
      <div
        className={clsx(classes.root, {
          [classes.active]: active,
        })}
      >
        {completed ? (
          <div className={classes.completed} />
        ) : (
          <div className={classes.circle} />
        )}
      </div>
    );
  }

  const theme = useTheme();

  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const [activeStep, setActiveStep] = useState(0);
  const dispatch = useDispatch();
  useEffect(() => {
    if (router.query.status && router.query.invoice_id) {
      setActiveStep(3);
    }
  }, [router]);
  const handleNext = () => {
    if (activeStep === 2) {
      console.log(
        Object.assign(
          {
            id: dataCart?.data?.id,
            address_id: address,
          },
          coupon && { coupon_code: coupon }
        )
      );
      if (useWallet) {
        dispatch(
          payByWallet(
            Object.assign(
              {
                id: dataCart?.data?.id,
                address_id: address,
              },
              coupon && { coupon_code: coupon }
            )
          )
        );
      } else {
        dispatch(
          payInvoice(
            Object.assign(
              {
                id: dataCart?.data?.id,
                address_id: address,
              },
              coupon && { coupon_code: coupon }
            )
          )
        );
      }
    } else {
      if ((activeStep === 1 && address) || activeStep !== 1) {
        setActiveStep((prev) => prev + 1);
        console.log(address);
      }
    }
  };
  const handleBack = () => {
    setActiveStep((prev) => prev - 1);
  };
  const handleReset = () => {
    setActiveStep(0);
  };
  const steps = [
    {
      id: 0,
      image: "/step1.png",
      doneImage: "/step1-done.png",
      label: "بــررسـی و تـایـیـد",
      subLabel: "تایید و نهایی‌سازی‌سفارش",
    },
    {
      id: 1,
      image: "/step2.png",
      doneImage: "/step2-done.png",
      label: "اطـلاعات گـیرنـده",
      subLabel: "مـشخصات تـحویـل گـیرنـده",
    },
    {
      id: 2,
      image: "/step3.png",
      doneImage: "/step3-done.png",
      label: "پــرداخـت هـزینه",
      subLabel: "پرداخت از طریق درگاه بانکی",
    },
    {
      id: 3,
      image: "/step4.png",
      doneImage: "/step4-done.png",
      label: "اتمام خرید و ارسال",
      subLabel: "خـرید شـما با مـوفقیت ثـبت شد",
    },
  ];
  return (
    <Container maxWidth="xl" disableGutters>
      <Head>سبد خرید</Head>

      <Grid
        container
        style={{
          justifyContent: "center",
        }}
      >
        <Grid lg={6} xs={12}>
          <Stepper
            style={{ backgroundColor: "transparent", width: "100%" }}
            activeStep={activeStep}
            alternativeLabel
            connector={<QontoConnector />}
          >
            {steps.map((item) => (
              <Step key={item.id}>
                <StepLabel step StepIconComponent={QontoStepIcon}>
                  <img
                    src={activeStep >= item.id ? item.doneImage : item.image}
                    style={{ width: matches ? null : 18 }}
                  />
                  <Typography
                    style={{
                      fontFamily: "ravi black",
                      fontSize: matches ? 21 : 10,
                      color: "#202439",
                    }}
                  >
                    {item.label}
                  </Typography>
                  <Hidden mdDown>
                    <Typography
                      style={{
                        fontFamily: "ravi",
                        fontSize: 12,
                        color: "#878A9B",
                      }}
                    >
                      {item.subLabel}
                    </Typography>
                  </Hidden>
                </StepLabel>
              </Step>
            ))}
          </Stepper>
          {activeStep < 3 ? (
            <Card
              style={{
                padding: "16px 32px",
                boxShadow: "0px 10px 20px rgba(0,0,0,0.05)",
                width: "100%",
                borderRadius: 10,
                display: "flex",
                flexDirection: matches ? "row" : "column",
                justifyContent: "space-between",
              }}
            >
              <Box
                style={{
                  display: "flex",
                  flexDirection: matches ? "row" : "column",
                  alignItems: "center",
                }}
              >
                <Box
                  style={{
                    display: "flex",
                    flexDirection: matches ? "column" : "row-reverse",
                    alignItems: !matches ? "center" : null,
                    width: !matches ? "100%" : null,
                    justifyContent: !matches ? "space-between" : null,
                  }}
                >
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-end",
                    }}
                  >
                    <Typography
                      style={{
                        fontSize: 18,
                        fontFamily: "ravi bold",
                        color: "#202439",
                        marginLeft: 4,
                      }}
                    >
                      {dataCart?.data?.total_price}
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontFamily: "ravi medium",
                        color: "#202439",
                      }}
                    >
                      تومان
                    </Typography>
                  </Box>
                  <Typography
                    style={{
                      fontSize: 12,
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    مبلغ کل کتاب‌های انتخابی
                  </Typography>
                </Box>
                <Divider
                  orientation={matches ? "vertical" : "horizontal"}
                  style={{ margin: matches ? "0px 24px" : "8px 0px" }}
                />
                <Box
                  style={{
                    display: "flex",
                    flexDirection: matches ? "column" : "row-reverse",
                    alignItems: !matches ? "center" : null,
                    width: !matches ? "100%" : null,
                    justifyContent: !matches ? "space-between" : null,
                  }}
                >
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-end",
                    }}
                  >
                    <Typography
                      style={{
                        fontSize: 18,
                        fontFamily: "ravi bold",
                        color: "#202439",
                        marginLeft: 4,
                      }}
                    >
                      {dataCart?.data?.total_discount +
                        (dataCoupon?.discount || 0)}
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontFamily: "ravi medium",
                        color: "#202439",
                      }}
                    >
                      تومان
                    </Typography>
                  </Box>
                  <Typography
                    style={{
                      fontSize: 12,
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    تخفیف‌های محاسبه شده
                  </Typography>
                </Box>
                <Divider
                  orientation={matches ? "vertical" : "horizontal"}
                  style={{ margin: matches ? "0px 24px" : "8px 0px" }}
                />
                <Box
                  style={{
                    display: "flex",
                    flexDirection: matches ? "column" : "row-reverse",
                    alignItems: !matches ? "center" : null,
                    width: !matches ? "100%" : null,
                    justifyContent: !matches ? "space-between" : null,
                  }}
                >
                  {dataCart?.data?.shipment_cost !== 0 ? (
                    <Box
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "flex-end",
                      }}
                    >
                      <Typography
                        style={{
                          fontSize: 18,
                          fontFamily: "ravi bold",
                          color: "#202439",
                          marginLeft: 4,
                        }}
                      >
                        {dataCart?.data?.shipment_cost}
                      </Typography>
                      <Typography
                        style={{
                          fontSize: 14,
                          fontFamily: "ravi medium",
                          color: "#202439",
                        }}
                      >
                        تومان
                      </Typography>
                    </Box>
                  ) : (
                    <Typography
                      style={{
                        fontSize: 18,
                        fontFamily: "ravi bold",
                        color: "#202439",
                        marginLeft: 4,
                      }}
                    >
                      رایگان
                    </Typography>
                  )}
                  <Typography
                    style={{
                      fontSize: 12,
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    هزینه ارسال محموله
                  </Typography>
                </Box>
              </Box>
              <Divider
                orientation={matches ? "vertical" : "horizontal"}
                style={{
                  margin: matches ? "0px 24px" : "8px 0px",
                  width: !matches ? "100%" : null,
                }}
              />
              <Box
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: !matches ? "space-between" : null,
                  marginTop: !matches ? 8 : 0,
                }}
              >
                <Box>
                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-end",
                    }}
                  >
                    <Typography
                      style={{
                        fontSize: 18,
                        fontFamily: "ravi bold",
                        color: "#FF5252",
                        marginLeft: 4,
                      }}
                    >
                      {dataCart?.data?.payable_price -
                        (dataCoupon?.discount || 0)}
                    </Typography>
                    <Typography
                      style={{
                        fontSize: 14,
                        fontFamily: "ravi medium",
                        color: "#FF5252",
                      }}
                    >
                      تومان
                    </Typography>
                  </Box>
                  <Typography
                    style={{
                      fontSize: 12,
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    مبلغ نهایی قابل پرداخت
                  </Typography>
                </Box>
                <Button
                  onClick={handleNext}
                  disabled={loadingPay || dataCart?.data?.items?.length === 0}
                  style={{
                    backgroundColor: "#FF5252",
                    borderRadius: 20,
                    marginRight: 16,
                  }}
                >
                  {loadingPay ? (
                    <CircularProgress
                      style={{ color: "white", margin: "8px 16px" }}
                    />
                  ) : (
                    <Typography
                      style={{
                        fontFamily: "ravi bold",
                        fontSize: "18",
                        color: "white",
                        margin: "8px 16px",
                      }}
                    >
                      {activeStep === 2
                        ? "پرداخت هزینه"
                        : "ادامـه فـرآیند خـرید"}
                    </Typography>
                  )}
                </Button>
              </Box>
            </Card>
          ) : null}
          {activeStep === 0 ? (
            <ConfirmCartComponent data={dataCart?.data?.items} />
          ) : null}
          {activeStep === 1 ? (
            <SetAddressComponent
              address={address}
              setAddress={(id) => setAddress(id)}
            />
          ) : null}
          {activeStep === 2 ? (
            <PaymentComponent
              coupon={coupon}
              price={dataCart?.data?.payable_price}
              setCoupon={(value) => setCoupon(value)}
              useWallet={useWallet}
              setUseWallet={(value) => setUseWallet(value)}
            />
          ) : null}
          {activeStep === 3 ? (
            <Success
              status={router.query.status}
              reference={router.query.reference_code}
              invoice={router.query.invoice_id}
            />
          ) : null}
          <Box
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              marginBottom: 16,
            }}
          >
            {activeStep > 0 && activeStep < 3 ? (
              <Button
                style={{
                  backgroundColor: "rgba(135,138,155,0.1)",
                  borderRadius: 20,
                  margin: "32px 8px 0px 8px",
                }}
                onClick={handleBack}
              >
                <Typography
                  style={{
                    fontFamily: "ravi bold",
                    fontSize: "18",
                    color: "#878A9B",
                    margin: "8px 16px",
                  }}
                >
                  بازگشت به {steps[activeStep - 1].label}
                </Typography>
              </Button>
            ) : null}
            {activeStep < 3 ? (
              <Button
                onClick={handleNext}
                disabled={loadingPay || dataCart?.data?.items?.length === 0}
                style={{
                  backgroundColor: "#FF5252",
                  borderRadius: 20,
                  margin: "32px 8px 0px 8px",
                }}
              >
                {loadingPay ? (
                  <CircularProgress
                    style={{ color: "white", margin: "8px 16px" }}
                  />
                ) : (
                  <Typography
                    style={{
                      fontFamily: "ravi bold",
                      fontSize: "18",
                      color: "white",
                      margin: "8px 16px",
                    }}
                  >
                    {activeStep === 2 ? "پرداخت هزینه" : "ادامـه فـرآیند خـرید"}
                  </Typography>
                )}
              </Button>
            ) : null}
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
