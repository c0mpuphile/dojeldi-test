import Head from "next/head";
import styles from "../styles/Home.module.css";
import {
  Container,
  Grid,
  CardMedia,
  Box,
  Typography,
  useTheme,
  Card,
  Fade,
  GridListTile,
  useMediaQuery,
  BottomNavigation,
  BottomNavigationAction,
  Hidden,
  Dialog,
  IconButton,
  Divider,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Link,
} from "@material-ui/core";
import {
  AccessibilityNew,
  ArrowForward,
  BookmarkBorderOutlined,
  CategoryOutlined,
  Close,
  DialerSip,
  HomeOutlined,
  MoreHoriz,
  MoreHorizOutlined,
  PersonOutline,
} from "@material-ui/icons";
import { useEffect, useState } from "react";
import ProductList from "./components/PublicComponents/ProductList";
import ProductListItem from "./components/PublicComponents/ProductListItem";
import CategoryListItem from "./components/PublicComponents/CategoryListItem";
import BlogListItem from "./components/PublicComponents/BlogListItem";
import Footer from "./components/PublicComponents/Footer";
import Header from "./components/PublicComponents/Header";
import Slider from "./components/PublicComponents/Slider";
import MobileSlider from "./components/PublicComponents/MobileSlider";
import ProductListItemMobile from "./components/PublicComponents/ProductListItemMobile";
import BottomTab from "./components/PublicComponents/BottomTab";
import Terms from "./components/PublicComponents/Terms";
import { useDispatch, useSelector } from "react-redux";
import { getBanners } from "../redux/banners/getBanners/actions";
import { getSections } from "../redux/sections/getSections/actions";

import { BASE_URL } from "../services/axios";
import { getFull } from "../redux/category/getFull/actions";

function Home() {
  const dispatch = useDispatch();
  const {
    isLoading: bannerLoading,
    data: bannerData,
    error: bannerError,
  } = useSelector((state) => state.banners.getBanners);
  const {
    isLoading: sectionLoading,
    data: sectionData,
    error: sectionError,
  } = useSelector((state) => state.sections.getSections);
  const {
    isLoading: categoriesLoading,
    data: categoriesData,
    error: categoriesError,
    ok: categoriesOk,
  } = useSelector((state) => state.category.getFull);
  useEffect(() => {
    dispatch(getBanners());
    dispatch(getSections());
    dispatch(getFull());
  }, []);
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const matchesSm = useMediaQuery(theme.breakpoints.up("sm"));
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Head>
        <title>دوجلدی</title>
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
      </Head>
      {/* header start */}
      {/* header end */}
      {/* main slider start */}
      <Box style={{ maxWidth: "100%" }}>
        {matches ? <Slider /> : <MobileSlider />}
      </Box>
      {/* main slider end */}
      {/* list start */}
      <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
          padding: "0px 8px",
        }}
      >
        {sectionData && sectionData.data.length > 0 ? (
          <ProductList
            url={sectionData?.data[0].link}
            component={(value) => (
              <GridListTile style={{ height: "100%" }}>
                {matches ? (
                  <ProductListItem data={value} />
                ) : (
                  <ProductListItemMobile data={value} />
                )}
              </GridListTile>
            )}
            title={sectionData?.data[0].title}
          />
        ) : null}
      </Box>
      <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
          padding: "0px 8px",
        }}
      >
        {sectionData?.data.length > 0 ? (
          <ProductList
            url={sectionData?.data[1].link}
            component={(value) => (
              <GridListTile style={{ height: "100%" }}>
                {matches ? (
                  <ProductListItem data={value} />
                ) : (
                  <ProductListItemMobile data={value} />
                )}
              </GridListTile>
            )}
            title={sectionData?.data[1].title}
          />
        ) : null}
      </Box>
      {/* list end */}
      {/* banner start */}
      {bannerData?.data.length > 0 ? (
        <Box
          style={{
            display: "flex",
            flex: "100%",
            marginTop: matches ? 80 : 40,
          }}
        >
          {matchesSm ? (
            <Link href={bannerData?.data[0]?.link}>
              <img
                src={`${BASE_URL}/image/${bannerData?.data[0]?.image}`}
                width={"100%"}
              />
            </Link>
          ) : (
            <Link href={bannerData?.data[0]?.link}>
              <img
                src={`${BASE_URL}image/${bannerData?.data[0]?.image}`}
                width={"100%"}
              />
            </Link>
          )}
        </Box>
      ) : null}
      {/* banner end */}
      {/* category list start */}
      <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
          padding: "0px 8px",
        }}
      >
        <ProductList
          data={categoriesData?.data}
          itemHeight={matches ? 480 : 280}
          component={(value) => {
            if (value?.image) {
              return (
                <GridListTile
                  style={{
                    borderRadius: 4,
                    boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
                  }}
                >
                  <CategoryListItem data={value} />
                </GridListTile>
              );
            } else {
              return null;
            }
          }}
          title={"دسته‌بندی ها"}
        />
      </Box>
      {/* category list end */}
      <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
          padding: "0px 8px",
        }}
      >
        {sectionData && sectionData.data.length > 0 ? (
          <ProductList
            url={sectionData?.data[2].link}
            component={(value) => (
              <GridListTile style={{ height: "100%" }}>
                {matches ? (
                  <ProductListItem data={value} />
                ) : (
                  <ProductListItemMobile data={value} />
                )}
              </GridListTile>
            )}
            title={sectionData?.data[2].title}
          />
        ) : null}
      </Box>
      {/* banner start */}
      {bannerData && bannerData?.data.length > 1 ? (
        <Box
          style={{
            display: "flex",
            flex: "100%",
            marginTop: matches ? 80 : 40,
          }}
        >
          {matchesSm ? (
            <Link href={bannerData?.data[1]?.link}>
              <img
                src={`${BASE_URL}image/${bannerData?.data[1]?.image}`}
                width={"100%"}
              />
            </Link>
          ) : (
            <Link href={bannerData?.data[1]?.link}>
              <img
                src={`${BASE_URL}image/${bannerData?.data[1]?.image}`}
                width={"100%"}
              />
            </Link>
          )}
        </Box>
      ) : null}
      {/* banner end */}
      {/* blog list start */}
      {/* <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: matches ? 80 : 40,
          padding: "0px 8px",
        }}
      >
        <ProductList
          component={
            <GridListTile
              style={{
                height: "100%",
                borderRadius: 4,
                boxShadow: "0px 0px 20px rgba(0,0,0,0.05)",
              }}
            >
              <BlogListItem />
            </GridListTile>
          }
          title={"دسته‌بندی ها"}
          showItem={matches ? 4 : 1.3}
        />
      </Box> */}
      {/* blog list end */}

      {/* footer started */}
      <Footer />
    </Container>
  );
}

export default Home;
