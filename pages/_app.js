import {
  Box,
  CircularProgress,
  createMuiTheme,
  jssPreset,
  responsiveFontSizes,
  StylesProvider,
  ThemeProvider,
} from "@material-ui/core";
import Head from "next/head";
import "../styles/globals.css";
import { create } from "jss";
import rtl from "jss-rtl";
import wrapper from "../redux/store";
import withReduxSaga from "next-redux-saga";
import "nprogress/nprogress.css";
import Router, { useRouter } from "next/router";
import Nprogress from "nprogress";
import { useEffect, useState } from "react";
import Loader from "./Loader";
import { Provider } from "react-redux";
import instance from "../services/axios";
import { SnackbarProvider, useSnackbar } from "notistack";
import Interceptor from "./Interceptor";
import Header from "./components/PublicComponents/Header";
import BottomTab from "./components/PublicComponents/BottomTab";

let theme = createMuiTheme({
  direction: "rtl",
  palette: {
    primary: { main: "#ff5252" },
    badge: { main: "#F78829" },
  },
});

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [pageLoading, setPageLoading] = useState(false);
  useEffect(() => {
    const handleStart = () => {
      setPageLoading(true);
    };
    const handleComplete = () => {
      setPageLoading(false);
    };
    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);
  }, [router]);
  return (
    <ThemeProvider theme={theme}>
      <StylesProvider jss={jss}>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
          />
        </Head>
        {pageLoading ? (
          <Box
            style={{
              width: "100vw",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <CircularProgress color="primary" />
          </Box>
        ) : (
          <SnackbarProvider maxSnack={3}>
            <Interceptor>
              <Header />
              <Component {...pageProps} />
              <BottomTab />
            </Interceptor>
          </SnackbarProvider>
        )}
      </StylesProvider>
    </ThemeProvider>
  );
}

export default wrapper.withRedux(MyApp);
