import { Box, CircularProgress, Container } from "@material-ui/core";

export default function Loader(props) {
  return (
    <Container>
      <Box
        style={{
          width: "100vw",
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <CircularProgress color="primary" />
      </Box>
    </Container>
  );
}
