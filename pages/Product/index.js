import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  CircularProgress,
  Container,
  Divider,
  Grid,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  SvgIcon,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  Instagram,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";
import MainImageMobile from "../components/ProductPageComponents/MainImageMobile";
import ProductListItemMobile from "../components/PublicComponents/ProductListItemMobile";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getBook } from "../../redux/book/getBook/actions";

export default function Product(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const router = useRouter();
  const dispatch = useDispatch();
  const {
    error: bookError,
    ok: bookOk,
    data: bookData,
    isLoading: bookLoading,
  } = useSelector((state) => state.book.getBook);
  useEffect(() => {
    dispatch(getBook({ id: router.query.id }));
  }, [router]);
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Head>محصول</Head>

      <Grid
        container
        spacing={4}
        style={{
          maxWidth: "100%",
          justifyContent: "center",
          margin: 0,
        }}
      >
        {bookData ? (
          <>
            <Grid item xl={3} lg={3} style={{ width: "100%" }}>
              {matches ? (
                <MainImage data={bookData?.data} />
              ) : (
                <MainImageMobile data={bookData?.data} />
              )}
            </Grid>
            <Grid item xl={6} lg={5} style={{ width: "100%" }}>
              <MainContent data={bookData?.data} />
            </Grid>
            <Grid item xl={3} lg={4} style={{ width: "100%" }}>
              <MainPrices data={bookData?.data} />
            </Grid>
            <Comment data={bookData?.data.rates} />
          </>
        ) : (
          <CircularProgress color="primary" />
        )}
      </Grid>

      {/* <Box
        style={{
          display: "flex",
          flex: "100%",
          marginTop: "80px",
          padding: "0px 8px",
        }}
      >
        <ProductList
          component={
            <GridListTile style={{ height: "100%" }}>
              {matches ? <ProductListItem /> : <ProductListItemMobile />}
            </GridListTile>
          }
          title={"شاید دوست داشته باشید"}
        />
      </Box> */}

      <Footer />
    </Container>
  );
}
