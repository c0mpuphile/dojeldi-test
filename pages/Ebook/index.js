import { Box, Container } from "@material-ui/core";
import { useEffect, useState } from "react";
import dynamic from "next/dynamic";

function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match

  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/

  const [windowSize, setWindowSize] = useState({
    width: undefined,

    height: undefined,
  });

  useEffect(() => {
    // Handler to call on window resize

    function handleResize() {
      // Set window width/height to state

      setWindowSize({
        width: window.innerWidth,

        height: window.innerHeight,
      });
    }

    // Add event listener

    window.addEventListener("resize", handleResize);

    // Call handler right away so state gets updated with initial window size

    handleResize();

    // Remove event listener on cleanup

    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount

  return windowSize;
}

export default function Ebook(props) {
  const ReactReader = dynamic(
    () => import("react-reader").then((res) => res.ReactReader),
    { ssr: false }
  );
  const size = useWindowSize();
  const [currentLocation, setCurrentLocation] = React.useState(0);

  const onLocationChanged = React.useCallback(
    (location) => setCurrentLocation(location),
    []
  );
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{
        backgroundColor: "#f7f8fa",
        width: size.width,
        height: size.height,
      }}
    >
      <Box style={{ position: "relative", height: "100%", direction: "ltr" }}>
        <ReactReader
          location={currentLocation}
          locationChanged={onLocationChanged}
          url="/Moby-Dick-Herman-Melville.epub"
          title="harchi"
        />
      </Box>
    </Container>
  );
}
