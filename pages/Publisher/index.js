import {
  Avatar,
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Hidden,
  Icon,
  IconButton,
  Link,
  makeStyles,
  Paper,
  SvgIcon,
  Tab,
  Tabs,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  Delete,
  Headset,
  MoreVert,
  PlayArrow,
  Settings,
} from "@material-ui/icons";
import { useEffect, useState } from "react";
import MyAddresses from "../components/ProfileComponents/MyAddresses";
import MyBookRequests from "../components/ProfileComponents/MyBookRequests";
import MyBooks from "../components/ProfileComponents/MyBooks";
import MyBooksListItem from "../components/ProfileComponents/MyBooksListItem";
import MyFavourits from "../components/ProfileComponents/MyFavourits";
import MyOrders from "../components/ProfileComponents/MyOrders";
import MyTickets from "../components/ProfileComponents/MyTickets";
import MyTransactions from "../components/ProfileComponents/MyTransactions";
import ProfileDetails from "../components/ProfileComponents/ProfileDetailsComponenet";
import ResumeBook from "../components/ProfileComponents/ResumeBookComponent";
import MyMessages from "../components/ProfileComponents/MyMessages";

import Header from "../components/PublicComponents/Header";
import PublisherDetails from "../components/PublisherComponents/PublisherDetailsComponent";
import SellDetails from "../components/PublisherComponents/SellDetailsComponent";
import PublisherProducts from "../components/PublisherComponents/PublisherProductsComponent";
import PublisherTransactions from "../components/PublisherComponents/PublisherTransactionsComponent";
import Router from "next/router";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

export default function Profile(props) {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  useEffect(() => {
    if (
      localStorage.getItem("token") === null ||
      !localStorage.getItem("roles").includes("is-publisher-admin")
    ) {
      Router.push("/Login");
    }
  }, []);
  const [tab, setTab] = useState(0);
  const handleTabChange = (event, value) => {
    setTab(value);
  };
  const useStyle = makeStyles({
    indicator: {
      backgroundColor: "#202439",
    },
    scroll: {
      maxWidth: "100vw",
    },
  });
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }
  const classes = useStyle();
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Grid
        container
        style={{
          marginTop: matches ? 40 : 16,
          justifyContent: "center",
        }}
      >
        <Hidden mdDown>
          <Grid item lg={3} style={{ paddingRight: 32 }}>
            <PublisherDetails />
          </Grid>
        </Hidden>
        <Grid
          item
          lg={7}
          md={12}
          style={{
            paddingLeft: matches ? 32 : 8,
            paddingRight: matches ? 16 : 8,
            maxWidth: !matches ? "100vw" : null,
          }}
        >
          <Hidden mdDown>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <SellDetails />
              <SellDetails />
              <SellDetails />
            </Box>
          </Hidden>
          <Box style={{ marginTop: matches ? 40 : 0, width: "100%" }}>
            <Tabs
              variant="scrollable"
              scrollButtons="off"
              value={tab}
              classes={{
                indicator: classes.indicator,
                scroller: classes.scroll,
              }}
              onChange={handleTabChange}
            >
              <Tab
                style={{
                  display: matches ? "none" : "block",
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="پروفایل"
                {...a11yProps(0)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="آمـار فـروش"
                {...a11yProps(1)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="مـحصـولات"
                {...a11yProps(2)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="تـیکت‌هـا"
                {...a11yProps(3)}
              />
              <Tab
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "#202439",
                }}
                label="کـیف پـول"
                {...a11yProps(4)}
              />
            </Tabs>
            <Hidden lgUp>
              <TabPanel style={{ marginTop: 24 }} value={tab} index={0}>
                <Box>
                  <PublisherDetails />

                  <Box
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Box style={{ marginTop: 16 }}>
                      <SellDetails />
                    </Box>
                    <Box style={{ marginTop: 16 }}>
                      <SellDetails />
                    </Box>
                    <Box style={{ marginTop: 16 }}>
                      <SellDetails />
                    </Box>
                  </Box>
                </Box>
              </TabPanel>
            </Hidden>
            <TabPanel
              style={{ marginTop: 24 }}
              value={tab}
              index={1}
            ></TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={2}>
              <PublisherProducts />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={3}>
              <MyTickets />
            </TabPanel>
            <TabPanel style={{ marginTop: 24 }} value={tab} index={4}>
              <PublisherTransactions />
            </TabPanel>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
