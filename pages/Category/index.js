import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Checkbox,
  CircularProgress,
  Container,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  FormControl,
  FormControlLabel,
  FormGroup,
  Grid,
  GridList,
  GridListTile,
  Hidden,
  Icon,
  IconButton,
  InputLabel,
  Link,
  makeStyles,
  MenuItem,
  Paper,
  Select,
  SvgIcon,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Close,
  Favorite,
  FavoriteBorder,
  GetApp,
  Instagram,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import Search from "../components/Icons/Search";
import _ from "lodash";
import {
  Autocomplete,
  Pagination,
  PaginationItem,
  Rating,
} from "@material-ui/lab";
import Head from "next/head";
import Comment from "../components/ProductPageComponents/Comment";
import Footer from "../components/PublicComponents/Footer";
import Header from "../components/PublicComponents/Header";
import ProductCartType from "../components/PublicComponents/ProductCartType";
import ProductList from "../components/PublicComponents/ProductList";
import ProductListItem from "../components/PublicComponents/ProductListItem";
import MainContent from "../components/ProductPageComponents/MainContent";
import MainImage from "../components/ProductPageComponents/MainImage";
import MainPrices from "../components/ProductPageComponents/MainPrices";
import CategoryProductItemMobile from "../components/CategoryPageComponents/CategoryProductItemMobile";
import BottomTab from "../components/PublicComponents/BottomTab";
import Filter from "../components/PublicComponents/FilterGenerator";
import MultiSelect from "../components/PublicComponents/MultiSelect";
import CategoryFilter from "../components/CategoryComponents/CategoryFilterComponent";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { searchFull } from "../../redux/search/searchFull/actions";
import { getTags } from "../../redux/filters/getTags/actions";

export default function Product(props) {
  const theme = useTheme();
  const dispatch = useDispatch();
  const matches = useMediaQuery(theme.breakpoints.up("lg"));
  const router = useRouter();
  const [timer, setTimer] = useState();
  const [query, setQuery] = useState(router.query);
  const [cat, setCat] = useState("");
  const [subCat, setSubCat] = useState("");
  const [open, setOpen] = useState(false);
  const {
    error: searchError,
    ok: searchOk,
    isLoading: searchLoading,
    data: searchData,
  } = useSelector((state) => state.search.searchFull);
  const {
    error: tagsError,
    data: tags,
    isLoading: tagsLoading,
    ok: tagsOk,
  } = useSelector((state) => state.filters.getTags);
  useEffect(() => {
    dispatch(getTags());
  }, []);
  useEffect(() => {
    dispatch(searchFull(router.query));
    setQuery((prev) => ({ ...prev, ...router.query }));
  }, [router]);
  useEffect(() => {
    if (!_.isEqual(router.query, query)) {
      clearTimeout(timer);
      setTimer(() =>
        setTimeout(() => {
          router.push({ pathname: "Category", query });
        }, 1000)
      );
    }
  }, [query]);
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Head>دسته‌بندی ها</Head>

      <Box>
        <Box
          style={{
            flex: "100%",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Hidden mdDown>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                paddingRight: 64,
                paddingTop: 16,
                paddingLeft: 40,
                paddingBottom: 16,
                alignItems: "center",
                flex: "0 0 20%",
              }}
            >
              <input
                onChange={(value) =>
                  setQuery((prev) => ({ ...prev, q: value.target.value }))
                }
                placeholder="جـستجو در نـتایج نـمایش داده شـده"
                value={query.q}
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  fontFamily: "ravi",
                  fontSize: matches ? 16 : 12,
                  color: "#878A9B",
                  width: "100%",
                }}
              />
              <Search />
            </Box>
            <Divider flexItem orientation="vertical" />
          </Hidden>
          <Box
            style={{
              paddingRight: matches ? 70 : 0,
              paddingTop: 16,
              paddingLeft: matches ? 70 : 0,
              paddingBottom: 16,
              display: "flex",
              flexDirection: matches ? "row" : "column",
              justifyContent: "space-between",
              flex: "1 0 0%",
              alignItems: "center",
            }}
          >
            <Box
              style={{
                display: "flex",
                flexDirection: matches ? "column" : "column-reverse",
                alignItems: matches ? "flex-start" : "center",
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi black",
                  fontSize: matches ? 21 : 18,
                  color: "#202439",
                  marginTop: !matches ? 4 : 0,
                }}
              >
                {subCat || cat}
              </Typography>
              <Breadcrumbs style={{ fontSize: !matches ? 10 : null }}>
                <Link
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  دوجلدی
                </Link>
                <Link
                  variant="h6"
                  style={{
                    fontFamily: "ravi",
                    color: "#878A9B",
                  }}
                >
                  دسته‌بندی
                </Link>
                {cat ? (
                  <Link
                    variant="h6"
                    style={{
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    {cat}
                  </Link>
                ) : null}
                {subCat ? (
                  <Link
                    variant="h6"
                    style={{
                      fontFamily: "ravi",
                      color: "#878A9B",
                    }}
                  >
                    {subCat}
                  </Link>
                ) : null}
              </Breadcrumbs>
            </Box>
            <Box
              style={{
                display: "flex",
                flex: "0px 0px 50%",
                flexDirection: "row",
                alignItems: "center",
                maxWidth: !matches ? "100vw" : null,
                paddingRight: !matches ? 16 : null,
                marginTop: !matches ? 16 : null,
                justifyContent: !matches ? "flex-start" : "flex-end",
              }}
            >
              <Typography
                variant="h6"
                style={{
                  fontFamily: "ravi medium",

                  color: "#202439",
                  marginLeft: 8,
                }}
              >
                مرتب‌سازی به ترتیب:
              </Typography>
              <Filter
                value={query?.tags && query?.tags?.find((item) => item !== "")}
                setValue={(value) =>
                  setQuery((prev) => ({
                    ...prev,
                    tags: value ? ["", value] : [""],
                  }))
                }
                type="tags"
                typeValue="id"
                data={tags?.data}
              />
            </Box>
          </Box>
        </Box>
      </Box>
      <Hidden mdDown>
        <Divider style={{ width: "100%" }} />
      </Hidden>
      <Box style={{ flex: "100%", display: "flex", flexDirection: "row" }}>
        <Hidden mdDown>
          <CategoryFilter
            setC={(value) => setCat(value)}
            setSC={(value) => setSubCat(value)}
            values={query}
            setValues={(value) => setQuery((prev) => ({ ...prev, ...value }))}
          />
          <Divider flexItem orientation="vertical" />
        </Hidden>

        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            flex: "1 0 0%",
            paddingRight: matches ? 40 : null,
            paddingTop: 32,
            paddingLeft: matches ? 70 : null,
            paddingBottom: 16,
            alignItems: "center",
          }}
        >
          {searchLoading ? (
            <CircularProgress color="primary" />
          ) : (
            <GridList
              spacing={32}
              cols={matches ? 3 : 1}
              cellHeight={"auto"}
              style={{ padding: !matches ? "0px 8px" : null, width: "100%" }}
            >
              {searchData?.data?.length > 0
                ? searchData.data.map((product) => (
                    <GridListTile
                      style={{
                        marginBottom: 8,
                      }}
                    >
                      {matches ? (
                        <ProductListItem data={product} />
                      ) : (
                        <CategoryProductItemMobile data={product} />
                      )}
                    </GridListTile>
                  ))
                : null}
            </GridList>
          )}
          <Pagination
            hidePrevButton
            hideNextButton
            renderItem={(item) => (
              <PaginationItem
                style={{
                  fontFamily: "ravi medium",
                  fontSize: 18,
                  color: "#878A9B",
                  paddingTop: 4,
                  marginTop: 16,
                  marginBottom: matches ? 0 : 96,
                }}
                {...item}
              />
            )}
            count={searchData?.pages}
            page={searchData?.current_page}
            onChange={(event, value) =>
              setQuery((prev) => ({ ...prev, page: value }))
            }
            variant="outlined"
            shape="rounded"
          />
        </Box>
      </Box>
      <Hidden lgUp>
        <Box
          style={{
            position: "fixed",
            width: "100%",
            bottom: 56,
            left: 0,
            zIndex: 1,
          }}
        >
          <Button
            onClick={() => setOpen(true)}
            fullWidth
            style={{ backgroundColor: "#FF5252", padding: 8 }}
          >
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 12,
                color: "white",
              }}
            >
              فـیلتر و جـستجو در نـتایج
            </Typography>
          </Button>
        </Box>
        <Dialog open={open} onBackdropClick={() => setOpen(false)}>
          <DialogTitle>
            <Box
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Typography variant="h2" style={{ fontFamily: "ravi bold" }}>
                فیلتر
              </Typography>
              <IconButton onClick={() => setOpen(false)}>
                <Close />
              </IconButton>
            </Box>
          </DialogTitle>
          <DialogContent>
            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingRight: 16,
                paddingTop: 16,
                paddingLeft: 16,
                paddingBottom: 16,
                alignItems: "center",
                width: "100%",
                border: "1px solid #E0E1E5",
                borderRadius: 12,
              }}
            >
              <Box style={{ flex: 1 }}>
                <input
                  onChange={(value) =>
                    setQuery((prev) => ({ ...prev, q: value.target.value }))
                  }
                  placeholder="جـستجو در نـتایج نـمایش داده شـده"
                  value={query.q}
                  style={{
                    backgroundColor: "transparent",
                    border: "none",
                    fontFamily: "ravi",
                    fontSize: matches ? 16 : 12,
                    color: "#878A9B",
                    width: "100%",
                  }}
                />
              </Box>
              <Box>
                <Search />
              </Box>
            </Box>
            <CategoryFilter
              setC={(value) => setCat(value)}
              setSC={(value) => setSubCat(value)}
              values={query}
              setValues={(value) => setQuery((prev) => ({ ...prev, ...value }))}
            />
          </DialogContent>
        </Dialog>
      </Hidden>
    </Container>
  );
}
