import {
  Box,
  Breadcrumbs,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  GridList,
  GridListTile,
  Icon,
  IconButton,
  Link,
  Paper,
  SvgIcon,
  Typography,
  withStyles,
} from "@material-ui/core";
import {
  AddCircleOutlineOutlined,
  Favorite,
  FavoriteBorder,
  GetApp,
  Instagram,
  Search,
  StarRounded,
  Telegram,
  Twitter,
  WhatsApp,
} from "@material-ui/icons";
import { Pagination, PaginationItem, Rating } from "@material-ui/lab";
import Head from "next/head";
import Comment from "./components/ProductPageComponents/Comment";
import Footer from "./components/PublicComponents/Footer";
import Header from "./components/PublicComponents/Header";
import ProductCartType from "./components/PublicComponents/ProductCartType";
import ProductList from "./components/PublicComponents/ProductList";
import ProductListItem from "./components/PublicComponents/ProductListItem";
import MainContent from "./components/ProductPageComponents/MainContent";
import MainImage from "./components/ProductPageComponents/MainImage";
import MainPrices from "./components/ProductPageComponents/MainPrices";

export default function NotFound(props) {
  return (
    <Container
      maxWidth="xl"
      disableGutters
      style={{ backgroundColor: "#f7f8fa" }}
    >
      <Head>صفحه مورد نظر پیدا نشد</Head>

      <Grid container lg={12} justify={"center"}>
        <Grid item lg={6}>
          <Box
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
              marginTop: 48,
            }}
          >
            <Typography
              style={{
                fontFamily: "ravi black",
                fontSize: 128,
                color: "#263238",
                textAlign: "center",
              }}
            >
              ۴۰۴
            </Typography>
            <Typography
              style={{
                fontFamily: "ravi medium",
                fontSize: 24,
                color: "#263238",
                marginTop: -32,
                textAlign: "center",
              }}
            >
              مـتاسـفانه صـفحه مـوردنـظر پـیدا نـشد
            </Typography>
            <Button
              style={{
                backgroundColor: "#FF5252",
                borderRadius: 20,
                marginTop: 10,
              }}
            >
              <Typography
                style={{
                  fontFamily: "ravi bold",
                  fontSize: 18,
                  color: "white",
                  margin: "8px 72px",
                }}
              >
                بازگشت به صفحه اصلی
              </Typography>
            </Button>
            <Box style={{ height: "100%", marginTop: 40 }}>
              <img
                style={{ width: "100%" }}
                src="/404 Error Page not Found with people connecting a plug-pana.png"
              />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
