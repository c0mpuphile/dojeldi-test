import axios from './axios';

const SectionsAPI = {
  getSections: (params) => axios.get(`/home/sections`, { params }),
};

export default SectionsAPI;