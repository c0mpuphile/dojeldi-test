import axios from "./axios";

const CartAPI = {
  addToCartOnline: (data) => axios.post(`/cart`, data),
  getCartOnline: (params) => axios.get(`/user/cart`, { params }),
  removeFromCartOnline: (params) => axios.delete(`/cart`, { params }),
};

export default CartAPI;
