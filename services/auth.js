import axios from './axios';

const AuthAPI = {
  login: (data) => axios.post(`/auth/login`, data),
  register: (data) => axios.post(`/auth/register`, data),
  verify: (data) => axios.post(`/user/email/verify`, data),
};

export default AuthAPI;