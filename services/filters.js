import axios from './axios';

const FiltersAPI = {
  getPublishers: (params) => axios.get(`/home/publishers`, { params }),
  getAuthors: (params) => axios.get(`/home/authors`, { params }),
  getTags: (params) => axios.get(`/home/tags`, { params }),
  getBookTypes: (params) => axios.get(`/home/book/types`, { params }),
};

export default FiltersAPI;