import { isArray } from "lodash";
import QueryString from "qs";
import axios from "./axios";

const SearchAPI = {
  searchFull: (params) =>
    axios.get(`/book`, {
      params,
      paramsSerializer: (params) => {
        let query = { ...params };
        for (var key in query) {
          if (query.hasOwnProperty(key) && isArray(query[key])) {
            if (query[key].indexOf("") > -1) {
              query[key].splice(query[key].indexOf(""), 1);
            }
          }
        }

        return QueryString.stringify(query);
      },
    }),
};

export default SearchAPI;
