import axios from "./axios";

const ProfileAPI = {
  getProfile: (params) => axios.get(`/user/profile`, { params }),
  editProfile: (data) => axios.post(`/user/profile/edit`, data),
  uploadProfilePic: (data) => axios.post(`/user/profile/picture`, data),
  addAddress: (data) => axios.post(`/user/address`, data),
  getAddress: (params) => axios.get(`/user/addresses`, { params }),
  editAddress: (address_id, data) =>
    axios.post(`/user/address/${address_id}/edit`, data),
  getAudioBooks: (params) => axios.get(`/user/books/audio`, { params }),
  getPrintBooks: (params) => axios.get(`/user/books/print`, { params }),
  getOrders: (status, params) =>
    axios.get(`/user/orders?status=${status}`, { params }),
  getIntrests: (params) => axios.get(`/user/interests`, { params }),
  getGenders: (params) => axios.get(`/home/genders`, { params }),
  getElectronicBooks: (params) =>
    axios.get(`/user/books/electronic`, { params }),
};

export default ProfileAPI;
