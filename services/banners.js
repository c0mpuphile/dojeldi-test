import axios from './axios';

const BannersAPI = {
  getBanners: (params) => axios.get(`/home/banners`, { params }),
};

export default BannersAPI;