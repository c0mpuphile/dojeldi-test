import axios from './axios';

const TransactionsAPI = {
  getUserTransactions: (params) => axios.get(`/user/transaction`, { params }),
  getPublisherTransactions: (params) => axios.get(`/publisher/transaction`, { params }),
};

export default TransactionsAPI;