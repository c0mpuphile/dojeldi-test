import axios from './axios';

const CategoryAPI = {
  getTree: (params) => axios.get(`/home/categories`, { params }),
  getFull: (params) => axios.get(`/home/full-categories`, { params }),
};

export default CategoryAPI;