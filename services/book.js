import axios from "./axios";

const BookAPI = {
  getBook: (id, params) => axios.get(`/book/${id}`, { params }),
  likeBook: (id, data) => axios.post(`/book/interest/${id}`, data),
  likeRate: (id, data) => axios.post(`/book/rate/like/${id}`, data),
  rateBook: (id, data) => axios.post(`/book/rate/${id}`, data),
  commentToRate: (id, rateId, data) =>
    axios.post(`/book/rate/${rateId}/comment/${id}`, data),
};

export default BookAPI;
