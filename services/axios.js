import axios from "axios";
export const BASE_URL = "https://api.dojeldi.org/api";
const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    Accepts: "application/json",
    "Referrer-Policy": "no-referrer",
  },
});

instance.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;

  return config;
});

export async function getData(url, setter) {
  const response = await instance.get(url).then((response) => response.data);
  setter(response.data);
}

export default instance;
