import axios from "./axios";

const InvoiceAPI = {
  payInvoice: (id, params) => axios.get(`/invoice/${id}`, { params }),
  verifyPayment: (params) => axios.get(`/invoice/verify`, { params }),
  paymentMethods: (params) => axios.get(`/invoice/payment-methods`, { params }),
  payByWallet: (id, params) =>
    axios.post(`/invoice/pay-by-wallet/${id}`, null, { params }),
  chargeWallet: (params) => axios.get(`/invoice/charge-wallet`, { params }),
  getBalance: (params) => axios.get(`/invoice/balance`, { params }),
  chargeWalletByCode: (data) =>
    axios.post(`/invoice/charge-wallet-by-code`, data),
  calculateCoupon: (id, price, params) =>
    axios.get(`/invoice/payable-price/${id}?payable_price=${price}`, {
      params,
    }),
};

export default InvoiceAPI;
