import axios from './axios';

const LocationsAPI = {
  getLocations: (params) => axios.get(`/home/city-province`, { params }),
};

export default LocationsAPI;