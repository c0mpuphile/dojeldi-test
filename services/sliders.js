import axios from './axios';

const SlidersAPI = {
  getSliders: (params) => axios.get(`/home/sliders`, { params }),
};

export default SlidersAPI;