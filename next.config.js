module.exports = {
  trailingSlash: true,
  exportPathMap: async function (defaultPathMap) {
    return {
      "/": { page: "/" },
      "/404": { page: "/404" },
      "/AboutUs": { page: "/AboutUs" },
      "/AudioBook": { page: "/AudioBook" },
      "/Category": { page: "/Category" },
      "/CheckOutProgress": { page: "/CheckOutProgress" },
      "/ContactUs": { page: "/ContactUs" },
      "/Ebook": { page: "/Ebook" },
      "/FAQ": { page: "/FAQ" },
      "/Login": { page: "/Login" },
      "/Product": { page: "/Product" },
      "/Profile": { page: "/Profile" },
      "/Publisher": { page: "/Publisher" },
      "/Register": { page: "/Register" },
    };
  },
};
